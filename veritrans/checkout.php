<?php
session_start();

require_once 'veritrans.php';
$veritrans = new Veritrans;

$veritrans->merchant_id   = 'T100000000000001000117';
$veritrans->merchant_hash = 'd8ea4209b6975a0fdafab17906545a91d281976b53ebece094401a1840585a3e';

$veritrans->settlement_type = '01';

$veritrans->order_id = 'L2012120001';
$veritrans->session_id = session_id();
$veritrans->gross_amount = 150000;
$veritrans->card_capture_flag = 1;
$veritrans->customer_specification_flag = 1;
$veritrans->shipping_flag = 0;

$commodities =  array (

		array("COMMODITY_ID" => '001', 
		      "COMMODITY_UNIT" => 150000, 
    		  "COMMODITY_NUM" => 1, 
    		  "COMMODITY_NAME1" => 'Kaos', 
    		  "COMMODITY_NAME2" => 'T-Shirt'
		)
);

$veritrans->commodity = $commodities;

$key = $veritrans->get_keys();

form_redirection($key,$veritrans->merchant_id,$veritrans->order_id);

function form_redirection($key,$merchant_id,$order_id){
	$html='<!DOCTYPE html>
			<html>
			<head>
				<script language="javascript" type="text/javascript">
				<!--
				function onloadEvent() {
				  document.form_auto_post.submit();
				}
				//-->
				</script>
			  </head>
			  <body>
			    <h1 align="center">Go to select payment options</h1>
			
			    <form action="https://payments.veritrans.co.id/web1/paymentStart.action" method="post" name="form_auto_post">
			      <input type="hidden" name="MERCHANT_ID" value="'.$merchant_id.'" />
			      <input type="hidden" name="ORDER_ID" value="'.$order_id.'" />
			      <input type="hidden" name="TOKEN_BROWSER" value="'.$key['token_browser'].'" />
			      <input id="submitBtn" type="submit" value="Confirm Checkout" />
			    </form>
			 </body>
			</html>';
	
	echo $html;
}

?>