<?php
function footers(){
	set_template(TEMPLATE_PATH."/footers.html",'footers');
	add_block('footersMenuBlock','footmenublock','footers');
	//add_block('footersContactBlock','footcontactblock','footers');
	add_block('footersContainerBlock','footcontainerblock','footers');
	add_block('footersBlock','footblock','footers');
	$exist_content_footer=false;
	$footermenus = the_menus('menuset=Footer Menu&show_title=false&class=footer-menu clearfix');
	add_variable('footer_menu', $footermenus);

	if(trim($footermenus)!=""){
		parse_template('footersMenuBlock','footmenublock',true);
		$exist_content_footer = true;
	}


	/*$address = get_meta_data('ci_address');
	if(trim($address)!=""){
		add_variable("title_address","<p class=\"text text-15\">Address.</p>");
		add_variable("address","<div class=\"text text-16\">$address</div>");
		parse_template('footersContactBlock','footcontactblock',true);
		$exist_content_footer = true;
	}*/

	$link_pdf  	= HTTP.site_url().'/pdf/LumoComp3-web-en.pdf';
	$link_pdf2  	= HTTP.site_url().'/pdf/LumoComp3-web-id.pdf';

	add_variable('link_pdf',$link_pdf);
	add_variable('link_pdf2',$link_pdf2);

	$phone_number  = get_meta_data('ci_phone_number');
	if(trim($phone_number)!=""){
		add_variable('phone_number',"<p>$phone_number</p>");
		$AddressSosmedFooterBlock = true;
	}

	$email  = get_meta_data('ci_email');
	if(trim($email)!=""){
		add_variable('email',"<p><a href=\"mailto:$email\">$email</a></p>");
		$AddressSosmedFooterBlock = true;
	}



	if($exist_content_footer)parse_template('footersContainerBlock','footcontainerblock',true);

	parse_template('footersBlock','footblock',false);
	return return_template('footers');
}
?>
