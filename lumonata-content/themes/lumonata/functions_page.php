<?php 
if(is_page("page_name=contact")){
	add_actions('thecontent', 'get_content_contact_page');
}elseif(is_page("page_name=contact-here")){
	add_actions('thecontent', 'get_content_contactcatalog_page');
}elseif(is_page("page_name=instant-catalog")){
	add_actions('thecontent', 'get_content_onlinecatalog_page');
}elseif(is_page("page_name=katalog-online")){
	add_actions('thecontent', 'get_content_katalogonline_page');
}elseif(is_blog_not_ready()){
	add_actions('thecontent', 'get_content_blog_not_ready_page');
}
elseif(is_page()){
	if(get_appname()=="pages"){
		add_actions('thecontent', 'get_content_page');
	}			
}

function is_blog_not_ready(){
	$cek_url = cek_url();
	if(isset($cek_url[0]) && $cek_url[0]=='blog-not-ready')return true;
	else return false;
}


function get_content_page(){
	global  $db;
	global $actions;
	$cek_url = cek_url();
	$q = $db->prepare_query("select a.larticle_title,a.larticle_content from lumonata_articles a where a.larticle_type=%s and a.larticle_status=%s and a.lsef=%s",'pages','publish',$cek_url[0]);
	//echo $q;
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	$sub_title ="&nbsp;"; 
	if($n>0){
		$dt = $db->fetch_array($r);
		set_template(TEMPLATE_PATH.'/page.html','page');
		add_block('pageBlock','page_block','page');
		
		add_variable('title',$dt['larticle_title']);
		add_variable('content',$dt['larticle_content']);
		add_variable('custom_content',custom_content_page($cek_url[0]));
		
		parse_template('pageBlock','page_block',false);
		return return_template('page');
	}else{
		
		return page_not_found();
	}
}

function custom_content_page($page){
	if($page=='about') return custom_content_about_us();
}

function custom_content_about_us(){
	global $db;
	
	
	set_template(TEMPLATE_PATH.'/about.html','about');
	
	$mime_type = get_mime_type();
	$q = $db->prepare_query("select b.lattach_loc_medium,b.lattach_loc_blur,b.ltitle,b.lalt_text from lumonata_articles a inner join lumonata_attachment b on a.larticle_id=b.larticle_id
												where a.lsef = %s and a.larticle_status=%s and a.larticle_type=%s and b.mime_type IN ($mime_type) order by b.lorder ",'about','publish','pages');
	$r = $db->do_query($q);	
	$n = $db->num_rows($r);
	if($n>0){
		add_block('listTeamBlock','list_team_block','about');
		add_block('teamBlock','team_block','about');
		add_block('aboutBlock','about_block','about');
		while($dt = $db->fetch_array($r)){
			//list($width, $height, $type, $attr) = getimagesize(HTTP.SITE_URL.$dt['lattach_loc_medium']);
			add_variable('img_team',HTTP.SITE_URL.$dt['lattach_loc_medium']);
			add_variable('img_team_blur',HTTP.SITE_URL.$dt['lattach_loc_blur']);
			//add_variable('width',$width);
			//add_variable('height',$height);
			
			add_variable('name',$dt['ltitle']);
			add_variable('job',$dt['lalt_text']);
			parse_template('listTeamBlock','list_team_block',true);
			//print_r($dt);
		}
		parse_template('teamBlock','team_block',true);
		
		$qs = $db->prepare_query("select larticle_title,larticle_content from lumonata_articles where larticle_type=%s and larticle_status=%s and lsef=%s",'pages','publish','services');
		$rs =  $db->do_query($qs);
		$ns = $db->num_rows($rs);
		if($ns>0){
			$ds = $db->fetch_array($rs);	
			add_variable('title_services',$ds['larticle_title']);
			add_variable('content_services',$ds['larticle_content']);
		}
		
	}

	$q2 = $db->prepare_query("select b.lattach_loc_medium,b.lattach_loc_blur,b.ltitle,b.lalt_text from lumonata_articles a inner join lumonata_attachment b on a.larticle_id=b.larticle_id
											where a.lsef = %s and a.larticle_status=%s and a.larticle_type=%s and b.mime_type IN ($mime_type) order by b.lorder ",'clients','publish','pages');
	$r2 = $db->do_query($q2);	
	$n2 = $db->num_rows($r2);
	$html_client = '';
	while($dt2 = $db->fetch_array($r2)){
		// add_variable('img_team',HTTP.SITE_URL.$dt['lattach_loc_medium']);
		// add_variable('img_team_blur',HTTP.SITE_URL.$dt['lattach_loc_blur']);	

		$html_client .='
			<a class="each-client">
        		<img src="'.HTTP.SITE_URL.$dt2['lattach_loc_medium'].'">
        	</a>

		';
	}
	add_variable('content_client',$html_client);
	
	parse_template('aboutBlock','about_block',false);
	return return_template('about');
}

function get_content_onlinecatalog_page(){
	global $db;
	global $actions;
	set_template(TEMPLATE_PATH.'/online-catalog.html','catalog');
	add_block('catalogBlock','catalog_block','catalog');			   
	
	$actions->action['meta_title']['func_name'][0] = "Get A Website and Reach Those Who #stayathome - ".web_title();   
	$actions->action['meta_title']['args'][0] = '';

	add_variable('template_image_url',HTTP.TEMPLATE_URL.'/images/');				
	
	parse_template('catalogBlock','catalog_block',false);
	return return_template('catalog');
}

function get_content_katalogonline_page(){
	global $db;
	global $actions;
	set_template(TEMPLATE_PATH.'/katalog-online.html','catalog');
	add_block('catalogBlock','catalog_block','catalog');			   
	
	$actions->action['meta_title']['func_name'][0] = "Buat Katalog Online dan Jangkau Mereka yang #DiRumahSaja - ".web_title();   
	$actions->action['meta_title']['args'][0] = '';

	add_variable('template_image_url',HTTP.TEMPLATE_URL.'/images/');				
	
	parse_template('catalogBlock','catalog_block',false);
	return return_template('catalog');
}

function get_content_contactcatalog_page(){
	global $db;
	global $actions;
	set_template(TEMPLATE_PATH.'/contact-catalog.html','contact');
	add_block('contactBlock','contact_block','contact');
	add_variable('google_script','<script src=\'https://www.google.com/recaptcha/api.js\'></script>');
	$actions->action['meta_title']['func_name'][0] = "Contact - ".web_title();   
	$actions->action['meta_title']['args'][0] = '';
	add_variable('capcha',get_meta_data('capcha_snipset'));
	
	$telp = get_meta_data("ci_phone_number");
	if($telp!="")add_variable('telp',"Phone: <a class=\"link-telp\" href=\"tel:$telp\">$telp</a>&nbsp;<br>");	
	$email = get_meta_data("ci_email");
	if($email!="") add_variable('email',"Email: <a href=\"mailto:$email\">$email</a>");
	
	/*<a href="{ci_facebook_url}" rel="nofollow" >Facebook</a> &nbsp; &nbsp; <a >Twitter</a> &nbsp; &nbsp; <a>Google+</a> &nbsp; &nbsp; <a>Instagram</a> &nbsp; &nbsp; <a>Medium</a>*/
	/*add_variable('ci_instagram', get_meta_data("ci_instagram"));
	add_variable('ci_facebook_url', get_meta_data("ci_facebook_url"));
	add_variable('ci_google_plus', get_meta_data("ci_google_plus"));
	add_variable('ci_youtube', get_meta_data("ci_youtube"));		
	add_variable('ci_twitter_url', get_meta_data("ci_twitter_url"));
	add_variable('ci_medium_url', get_meta_data("ci_medium_url"));*/
	
	//echo FRONT_TEMPLATE_URL;
	
	$custom_css = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/normalize.css\" />
							 <link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/component.css\" />";
	add_variable('custom_css',$custom_css);
	
	
	$custom_js = "<script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/modernizr.custom.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/classie.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/progressButton.js\"></script>";
	add_variable('custom_js',$custom_js);					   
							
	$sosmed = "";
	$sosmed_array = array('ci_facebook_url'=>'Facebook','ci_twitter_url'=>'Twitter','ci_google_plus'=>'Google+','ci_instagram'=>'Instagram','ci_medium_url'=>'Medium');
	foreach($sosmed_array as $sosmed_key => $sosmed_title){
		$url = get_meta_data($sosmed_key);
		if($url!="") $sosmed .= ($sosmed!=""?"&nbsp; &nbsp;&nbsp; &nbsp;":"")."<a href=\"$url\" rel=\"nofollow\" target=\"_blank\" > $sosmed_title</a>";
	}
	add_variable('sosmed',$sosmed);
	add_variable('api_key_google_map',get_meta_data('api_key_google_map'));
	
	parse_template('contactBlock','contact_block',false);
	return return_template('contact');	
}

function get_content_contact_page(){
	global $db;
	global $actions;
	set_template(TEMPLATE_PATH.'/contact.html','contact');
	add_block('contactBlock','contact_block','contact');
	add_variable('google_script','<script src=\'https://www.google.com/recaptcha/api.js\'></script>');
	$actions->action['meta_title']['func_name'][0] = "Contact - ".web_title();   
	$actions->action['meta_title']['args'][0] = '';
	add_variable('capcha',get_meta_data('capcha_snipset'));
	
	$telp = get_meta_data("ci_phone_number");
	if($telp!="")add_variable('telp',"Phone: <a class=\"link-telp\" href=\"tel:$telp\">$telp</a>&nbsp;<br>");	
	$email = get_meta_data("ci_email");
	if($email!="") add_variable('email',"Email: <a href=\"mailto:$email\">$email</a>");
	
	/*<a href="{ci_facebook_url}" rel="nofollow" >Facebook</a> &nbsp; &nbsp; <a >Twitter</a> &nbsp; &nbsp; <a>Google+</a> &nbsp; &nbsp; <a>Instagram</a> &nbsp; &nbsp; <a>Medium</a>*/
	/*add_variable('ci_instagram', get_meta_data("ci_instagram"));
	add_variable('ci_facebook_url', get_meta_data("ci_facebook_url"));
	add_variable('ci_google_plus', get_meta_data("ci_google_plus"));
	add_variable('ci_youtube', get_meta_data("ci_youtube"));		
	add_variable('ci_twitter_url', get_meta_data("ci_twitter_url"));
	add_variable('ci_medium_url', get_meta_data("ci_medium_url"));*/
	
	//echo FRONT_TEMPLATE_URL;
	
	$custom_css = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/normalize.css\" />
							 <link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/component.css\" />";
	add_variable('custom_css',$custom_css);
	
	
	$custom_js = "<script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/modernizr.custom.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/classie.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/progressButton.js\"></script>";
	add_variable('custom_js',$custom_js);					   
							
	$sosmed = "";
	$sosmed_array = array('ci_facebook_url'=>'Facebook','ci_twitter_url'=>'Twitter','ci_google_plus'=>'Google+','ci_instagram'=>'Instagram','ci_medium_url'=>'Medium');
	foreach($sosmed_array as $sosmed_key => $sosmed_title){
		$url = get_meta_data($sosmed_key);
		if($url!="") $sosmed .= ($sosmed!=""?"&nbsp; &nbsp;&nbsp; &nbsp;":"")."<a href=\"$url\" rel=\"nofollow\" target=\"_blank\" > $sosmed_title</a>";
	}
	add_variable('sosmed',$sosmed);
	add_variable('api_key_google_map',get_meta_data('api_key_google_map'));
	//add_variable('api_key_google_map', 'AIzaSyC9mKKb71I104S_XsT8jhw1DQ2FGqtLkds' );
	
	parse_template('contactBlock','contact_block',false);
	return return_template('contact');
}

function get_content_blog_not_ready_page(){
	global $db;
	global $actions;
	
	
	$actions->action['meta_title']['func_name'][0] = "Blog Not Ready - ".web_title();   
	$actions->action['meta_title']['args'][0] = '';
	
	$custom_css = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/normalize.css\" />
							 <link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/component.css\" />";
	add_variable('custom_css',$custom_css);
	
	
	$custom_js = "<script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/modernizr.custom.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/classie.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/progressButton.js\"></script>";
	add_variable('custom_js',$custom_js);	
	
	set_template(TEMPLATE_PATH.'/blog-not-ready.html','blog-not-ready');
	add_block('blogDetailBlock','blog_detail_block','blog-not-ready');
	add_variable('google_script','<script src=\'https://www.google.com/recaptcha/api.js\'></script>');
	add_variable('capcha',get_meta_data('capcha_snipset'));
	
	parse_template('blogDetailBlock','blog_detail_block',false);
	return return_template('blog-not-ready');
}



?>