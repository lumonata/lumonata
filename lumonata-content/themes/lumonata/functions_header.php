<?php 
function headers(){
	set_template(TEMPLATE_PATH."/headers.html",'headers');
	//add_block('SosmedHeaderBlock','sosmedheadersb','headers');
	add_block('HeaderBlock','headersb','headers');
	
	add_variable('top_menu', the_menus('menuset=Top Menu&show_title=false&class=mobilenav menu clearfix'));
	
	parse_template('HeaderBlock','headersb',false);
	return return_template('headers');
}
?>