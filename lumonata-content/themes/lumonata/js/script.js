(function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:339567,hjsv:5};
    a=o.getElementsByTagName('head')[0];
    r=o.createElement('script');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');



// JavaScript Document
jQuery(document).ready(function(e) {
	headers();
	jQuery(window).resize(function(){
		set_event_menu_header();
		portfolio_detail_event();
	});
	 
	share_button();
	default_contact_form();
	home_page_event();
	
	blog_page_event();
	blog_not_ready();
	about_page_event();
	portfolio_event();
	portfolio_detail_event();
	
	
	 
	jQuery(window).load(function () {
		jQuery("h1").fitText(1, { minFontSize: '32px', maxFontSize: '60px' });
		jQuery(".content.page h1").fitText(1, { minFontSize: '32px', maxFontSize: '60px' }); 
    	jQuery('body').removeClass('not-ready').addClass('ready-load');	    


    	// STAY AT HOME COVID19
    	$('.wfh i').click(function(){
			$('.wfh').css('transform','translateY(-100%)');
			$('.header').css('top','0');

			return false;
		});

		  var getUrl          = window.location,
      		  appUrl          = getUrl.pathname.split('/')[1];
      		  console.log(appUrl);
  	      if (appUrl == 'instant-catalog' || appUrl == 'katalog-online') {
		      			$('.wfh').css('transform','translateY(-100%)');
			$('.header').css('top','0');

						$('.armlang').css('display','flex');
			$('.header .contact').css('display','none');
		  }
    });
	
});


function fbs_click(width, height) {
	var leftPosition, topPosition;
	//Allow for borders.
	leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
	//Allow for title and status bars.
	topPosition = (window.screen.height / 2) - ((height / 2) + 50);
	var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
	u=location.href;
	t=document.title;
	window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'?v=1&t='+encodeURIComponent(t),'sharer', windowFeatures);
	return false;
}

function share_button(){
	jQuery('.share_twitter').click(function(event) {
	  var width  = 575,
		  height = 400,
		  left   = ($(window).width()  - width)  / 2,
		  top    = ($(window).height() - height) / 2,
		  url    = this.href,
		  opts   = 'status=1' +
				   ',width='  + width  +
				   ',height=' + height +
				   ',top='    + top    +
				   ',left='   + left;
	  
	  window.open(url, 'twitter', opts);
   
	  return false;
	});
	
}


/*========Home Event Start==========*/
function home_page_event(){
	if(jQuery('.portfolio .container .image').length>0){
		lazy_load_image('.portfolio .container .image');
	}
}
/*========Home Event End==========*/

function about_page_event(){
	if(jQuery('.team img').length>0){
		lazy_load_image('.team img');
		jQuery(window).load(function () {
			var hash = window.location.hash.substring(1);
			if(hash!='' && hash=='services')go_to_services();	
		});
		
		jQuery("[name=goto-services]").click(function(){
			go_to_services();
		});
	}
}

function go_to_services(){
	var target = jQuery("[name=services]");
	jQuery('html,body').animate({scrollTop: target.offset().top}, 1300);
}


/*========Portfolio Event Start==========*/
function portfolio_event(){
	var fillter_container = document.getElementById("fillter-container");
	if(!!fillter_container){
		jQuery("#fillter-container a").click(function(){
			if(!jQuery(this).hasClass('current-filter')){
				var portfolio    = jQuery('#portfolio-continer');
				var current_link = jQuery(this);

           		var param = new Object;
           			param.pKEY = 'is_use_ajax';

				jQuery.ajax({
	                data: param,
	                type: 'POST',
	                dataType : 'json',
	                url: current_link.attr('href'),
	                beforeSend: function( xhr ){
						portfolio.addClass('load-image');
	                },
	                success: function(e){
	                	console.log(e);
						if( e.status=='success' )
						{
							jQuery('.works .paging').remove();
							jQuery('.works .portfolio').remove();
							jQuery('.works .filter').after(e.content);

							lazy_load_image('.portfolio .container a img');

							jQuery('#fillter-container a').removeClass('current-filter');

							current_link.addClass('current-filter');

							history.pushState( 'data', '', current_link.attr('href') );
						}
	                },
	                error: function(e){
						portfolio.removeClass('load-image');
	                }
	            });
			}
		});
	}
}

function portfolio_detail_event(){
	var ww = jQuery(window).width();
	if(jQuery('.portfolio-image-style .colum-image img').length>0){
		var class_ = ''
		if(ww>900) class_ = '.portfolio-image-style .colum-image img.large';
		else class_ = '.portfolio-image-style .colum-image img.medium';
		if(!jQuery(class_).hasClass('show')) lazy_load_image(class_);
	}
}

/*========Portfolio Event End==========*/

/*========Contact Event Start==========*/

function initMap() {
	//var myLatlng = new google.maps.LatLng(-8.715530,115.181255);
	var myLatlng = new google.maps.LatLng(-8.7155886,115.1818151);
	//var myLatlng = new google.maps.LatLng(-8.715666,115.1817416);
	var mapOptions = {
	  zoom: 16,
	  center: myLatlng
	}
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
	
	var marker = new google.maps.Marker({
		position: myLatlng,
		title:"Hello World!",
		// icon:{
		// 	url:'https://lumonata.com/lumonata-content/themes/lumonata/favicon/apple-icon-60x60.png'
		// }
	});
	// To add the marker to the map, call setMap();
	marker.setMap(map);
}

function default_contact_form(){
	var elementExists = document.getElementById("name");
	if(!!elementExists){
		document.getElementsByName("name")[0].addEventListener('keyup', clearError);	
		document.getElementsByName("email")[0].addEventListener('keyup', clearError);	
		document.getElementsByName("describe_product_service")[0].addEventListener('keyup', clearError);
		document.getElementsByName("service_interest")[0].addEventListener('keyup', clearError);
		document.getElementsByName("project_timeline")[0].addEventListener('keyup', clearError);
		document.getElementsByName("budget")[0].addEventListener('change', clearError);		
		document.getElementsByName("more_detail")[0].addEventListener('keyup', clearError);		
		//document.getElementsByName("g-recaptcha-response")[0].addEventListener('change', clearError);		
	}
	
	jQuery('.contact.page .wrapper').click(function () {
        jQuery('.contact.page .wrapper #map').css("pointer-events", "auto");
    });
    
    jQuery( ".contact.page .wrapper" ).mouseleave(function() {
      jQuery('.contact.page .wrapper #map').css("pointer-events", "none"); 
    });
	
	jQuery('.contact-send-button').click(function(e){
		e.preventDefault();
	});

	[].slice.call( document.querySelectorAll( '.contact-send-button' ) ).forEach( function( bttn ) {
		new ProgressButton( bttn, {
			callback : function( instance ) {

				//goog_report_conversion();

				var url = document.contact.site_url.value;
				var name = document.contact.name.value;
				var email = document.contact.email.value;
				var describe_product_service = document.contact.describe_product_service.value;
				var service_interest = document.contact.service_interest.value;
				var project_timeline = document.contact.project_timeline.value;
				var budget = document.contact.budget.value;
				var more_detail = document.contact.more_detail.value;
				var newsletter = 0;
				var response = document.getElementById('g-recaptcha-response').value;
				var error = 0; var scrolled=0;	
				var first_el = '';

				if(trim(name)==''){
					var el = document.getElementById('name');
					el.classList.add('error');
					first_el  = 'name';
					error++;scrolled++;
				}
				
				if(trim(email)==''){
					var el = document.getElementById('email');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'email';scrolled++;}
					error++;
				}
				
				if(trim(email)!='' && !valid_email(email)){
					var el = document.getElementById('email');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'email';scrolled++;}
					error++;
				}
				
				if(trim(describe_product_service)==''){
					var el = document.getElementById('describe_product_service');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'describe_product_service';scrolled++;}
					error++;
				}
				
				if(trim(service_interest)==''){
					var el = document.getElementById('service_interest');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'service_interest';scrolled++;}
					error++;
				}
				
				
				if(trim(project_timeline)==''){
					var el = document.getElementById('project_timeline');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'project_timeline';scrolled++;}
					error++;
				}
				
				if(trim(budget)==''){
					var el = document.getElementById('budget');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'budget';scrolled++;}
					error++;
				}
				
				if(trim(more_detail)==''){
					var el = document.getElementById('more_detail');
					el.classList.add('error');
					if(scrolled==0){first_el  = 'more_detail';scrolled++;}
					error++;
				}
				
				
				var notify = document.getElementById('notify');
					notify.classList.remove('error');
					notify.classList.remove('success');
				
				if( trim(response)=='' && error==0 )
				{
					notify.classList.add('error');

					document.getElementById('notify-text').innerHTML = 'Please verify use Google Capcha';

					error++;
				}

				if( error == 0 )
				{
					var progress = 0,

					interval = setInterval( function() {
						progress = Math.min( progress + Math.random() * 0.1, 0.8 );
						instance._setProgress( progress );

						if( progress === 1 ) {
							instance._stop(1);
							clearInterval( interval );
						}
					}, 300 );
					
					var xhttp = new XMLHttpRequest();

					xhttp.onreadystatechange = function() {
						if( xhttp.readyState == 4 && xhttp.status == 200 )
						{
							instance._stop(1);

							clearInterval( interval );

							var response  = xhttp.responseText;
							var res = JSON.parse(response);

							document.getElementById("notify-text").innerHTML = res.message;

							if( res.status=='success' )
							{
								notify.classList.add("success");
							}
							else
							{
								notify.classList.add("error");
							}

							reset_after_success();
						}
					}

					xhttp.open("POST", url+'/send-inquiry', true);
					xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					xhttp.send("pKEY=is_use_ajax&name="+name+"&email="+email+"&describe_product_service="+describe_product_service+"&service_interest="+service_interest+"&project_timeline="+project_timeline+"&budget="+budget+"&more_detail="+more_detail+"&newsletter= "+newsletter+"&response="+response+" ");	
				}
				else
				{
					instance._stop( -1 );

					jQuery('html, body').animate({ scrollTop: jQuery('#' + first_el).offset().top -100 }, 2500,function(){
						var the_el = document.getElementById( first_el );
							the_el.focus();
					});
				}
			}
		});
	});
}

/*goog_report_conversion  = function(){
	alert('woiiii google conversion')
}*/


/* <![CDATA[ */
//   goog_snippet_vars = function() {
//     var w = window;
//     w.google_conversion_id = 1010183504;
//     w.google_conversion_label = "tCyPCJjK3mIQ0NrY4QM";
//     w.google_remarketing_only = false;
//   }
//   // DO NOT CHANGE THE CODE BELOW.
//   goog_report_conversion = function(url) {
//     goog_snippet_vars();
//     window.google_conversion_format = "3";
//     window.google_is_call = true;
//     var opt = new Object();
//     opt.onload_callback = function() {
//     if (typeof(url) != 'undefined') {cont
//       window.location = url;
//     }
//   }
//   var conv_handler = window['google_trackConversion'];
//   if (typeof(conv_handler) == 'function') {
//     conv_handler(opt);
//   }
// }
/* ]]> */




function reset_after_success(){
	var array_el = ['name','email','describe_product_service','service_interest','project_timeline','budget','more_detail'];
	for	(i = 0; i < array_el.length; i++) {
		var id = array_el[i];
		//console.log(id);
		var el = document.getElementById(id); 
		//console.log(el.value);
		if(el.value!="") el.value="";
		el.classList.remove("error");
	}
}




/*========Contact Event End==========*/

/*========Blog Event Start==========*/
function filter_blog(){
	var fillter_container = document.getElementById("fillter-blog");
	if(!!fillter_container){
		//console.log('oioio');
		jQuery("#fillter-blog a").click(function(){
			if(!jQuery(this).hasClass('current-filter')){
				jQuery('.loader-blog').fadeIn(80);
				var current_link = jQuery(this);
				var href = jQuery(this).attr('href');
				//console.log(href);
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					var response  = xhttp.responseText;
					var res = JSON.parse(response);
					if(res.status=='success'){
						jQuery('.list-blog,.year-blog').remove();
						jQuery('.more-blog').remove();
						jQuery('.block-list-blog .loader-blog').after(res.content);
						/*jQuery('.works .portfolio').remove();
						jQuery('.works .filter').after(res.content);
						lazy_load_image('.portfolio .container a img');
						*/
						jQuery("#fillter-blog a").removeClass('current-filter');
						current_link.addClass('current-filter');
						jQuery('.loader-blog').fadeOut(80);
						var not_ready = document.getElementsByName("list-blog")[0];
						if(!!not_ready)not_ready.classList.remove('not-ready');
						var more_blog = document.getElementsByName("more-blog")[0];
						if(!!more_blog)more_blog.classList.remove('not-ready');
						history.pushState('data', '', href);
					}
				  }
				}
				
				xhttp.open("POST", href, true);
				xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xhttp.send("pKEY=is_use_ajax");	
			}
		});
	}
}

function more_blog(){
	
		
		
		/*jQuery("#fillter-container a").click(function(){
			if(!jQuery(this).hasClass('current-filter')){
				var portfolio = document.getElementById('portfolio-continer');
				portfolio.classList.add("load-image");
				var current_link = jQuery(this);
				
				var href = jQuery(this).attr('href');
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					var response  = xhttp.responseText;
					var res = JSON.parse(response);
					if(res.status=='success'){
						jQuery('.works .portfolio').remove();
						jQuery('.works .filter').after(res.content);
						lazy_load_image('.portfolio .container a img');
						jQuery("#fillter-container a").removeClass('current-filter');
						current_link.addClass('current-filter');
						history.pushState('data', '', href);
					}
				  }
				}
				
				xhttp.open("POST", href, true);
				xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xhttp.send("pKEY=is_use_ajax");	
			}
		});*/
	
	
	
	
	if(!jQuery('.more-blog').hasClass('not-ready')){
		jQuery('.loader-blog').fadeIn();
		var url = document.getElementsByClassName("link-more")[0].value;
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var response  = xhttp.responseText;
				var res = JSON.parse(response);
				remove_element_by_class("more-blog");
				if(res.status=='success'){	
					jQuery(".block-list-blog .list-blog:last-child").after(res.content);	
					remove_not_ready_blog_ajax();
					jQuery('.loader-blog').fadeOut();
				}
			}
		}//end xhttp.onreadystatechange
		
		xhttp.open("POST", url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("pKEY=is_use_ajax");
	}
}
function blog_lazy_load(){
	if(jQuery(".detail-blog img").length>0){
		time =0 ;
		jQuery(function() {
			  jQuery(".detail-blog img").lazyload({
				 event : "sporty",
				 load:function(){
					 var imgLoad = jQuery(this);
				
					var update = function(){
						//imgLoad.addClass('blur');			
						setTimeout(function(){
							//imgLoad.removeClass('blur');		
							//imgLoad.addClass('show');		
							imgLoad.css('height','auto');
						}, time);
						time = time+1000;
					};
					this.addEventListener('load', update, true);
				}
			});
		});
		


		jQuery(window).bind("load", function() {
			var timeout = setTimeout(function() {
				jQuery(".detail-blog img").trigger("sporty")
			}, 5000);
		});
	}	
}


function blog_page_event(){
	jQuery(window).load(function () {
		var not_ready = document.getElementsByName("list-blog")[0];
		if(!!not_ready)not_ready.classList.remove('not-ready');
		var more_blog = document.getElementsByName("more-blog")[0];
		if(!!more_blog)more_blog.classList.remove('not-ready');
    });
	filter_blog();
	
	jQuery('.search_input').keydown(function(e){
	   var ingnore_key_codes = [220];
	   if ($.inArray(e.keyCode, ingnore_key_codes) >= 0){
		  e.preventDefault();
	   }
	});
	
	
	if(jQuery('.content.blog-detail').length>0){
		blog_lazy_load();
	}
}

function remove_not_ready_blog_ajax(){
	/*var not_ready = document.getElementsByName("list-blog")[0];
	not_ready.classList.remove('not-ready');*/
	jQuery('.list-blog').removeClass('not-ready');
	var more_blog = document.getElementsByName("more-blog")[0];
	if(!!more_blog)more_blog.classList.remove('not-ready');
}

function blog_not_ready(){
	var elementExists = document.getElementById("name_newsletter");
	if(!!elementExists){
		document.getElementsByName("name_newsletter")[0].addEventListener('keyup', clearError);	
		document.getElementsByName("email_newsletter")[0].addEventListener('keyup', clearError);	
		
		[].slice.call( document.querySelectorAll( '.newsletter-button' ) ).forEach( function( bttn ) {
			new ProgressButton( bttn, {
				callback : function( instance ) {
					var url = document.signup_newsletter.site_url.value;
					var name = document.signup_newsletter.name_newsletter.value;
					var email = document.signup_newsletter.email_newsletter.value;
					var response = document.getElementById('g-recaptcha-response').value;
					
					var error = 0; var scrolled=0;	
					var first_el = '';
					if(trim(name)==''){
						var el = document.getElementById('name_newsletter');
						el.classList.add("error");
						first_el  = 'name_newsletter';
						error++;scrolled++;
					}
					
					if(trim(email)==''){
						var el = document.getElementById('email_newsletter');
						el.classList.add("error");
						if(scrolled==0){first_el  = 'email_newsletter';scrolled++;}
						error++;
					}
					
					if(trim(email)!='' && !valid_email(email)){
						var el = document.getElementById('email_newsletter');
						el.classList.add("error");
						if(scrolled==0){first_el  = 'email_newsletter';scrolled++;}
						error++;
					}
					
					/*var notify = document.getElementById('notify');
					notify.classList.remove("error");
					notify.classList.remove("success");
					*/
					if(trim(response)=="" && error==0){
						notify.classList.add("error");
						document.getElementById("notify-text").innerHTML = "Please verify use Google Capcha";
						error++;
					}
					//error =0;
					if(error==0){
						var progress = 0,
						interval = setInterval( function() {
							progress = Math.min( progress + Math.random() * 0.1, 0.8 );
							instance._setProgress( progress );
						}, 300 );
						
						var xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
						if (xhttp.readyState == 4 && xhttp.status == 200) {
							instance._stop(1);
							clearInterval( interval );
							var response  = xhttp.responseText;
							var res = JSON.parse(response);
							document.getElementById("notify-text").innerHTML = res.message;
							if(res.status=='success')	notify.classList.add("success");
							else notify.classList.add("error");
							var array_el = ['name_newsletter','email_newsletter'];
							default_reset_after_success(array_el);
						  }
						}
						xhttp.open("POST", url+'/mailchimp-suscribe', true);
						xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
						xhttp.send("pKEY=is_use_ajax&name="+name+"&email="+email+"&response="+response+" ");
						
						
					}else{
						instance._stop(-1);
						jQuery('html, body').animate({scrollTop: $=jQuery("#"+first_el).offset().top -100}, 2500,function(){
							var the_el = document.getElementById(first_el);
							the_el.focus();
						});
					}//end if error==0
					
				
					
					
				}//end callback function
			} );
		} );//end progress button
	}//end if element eksis
}


/*========Blog Event End==========*/

/*========Default Event Start==========*/


function lazy_load_image(el){
	time =0 ;
	jQuery(function() {
		  jQuery(el).lazyload({
			 event : "sporty",
			 load:function(){
				var imgLoad = jQuery(this);
				imgLoad.removeClass('blur');		
				//imgLoad.css('height','auto');
				var update = function(){
					//imgLoad.addClass('blur');			
					setTimeout(function(){
						
						imgLoad.addClass('show');		
						
					}, time);
					time = time+1000;
				};
				this.addEventListener('load', update, true);
			}
		});
	});
	


	jQuery(window).bind("load", function() {
		var timeout = setTimeout(function() {
			jQuery(el).trigger("sporty")
		}, 5000);
	});
}

function loading_bar_first(){
	// Show the progress bar 
    NProgress.start();
    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1500);        
    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
		jQuery(".header").addClass("header-ready");
    });
    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
}

function set_event_menu_header(){
	jQuery(".icon-mn").unbind("click");
	jQuery(".icon-mn").click(function() {
		//jQuery("#topmenu").toggleClass("show-topmenu");
		jQuery("#topmenu").slideToggle(300).toggleClass("show-topmenu");
		jQuery(".top-menu").toggleClass("top-animate");
		//jQuery("body").toggleClass("scroll-y-none");
		jQuery(".mid-menu").toggleClass("mid-animate");
		jQuery(".bottom-menu").toggleClass("bottom-animate");
	 });
	 
	 var ht = jQuery(".header").height();
	 var wh = jQuery(window).height();
	 var hm = wh-ht;
	 jQuery("#topmenu").css('height',hm+'px');
	 
	
}

function headers(){
	loading_bar_first();
	set_event_menu_header();
}

function clearError(){
	this.classList.remove("error");
}	

function valid_email(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function trim(str) {
	var	str = str.replace(/^\s\s*/, ''),
			ws = /\s/,
			i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i + 1);
}

function remove_element_by_class(name){
	var list = document.getElementsByClassName(name);
   for(var i = list.length - 1; 0 <= i; i--)
   if(list[i] && list[i].parentElement)
   list[i].parentElement.removeChild(list[i]);
}

function default_reset_after_success(array_el){
	for	(i = 0; i < array_el.length; i++) {
		var id = array_el[i];
		var el = document.getElementById(id); 
		//console.log(el.value);
		if(el.value!="") el.value="";
		el.classList.remove("error");
	}
}

/*========Default Event End==========*/