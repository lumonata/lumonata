<?php 
/*
	Title: Lumonata
	Preview: template.png
	Author: Adi Juliartha
	Author Url: http://www.lumonata.com
	Description:Template for Lumonata
*/	

if(isset($actions->action['is_use_ajax']) && $actions->action['is_use_ajax']['func_name'][0] || isset($_POST['pKEY'])=='is_use_ajax'){
	set_template(TEMPLATE_PATH."/ajax.html");
}else{	

	set_template(TEMPLATE_PATH."/index.html");
}
require_once('functions.php');

/* Get the site URI */
add_variable('site_url',HTTP.site_url());
add_variable('template_url',HTTP.site_url().'/lumonata-content/themes/lumonata');

add_variable('headers',headers());

$thecontent =run_actions("thecontent");
if(trim($thecontent)=="")  $thecontent = page_not_found();// echo 'dasd';;
//echo $thecontent.'#';
add_variable('contents',$thecontent);

add_variable('footers',footers());

/*Get the Web Title*/
add_variable('web_title',web_title());

/* Meta Title */
$meta_title= str_replace('.','',run_actions("meta_title"));

if(!empty($meta_title)){
	add_variable('meta_title',str_replace('.','',run_actions("meta_title")));
}else{
	if(is_category('appname=login') || is_page("page_name=login")){
		add_variable('meta_title',"Login - ".web_title());
	}elseif(is_category('appname=register') || is_page("page_name=register")){
		add_variable('meta_title',"Register - ".web_title());
	}else{
		add_variable('meta_title',str_replace('.','',get_post_title("-")).web_title());
	}
}

/* Get the tagline */
add_variable('tagline',web_tagline());

/*Meat Keywords*/
add_variable('meta_keywords',run_actions("meta_keywords"));

/*Meta Description*/
add_variable('meta_description',run_actions("meta_description"));
if (cek_url()[0] == "instant-catalog" || cek_url()[0] == "katalog-online") {
	add_variable('og_image','<meta property="og:image" content="https://lumonata.com/lumonata-content/themes/lumonata/images/headercatalog.jpg" />');
}

	add_variable('ci_text_ta', get_meta_data("ci_text_ta"));
	add_variable('ci_link_ta', get_meta_data("ci_link_ta"));

print_template(); 	
?>