<?php 

require_once('functions_header.php');

require_once('functions_home.php');

require_once('functions_page.php');

require_once('functions_blog.php');

require_once('functions_footer.php');









function validBase64($string){	

	$decoded = base64_decode($string, true);

	// Check if there is no invalid character in strin

	if(!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string)) return false;

	// Decode the string in strict mode and send the responce

	if(!base64_decode($string, true)) return false;

	// Encode and compare it to origional one

	if(base64_encode($decoded) != $string) return false;

	return true;

}



function page_not_found(){

	global $actions;

	header('HTTP/1.1 404 Not Found');

	set_template(TEMPLATE_PATH.'/404.html','page_not_found');

	add_block('pageNotFoundBlock','not_found','page_not_found');

	$actions->action['meta_title']['func_name'][0] = 'Page Not Found - '.trim(web_title());   

	$actions->action['meta_title']['args'][0] = '';

		

	$home = HTTP.SITE_URL;

	add_variable('home',$home);	

	parse_template('pageNotFoundBlock','not_found',false);

	return return_template('page_not_found');

}





?>