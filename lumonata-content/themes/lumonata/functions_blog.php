<?php

if(is_blog_detail()){
	add_actions('thecontent','blog_detail_content');
}else if(is_blog_search_redirect()){//echo 'woii';
	$key = myUrlEncode($_POST['search-key']);
	header("Location: ". HTTP.SITE_URL.'/blog/search-result/'.$key );
	//add_actions('thecontent','blog_search_redirect');
}else if(is_blog_search()){
	add_actions('thecontent','blog_search_content');
}else if(is_blog_category()){
	add_actions('thecontent','blog_category_content');
}else if(is_blog_author()){
	add_actions('thecontent','blog_author_content');
}else if(is_blog()){
	//add_actions('thecontent','get_content_blog_not_ready_page');
	//header('Location: https://'.SITE_URL.'/blog-not-ready/',true);
	add_actions('thecontent','blog_content');
}

function is_blog_author(){
	$cek_url = cek_url();
	if(isset($cek_url[0]) && $cek_url[0]=='blog' && isset($cek_url[1]) && $cek_url[1]=='author' && isset($cek_url[2]) && !empty($cek_url[2]) )return true;
	else return false;
}

function is_blog(){
	$cek_url = cek_url();
	if((isset($cek_url[0]) && $cek_url[0]=='blog' && !isset($cek_url[1]) )|| (isset($cek_url[0]) && $cek_url[0]=='blog' && isset($cek_url[1]) && validBase64($cek_url[1]) ))return true;
	else return false;
}
function is_blog_search(){
	$cek_url = cek_url();
	if(isset($cek_url[0]) && $cek_url[0]=='blog' && isset($cek_url[1]) && $cek_url[1]=='search-result' )return true;
	else return false;
}

function is_blog_search_redirect(){
	$cek_url = cek_url();//print_r($cek_url);exit;
	if(isset($cek_url[0]) && $cek_url[0]=='blog' && isset($cek_url[1]) && $cek_url[1]=='search-redirect' )return true;
	else return false;
}

//|| (isset($cek_url[1]) && validBase64(json_decode($cek_url[1]))
function is_blog_category(){
	$cek_url = cek_url();
	if(isset($cek_url[0]) && $cek_url[0]=='blog' && isset($cek_url[1]) && !validBase64($cek_url[1])){
		if(isset($cek_url[2]) && validBase64($cek_url[2])) return true;
		else if(isset($cek_url[2]) && !validBase64($cek_url[2])) return false;
		else return true;
	}else return false;
}

function is_blog_detail(){
	$cek_url = cek_url();
	if(isset($cek_url[0]) && $cek_url[0]=='blog' && isset($cek_url[1]) && !validBase64($cek_url[1]) && isset($cek_url[2]) && !validBase64($cek_url[2]) ){
		$dtsef = explode('.',$cek_url[2]);
		if(isset($dtsef[1]) && $dtsef[1]=='html') return true;
	}else return false;
}

function get_filter_blog($type='categories',$exclude=""){
	global $db;
	$filter = "";
	$q = $db->prepare_query("select lname,lsef from lumonata_rules where lgroup=%s and lcount > %d and lparent=%d  and lrule=%s order by lorder",'articles',0,0,$type);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		$links = "";
		while($dt= $db->fetch_array($r)){
			/*if($dt['lsef']!=$exclude)  $links .="&nbsp; &nbsp; <a href=\"".HTTP.SITE_URL.'/blog/'.($type_rule=='tags'?'tags/':'').$dt['lsef']."\">".$dt['lname']."</a>";
			else $links .="&nbsp; &nbsp; ".$dt['lname'];*/
			$current_link = ($dt['lsef']==$exclude? "class=\"current-filter\"":"");
			$links .="&nbsp; &nbsp; <a href=\"".HTTP.SITE_URL.'/blog/'.($type_rule=='tags'?'tags/':'').$dt['lsef']."\" $current_link  onclick=\"return false\">".$dt['lname']."</a>";
		}
		/*$all_post = "All Post";
		if($exclude!=""){
			$all_post = "<a href=\"".HTTP.SITE_URL.'/blog/'."\">All Post </a>";
		}*/
		$current_all_post = ($exclude==""?"class=\"current-filter\"":"");
		$all_post = "<a href=\"".HTTP.SITE_URL.'/blog/'."\" $current_all_post  onclick=\"return false\">All Post </a>";

		$filter = "<p id=\"fillter-blog\" class=\"text text-2\">$all_post $links</p>";
	}
	return $filter;
}

function redirect_empty_blog(){
	header('Location: '.HTTP.SITE_URL.'/blog-not-ready/',true);
}


function blog_content(){
	global $db;global $actions;
	$cek_url = cek_url();
	$view = get_meta_data('post_viewed');
	if(isset($cek_url[1])  &&  $cek_url[1]=='page' && isset($cek_url[2]) && is_numeric($cek_url[2])) {
		$page = $cek_url[2] - 1;
		$offset = $view * $page ;
		$state = $cek_url[2] ;
	 }else {
		$page = 0;
		$offset = 0;
		$state = 1;
	 }

	 $str = "select tab1.*, tab2.cat_sef, tab2.lname cat_category from
												(select a.larticle_id, a.larticle_title, a.larticle_content, a.lsef, a.lpost_date, a.lorder, a2.lusername, a2.ldisplay_name from lumonata_articles  a inner join lumonata_users a2 on a.lpost_by=a2.luser_id
												where a.larticle_status=%s and a.larticle_type=%s ) as tab1
												inner join (select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2 on tab1.larticle_id=tab2.lapp_id
												group by tab1.larticle_id  order by tab1.lorder asc";

	$qall = $db->prepare_query($str,'publish','articles');
	$rall = $db->do_query($qall);
	$nall = $db->num_rows($rall);

	$q = $db->prepare_query($str." limit %d,%d",'publish','articles',$offset,$view);
	$r = $db->do_query($q);
	$n =$db->num_rows($r);
	$list_blog = "";
	if($n>0){
		$list_blog = set_list_blog($r,$date_start,"",$curr_url);
	}else{
		redirect_empty_blog();
	}

	if(isset($_POST['pKEY']) && $_POST['pKEY']=='is_use_ajax'){
		$return  = array('status'=>($list_blog!=""?"success":"failed"),"content"=>$list_blog);
		echo json_encode($return);
	}else{
		set_template(TEMPLATE_PATH.'/blog.html','blog');
		add_block('blogBlock','blog_block','blog');
		$actions->action['meta_title']['func_name'][0] = "Blog - ".web_title();
		$actions->action['meta_title']['args'][0] = '';


		add_variable('search_panel',search_blog_panel());
		add_variable('title','Blog.');
		$filter = get_filter_blog();
		add_variable('filter',$filter);
		add_variable('list_blog',$list_blog);
		$url_blog = HTTP.SITE_URL.'/blog';
		$pagging = set_pagging_blog($url_blog,$nall,$state,$view);
		//echo $pagging;
		add_variable('pagging',$pagging);
		parse_template('blogBlock','blog_block',false);
		return return_template('blog');
	}
}

function search_blog_panel(){
	set_template(TEMPLATE_PATH.'/panel-search.html','panel_search');
	add_block('PanelSearchBlock','panel_block','panel_search');
	$action = HTTP.SITE_URL.'/blog/search-redirect/';
	add_variable('actions',$action);
	parse_template('PanelSearchBlock','panel_block',false);
	return return_template('panel_search');
}




function blog_author_content(){
	global $db;global $actions;
	$cek_url = cek_url();
	$curr_year  =0;
	$author = $cek_url[2];
	$curr_url = HTTP.SITE_URL."/blog/author/".$author;
	$view = get_meta_data('post_viewed');
	if(isset($cek_url[3])  &&  $cek_url[3]=='page' && isset($cek_url[4]) && is_numeric($cek_url[4])) {
		$page = $cek_url[4] - 1;
		$offset = $view * $page ;
		$state = $cek_url[4] ;
	 }else {
		$page = 0;
		$offset = 0;
		$state = 1;
	 }

	  $str = "select tab1.*, tab2.cat_sef, tab2.lname cat_category from
												(select a.larticle_id, a.larticle_title, a.larticle_content, a.lsef, a.lpost_date, a.lorder, a2.lusername, a2.ldisplay_name from lumonata_articles  a inner join lumonata_users a2 on a.lpost_by=a2.luser_id
												where a.larticle_status=%s and a.larticle_type=%s and a2.lusername=%s ) as tab1
												inner join (select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2 on tab1.larticle_id=tab2.lapp_id
												group by tab1.larticle_id  order by tab1.lorder asc";

	$qall = $db->prepare_query($str,'publish','articles',$author);// echo $qall;
	$rall = $db->do_query($qall);
	$nall = $db->num_rows($rall);

	$q = $db->prepare_query($str." limit %d,%d",'publish','articles',$author,$offset,$view);
	$r = $db->do_query($q);
	$n =$db->num_rows($r);
	$list_blog = "";
	if($n>0){
		$list_blog = set_list_blog($r,$date_start,"",$curr_url);
	}else{
		redirect_empty_blog();
	}

	if(isset($_POST['pKEY']) && $_POST['pKEY']=='is_use_ajax'){
		$return  = array('status'=>($list_blog!=""?"success":"failed"),"content"=>$list_blog);
		echo json_encode($return);
	}else{
		set_template(TEMPLATE_PATH.'/blog.html','blog');
		add_block('blogBlock','blog_block','blog');
		//get user data
		$qu = $db->prepare_query("select ldisplay_name from lumonata_users where lusername=%s",$author);
		$ru  = $db->do_query($qu);
		$nu = $db->num_rows($ru);

		if($nu>0){
			$dtU = $db->fetch_array($ru);
			$actions->action['meta_title']['func_name'][0] = $dtU['ldisplay_name']." - Blog - ".web_title();
		}else $actions->action['meta_title']['func_name'][0] = "Blog - ".web_title();

		$actions->action['meta_title']['args'][0] = '';

		add_variable('search_panel',search_blog_panel());
		add_variable('title','Blog.');
		$filter = get_filter_blog();
		add_variable('filter',$filter);
		add_variable('list_blog',$list_blog);
		//$url_blog = HTTP.SITE_URL.'/blog';
		$pagging = set_pagging_blog($curr_url,$nall,$state,$view);
		//echo $pagging;
		add_variable('pagging',$pagging);
		parse_template('blogBlock','blog_block',false);
		return return_template('blog');
	}
}




function set_pagging_blog($url,$nall,$state,$view){
	$np = ceil($nall/$view);
	$temp = "";
	if($np>1){
		$temp = '<div class="nav-links">';

		for($n=1; $n<=$np; $n++){
			if($n==$state) $temp .='<span class="page-numbers current">'.$n.'</span>';
			else {
				/*if($n==1)	$temp .= '<a class="page-numbers" href="'.$url.'">'.$n.'</a>';
				else */$temp .= '<a class="page-numbers" href="'.$url.'/page/'.$n.'/">'.$n.'</a>';
			}
		}

		$temp .= '</div>';
	}
	return $temp;
}


function set_list_blog($r,$current_year,$category,$curr_url){
	global $db;
	set_template(TEMPLATE_PATH.'/blog-list.html','blog-list');
	add_block('blogListContent','blog_list_content','blog-list');
	add_block('blogList','blog_list','blog-list');
	$blog_list = "";
	while($dt = $db->fetch_array($r)){//print_r($dt);
		$url_blog = HTTP.SITE_URL.'/blog';
		$url = $url_blog.'/'.$dt['cat_sef'].'/'.$dt['lsef'].'.html';
		add_variable('title',$dt['larticle_title']);
		add_variable('link',$url);
		$author_link= $url_blog.'/author/'.$dt['lusername'].'/';
		add_variable('author_link','<a href="'.$author_link.'">'.$dt['ldisplay_name'].'</a>');
		$cat_link = $url_blog.'/'.$dt['cat_sef'];
		add_variable('category_link','<a href="'.$cat_link.'">'.$dt['cat_category'].'</a>');

		$dt_content = explode('<!-- pagebreak -->',$dt['larticle_content']);
		$content = add_responsive_class($dt_content[0]);
		add_variable('content',$content);
		add_variable('date',date('d F Y',strtotime($dt['lpost_date'])));
		parse_template('blogListContent','blog_list_content',true);
		//$blog_list .= ($blog_list!=""? "&nbsp;&nbsp;<span>•</span>&nbsp;&nbsp;":"")."<a href=\"$url\"><t>$title</t></a> ";
	}//end while

	parse_template('blogList','blog_list',false);
	return return_template('blog-list');
}


function add_responsive_class($content){
	global $db;
	$content = preg_replace('/<p style=".*?">\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU',           '\1\2\3', $content);
    $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
	$document = new DOMDocument();
	libxml_use_internal_errors(true);
	$document->loadHTML(utf8_decode($content));

	$imgs = $document->getElementsByTagName('img');
	$delimiters = array("-medium","-large");

	foreach ($imgs as $img) { //print_r($img)   ;
	  $src_first = $img->getAttribute('src');
	  $res = multiexplode($delimiters,$src_first);
	  $ext  = explode('.',$res[1][0]);
	  $src_original = $res[0][0].".".$ext[1];
	  $blur_src = $res[0][0]."-blur.".$ext[1];
	  $img->setAttribute('data-original',$src_first);
	  $img->setAttribute('src',$blur_src);
	  $img->setAttribute('class','img-responsive');
	}
	$html = $document->saveHTML();
	return $html;
}

function multiexplode ($delimiters,$string) {
    $ary = explode($delimiters[0],$string);
    array_shift($delimiters);
    if($delimiters != NULL) {
        foreach($ary as $key => $val) {
             $ary[$key] = multiexplode($delimiters, $val);
        }
    }
    return  $ary;
}



function is_exist_more_blog($current_year,$cat="",$type_rule="categories"){
	global $db;

	$date_start = ($current_year-1)."0101";
	$date_end = $current_year."0101";

	if($cat!=""){
		$q = $db->prepare_query("select tab1.*,tab2.cat_sef from
												(select larticle_id,larticle_title,lsef,lorder from lumonata_articles where larticle_status=%s and larticle_type=%s and lpost_date >=%d and lpost_date <%d) as tab1
												inner join
												(select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id where c.lsef=%s order by c.lorder) as tab2
												on tab1.larticle_id=tab2.lapp_id group by tab1.larticle_id order by tab1.lorder asc",'publish','articles',$date_start,$date_end,$cat);
	}else{
		$q = $db->prepare_query("select tab1.*,tab2.cat_sef from
												(select larticle_id,larticle_title,lsef,lorder from lumonata_articles where larticle_status=%s and larticle_type=%s and lpost_date >=%d and lpost_date <%d) as tab1
												inner join
												(select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2
												on tab1.larticle_id=tab2.lapp_id group by tab1.larticle_id order by tab1.lorder asc",'publish','articles',$date_start,$date_end);

	}//echo $q;
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		return true;
	}
	else return false;

}

//function ajax_blog



function blog_category_content(){
	global $db;global $actions;
	//validate state
	$cek_url = cek_url();
	//$pagging = 1;

	$curr_year  =0;
	$sef_cat = $cek_url[1];
	$curr_url = HTTP.SITE_URL."/blog/$sef_cat";
	$view = get_meta_data('post_viewed');
	if(isset($cek_url[2])  &&  $cek_url[2]=='page' && isset($cek_url[3]) && is_numeric($cek_url[3])) {
		$page = $cek_url[3] - 1;
		$offset = $view * $page ;
		$state = $cek_url[3] ;

	 }else {
		$page = 0;
		$offset = 0;
		$state = 1;
	 }

	$str = "select tab1.*, tab2.cat_sef, tab2.lname cat_category from
												(select a.larticle_id, a.larticle_title, a.larticle_content, a.lsef, a.lpost_date, a.lorder, a2.lusername, a2.ldisplay_name from lumonata_articles  a inner join lumonata_users a2 on a.lpost_by=a2.luser_id
												inner join lumonata_rule_relationship a3 on a.larticle_id=a3.lapp_id inner join lumonata_rules a4 on a3.lrule_id=a4.lrule_id

												where a.larticle_status=%s and a.larticle_type=%s and a4.lsef=%s ) as tab1
												inner join (select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2 on tab1.larticle_id=tab2.lapp_id
												group by tab1.larticle_id  order by tab1.lorder asc";

	$qall = $db->prepare_query($str,'publish','articles',$sef_cat);
	$rall = $db->do_query($qall);
	$nall = $db->num_rows($rall);

	$q = $db->prepare_query($str." limit %d,%d",'publish','articles',$sef_cat,$offset,$view);
	$r = $db->do_query($q);
	$n =$db->num_rows($r);
	$list_blog = "";
	if($n>0){
		$list_blog = set_list_blog($r,$date_start, $sef_cat,$curr_url);
	}else{
		redirect_empty_blog();
	}

	if(isset($_POST['pKEY']) && $_POST['pKEY']=='is_use_ajax'){
		$return  = array('status'=>($list_blog!=""?"success":"failed"),"content"=>$list_blog);
		echo json_encode($return);
	}else{
		set_template(TEMPLATE_PATH.'/blog.html','blog');
		add_block('blogBlock','blog_block','blog');
		$actions->action['meta_title']['func_name'][0] = "Blog - ".web_title();
		$actions->action['meta_title']['args'][0] = '';

		add_variable('search_panel',search_blog_panel());
		add_variable('title','Blog.');
		$filter = get_filter_blog('categories',$sef_cat);
		add_variable('filter',$filter);
		add_variable('list_blog',$list_blog);
		$pagging = set_pagging_blog($curr_url,$nall,$state,$view);
		add_variable('pagging',$pagging);

		parse_template('blogBlock','blog_block',false);
		return return_template('blog');
	}


}

function blog_search_redirect(){

}

function myUrlEncode($string) {
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}




function blog_search_content(){
	global $db;
	$cek_url = cek_url();
	if(!isset($cek_url[2]) || (isset($cek_url[2]) && empty($cek_url[2]))) return;
	$val = urldecode($cek_url[2]);

	set_template(TEMPLATE_PATH.'/blog.html','blog');
	add_block('blogBlock','blog_block','blog');
	$actions->action['meta_title']['func_name'][0] = "Blog - ".web_title();
	$actions->action['meta_title']['args'][0] = '';

	add_variable('title','Blog');
	if(!empty($val)){
		$sef_cat = $cek_url[1];
		$curr_url = HTTP.SITE_URL."/blog/search-result/".$cek_url[2];
		$view = get_meta_data('post_viewed');
		if(isset($cek_url[3])  &&  $cek_url[3]=='page' && isset($cek_url[4]) && is_numeric($cek_url[4])) {
			$page = $cek_url[4] - 1;
			$offset = $view * $page ;
			$state = $cek_url[4] ;
		 }else {
			$page = 0;
			$offset = 0;
			$state = 1;
		 }

		$str = "select tab1.*,tab2.cat_sef from
												(select a1.larticle_id,a1.larticle_title, a1.larticle_content,a1.lsef,a1.lorder, a1.lpost_date, a3.lname cat_category, a4.ldisplay_name, a4.lusername from lumonata_articles a1 inner join lumonata_rule_relationship a2 on a1.larticle_id=a2.lapp_id
												inner join lumonata_rules a3 on a2.lrule_id=a3.lrule_id
												inner join lumonata_users a4 on a1.lpost_by= a4.luser_id

												where a1.larticle_status=%s and a1.larticle_type=%s and (a1.larticle_title like %s OR a1.larticle_content like %s OR a3.lname like %s  OR a4.ldisplay_name like %s)  ) as tab1
												inner join (select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2
												on tab1.larticle_id=tab2.lapp_id group by tab1.larticle_id order by tab1.lorder asc";

		$qall = $db->prepare_query($str,'publish','articles',"%$val%","%$val%","%$val%","%$val%");
		$rall = $db->do_query($qall);
		$nall =$db->num_rows($rall);

		$q = $db->prepare_query($str." limit %d,%d",'publish','articles',"%$val%","%$val%","%$val%","%$val%",$offset,$view);//echo $q;
		$r = $db->do_query($q);
		$n =$db->num_rows($r);

		$list_blog = "";
		if($n>0){
			$list_blog = set_list_blog($r,$date_start, $sef_cat,$curr_url);

			add_variable('search_value',$val);
			$pagging = set_pagging_blog($curr_url,$nall,$state,$view);
			add_variable('pagging',$pagging);
		}else{
			$list_blog = blog_not_found();
			//$list_blog = "<p class=\"article-not-found\">Article not found.</p>"	;
		}
		add_variable('search_panel',search_blog_panel());
		add_variable('list_blog',$list_blog);
	}
	parse_template('blogBlock','blog_block',false);
	return return_template('blog');//print_r($_POST);
}


function blog_not_found(){
	global $actions;
	set_template(TEMPLATE_PATH.'/blog-not-found.html','blog_not_found');
	add_block('blogNotFoundBlock','not_found','blog_not_found');

	$actions->action['meta_title']['func_name'][0] = 'Page Not Found - '.trim(web_title());
	$actions->action['meta_title']['args'][0] =

	parse_template('blogNotFoundBlock','not_found',false);
	return return_template('blog_not_found');
}

function blog_detail_content(){
	global $db;
	global $actions;
	$cek_url = cek_url();
	$dtsef = explode('.',$cek_url[2]);
	$sef  = $dtsef[0];

	$mime_type = get_mime_type();
	$q = $db->prepare_query("select tab1.*,tab2.cat_sef,tab2.cat_name,tab3.lattach_loc_medium  from
												(select a1.larticle_id,a1.larticle_title,a1.larticle_content,a1.lorder,a1.lpost_date, a2.lusername,a2.ldisplay_name,a2.lemail from lumonata_articles a1 inner join lumonata_users a2 on a1.lpost_by=a2.luser_id
												where a1.lsef=%s and a1.larticle_status=%s and a1.larticle_type=%s) as tab1
												inner join (select b.lapp_id,c.lname cat_name,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2
												on tab1.larticle_id=tab2.lapp_id
												left join (select d.larticle_id,d.lattach_loc_medium from lumonata_attachment d where d.mime_type IN ($mime_type) order by d.lorder  asc ) as tab3
												on tab1.larticle_id=tab3.larticle_id
												group by tab1.larticle_id
												",$sef,'publish','articles');

	/* $str = "select tab1.*, tab2.cat_sef, tab2.lname cat_category from
												(select a.larticle_id, a.larticle_title, a.larticle_content, a.lsef, a.lpost_date, a.lorder, a2.lusername, a2.ldisplay_name from lumonata_articles  a inner join lumonata_users a2 on a.lpost_by=a2.luser_id
												where a.larticle_status=%s and a.larticle_type=%s ) as tab1
												inner join (select b.lapp_id,c.lname,c.lsef cat_sef from lumonata_rule_relationship b inner join lumonata_rules c on b.lrule_id=c.lrule_id order by c.lorder) as tab2 on tab1.larticle_id=tab2.lapp_id
												group by tab1.larticle_id  order by tab1.lorder asc";

	$url_blog = HTTP.SITE_URL.'/blog';
		$url = $url_blog.'/'.$dt['cat_sef'].'/'.$dt['lsef'].'.html';
		add_variable('title',$dt['larticle_title']);
		add_variable('link',$url);
		$author_link= $url_blog.'/author/'.$dt['lusername'].'/';
		add_variable('author_link','<a href="'.$author_link.'">'.$dt['ldisplay_name'].'</a>');
		$cat_link = $url_blog.'/'.$dt['cat_sef'];
		add_variable('category_link','<a href="'.$cat_link.'">'.$dt['cat_category'].'</a>');

		$dt_content = explode('<!-- pagebreak -->',$dt['larticle_content']);
		$content = add_responsive_class($dt_content[0]);
		add_variable('content',$content);
		add_variable('date',date('d F Y',strtotime($dt['lpost_date'])));
		parse_template('blogListContent','blog_list_content',true);
	*/

	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		$dt = $db->fetch_array($r);//print_r($dt);
		//$content = convert_content_img($dt['larticle_content']);
		$content = add_responsive_class($dt['larticle_content']);
		$actions->action['meta_title']['func_name'][0] = $dt['larticle_title'].' - '.trim(web_title());
		$actions->action['meta_title']['args'][0] = '';

		set_template(TEMPLATE_PATH.'/blog-detail.html','blog-detail');
		add_block('blogDetailBlock','blog_detail_block','blog-detail');
		add_variable('title',$dt['larticle_title']);
		add_variable('category','<a href="'.HTTP.SITE_URL.'/blog/'.$dt['cat_sef'].'">'.$dt['cat_name'].'</a>');

		$author_link= HTTP.SITE_URL.'/blog/author/'.$dt['lusername'].'/';
		add_variable('author','<a href="'.$author_link.'">'.$dt['ldisplay_name'].'</a>');

		//add_variable('author','<a href="mailto:'.$dt['lemail'].'">'.$dt['ldisplay_name'].'</a>');
		$date = date_create($dt['lpost_date']);
		add_variable('date','<span>'.date_format($date,'d F Y').'</span>');
		add_variable('detail_blog',$content);

		add_variable('addthis_title',"<meta property=\"og:title\" content=\"".$dt['larticle_title'].' - '.trim(web_title())."\" /> ");
		add_variable('addthis_desc',"<meta property=\"og:description\" content=\"".substr(strip_tags($dt['larticle_content']),0,300)."\" /> ");
		add_variable('addthis_image',"<meta property=\"og:image\" content=\"".HTTP.SITE_URL.$dt['lattach_loc_medium']."\" />");

		$dtPrevNext = get_link_prev_next_articles_detail($dt['lorder'],'articles','blog');
		add_variable('prev',$dtPrevNext['prev']);
		add_variable('next',$dtPrevNext['next']);
		//print_r($dtPrevNext);


		$custom_css = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/normalize.css\" />
							 <link rel=\"stylesheet\" type=\"text/css\" href=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/css/component.css\" />";
		add_variable('custom_css',$custom_css);

		$custom_js = "<script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/modernizr.custom.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/classie.js\"></script>
						   <script src=\"".HTTP.TEMPLATE_URL."/js/ProgressButton/js/progressButton.js\"></script>";
		add_variable('custom_js',$custom_js);

		add_variable('google_script','<script src=\'https://www.google.com/recaptcha/api.js\'></script>');
		add_variable('capcha',get_meta_data('capcha_snipset'));

		add_variable('short_name',get_meta_data('short_name_disqus'));

		parse_template('blogDetailBlock','blog_detail_block',false);
		return return_template('blog-detail');
	}

}

function convert_content_img($content){
	$new_content = str_replace('src="','data-original="',$content);
	return $new_content;
	/*echo $content."###########################################################";
	echo $new_content; exit;*/
}


function get_link_prev_next_articles_detail($order,$type='articles',$post_type){
	global $db;
	$exist_pagging = false;
	$qprev = $db->prepare_query("select a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a.lorder < %d and a3.lparent=%d order by lorder desc limit 1 ",'publish',$type,$order,0);
	$rprev = $db->do_query($qprev);
	$nprev = $db->num_rows($rprev);
	$return = array();
	$return['prev']  ='#';
	$return['next']  ='#';

	if($nprev>0){//print_r($dtprev);
		$exist_pagging = true;
		$dtprev = $db->fetch_array($rprev);
		$return['prev'] = 	HTTP.SITE_URL.'/'.$post_type.'/'.$dtprev['sef_category'].'/'.$dtprev['lsef'].'.html';
		//add_variable("prev",'<a href="'.HTTP.SITE_URL.'/'.$type.'/'.$dtprev['sef_category'].'/'.$dtprev['lsef'].'.html'.'">Prev project</a>');
	}
	$qnext = $db->prepare_query("select a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a.lorder > %d and a3.lparent=%d order by lorder asc limit 1 ",'publish',$type,$order,0);
	$rnext = $db->do_query($qnext);
	$nnext = $db->num_rows($rnext);
	if($nnext>0){//print_r($dtnext);
		$exist_pagging = true;
		$dtnext = $db->fetch_array($rnext);
		$return['next'] = HTTP.SITE_URL.'/'.$post_type.'/'.$dtnext['sef_category'].'/'.$dtnext['lsef'].'.html';
		//add_variable("next",'<a href="'.HTTP.SITE_URL.'/'.$type.'/'.$dtnext['sef_category'].'/'.$dtnext['lsef'].'.html'.'">Next project</a>');
	}
	return $return;

}



?>
