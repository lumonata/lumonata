<?php 
if(is_home()){
	add_actions('thecontent', 'home_content');
}
function home_content(){
	global $db;
	global $actions;
	set_template(TEMPLATE_PATH."/home.html",'home');
	add_block('PortfolioBlock','portfolio_block','home');
	add_block('PortfolioListBlock','portfolio_list_block','home');
	add_block('PaggingPortfolio','pagging_portfolio_block','home');
	add_block('HomepageBlock','hmpb','home');
	
	$actions->action['meta_title']['func_name'][0] = web_title();   
	$actions->action['meta_title']['args'][0] = '';	
	//print_r($actions);
	
	$page_home = get_meta_data('page_home');
	$qhome = $db->prepare_query("select larticle_title,larticle_content from lumonata_articles where larticle_id=%d and larticle_status=%s and larticle_type=%s",$page_home,'publish','pages');
	$rhome = $db->do_query($qhome);
	$nhome = $db->num_rows($rhome);
	if($nhome>0){
		while($dthome = $db->fetch_array($rhome)){
			add_variable('title_home',$dthome['larticle_title']);
			add_variable('description_home',$dthome['larticle_content']);
		}
	}

	$limit_home =get_meta_data('post_home');

	//get list portfolio
	$q = $db->prepare_query("select * from (select a.larticle_id,a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a 
							inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
							where a.larticle_status=%s and a.larticle_type=%s and a3.lparent=%d  
							group by a.larticle_id order by a.lorder limit %d) as last_tbl order by rand()
							",'publish','portfolio',0,$limit_home);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		$idx = 1;
		while ($dt = $db->fetch_array($r)) {
			add_variable('portfolio_title',$dt['larticle_title']);
			$first_image = array();
			$list_image_portfolio = get_additional_field($dt['larticle_id'], 'list_image_portfolio', 'portfolio');
			if($list_image_portfolio!=""){
				$arr_list_image_portfolio = json_decode($list_image_portfolio,true);
				$first_image = get_first_image_on_list_image_portfolio($arr_list_image_portfolio);
			}
			
			$thumb_portfolio = "";
			$blur_porfolio = "";
			if(!empty($first_image)){
				 $thumb_portfolio = 	($idx>2? $first_image['img_t'] : $first_image['img_m']) ;//$first_image['img_t'];
				 $blur_portfolio = $first_image['img_b'];
			}
			add_variable('thumb_portfolio',$thumb_portfolio);
			add_variable('blur_portfolio',$blur_portfolio);
			
			add_variable('portfolio_detail',HTTP.SITE_URL.'/portfolio/'.$dt['lsef'].'.html');
			parse_template('PortfolioBlock','portfolio_block',true);
			$idx++;
		}
		parse_template('PortfolioListBlock','portfolio_list_block',true);

		//set pagging
		$qall = $db->prepare_query("select larticle_id from lumonata_articles where larticle_status=%s and larticle_type=%s",'publish','portfolio');
		$rall= $db->do_query($qall);
		$nall  =$db->num_rows($rall);


		if($nall>$limit_home)parse_template('PaggingPortfolio','pagging_portfolio_block',true);
	}

	parse_template('HomepageBlock','hmpb',false);
	return return_template('home');
}
?>