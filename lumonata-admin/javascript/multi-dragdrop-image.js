jQuery(document).ready(function(e) {
	drop_image_event();
	jQuery('[name=button-add-single-attachment-{iii}]').click(function(){
		var html = jQuery('#theme-li-image-{iii} .single-drop').clone();
		append_html(html);
	});
	jQuery('[name=button-add-double-attachment-{iii}]').click(function(){
		var html = jQuery('#theme-li-image-{iii} .double-drop').clone();
		append_html(html);
	});
});

var convArrToObj = function(array){
    var thisEleObj = new Object();
    if(typeof array == "object"){
        for(var i in array){
            var thisEle = convArrToObj(array[i]);
            thisEleObj[i] = thisEle;
        }
    }else {
        thisEleObj = array;
    }
    return thisEleObj;
}


function append_html(html){//console.log(html);
	if(jQuery('#list-image-portfolio-{iii} li').length>0) jQuery('#list-image-portfolio-{iii} li:last-child').after(html);
	else jQuery('#list-image-portfolio-{iii}').append(html);
	var no = jQuery('[name=button-add-single-attachment-{iii}]').val();
	no = parseInt(no)+1;
	jQuery('#list-image-portfolio-{iii} li.new').attr('rel',no);
	jQuery('[name=button-add-single-attachment-{iii}]').val(no);
	jQuery('[name=button-add-double-attachment-{iii}]').val(no);
	drop_image_event();
}

function drop_image_event(){
	//var myDropzone =  jQuery(".drop-image");
	jQuery(".drop-image").dropzone({ 
			url: "upload-media.php" ,
			sending: function(file, xhr, formData) {
			formData.append("_token", 'huhuh');
		}
	});
}

function drop_image_event_old(){
	/*jQuery('#list-image-portfolio-{iii} li.new .border-drop').each(function(i, e) {
		drop_event(i);
    });*/
	
	var post_id = jQuery('[name=post_id[{iii}]]').val();
	var textarea_id = '{iii}';
	jQuery('#drop-image-0').filedrop({
			fallback_id: 'upload_button', 
			paramname:'pic',
			maxfiles: 2,
			maxfilesize: 8,
			data: { post_id : post_id, textarea_id : textarea_id,type_post:'portfolio'},
			url: 'upload-media.php',
		uploadFinished:function(i,file,res){
			  if(res.status=='success'){
					  var thumb  = res.thumb;
					  console.log('masuk 0');
					  // border_top.remove();
					  jQuery('#drop-image-0').removeClass('load-attachment').addClass('has-image');
					  jQuery('#drop-image-0').css('background-image','url('+thumb+')');
					  jQuery('#drop-image-0').attr('rel',res.id);
					  set_textarea_list_image_portfolio();
			  }
		},
		
		error: function(err, file) {
		
		},
		beforeEach: function(file){
			if(!file.type.match(/^image\//)){
				alert('Only images are allowed!');
				return false;
			}
		},
		uploadStarted:function(i, file, len){
			jQuery('#drop-image-0').addClass('load-attachment');
		},
		
		progressUpdated: function(i, file, progress) {
			//$.data(file).find('.progress').width(progress);
		}
		
	});	
	jQuery('#drop-image-0').removeClass('no-event');
	
	
	//======================================
	
	jQuery('#drop-image-1').filedrop({
			fallback_id: 'upload_button', 
			paramname:'pic',
			maxfiles: 2,
			maxfilesize: 8,
			data: { post_id : post_id, textarea_id : textarea_id,type_post:'portfolio'},
			url: 'upload-media.php',
		uploadFinished:function(i,file,res){
			  if(res.status=='success'){
					  var thumb  = res.thumb;
					  console.log('masuk 0');
					  // border_top.remove();
					  jQuery('#drop-image-1').removeClass('load-attachment').addClass('has-image');
					  jQuery('#drop-image-1').css('background-image','url('+thumb+')');
					  jQuery('#drop-image-1').attr('rel',res.id);
					  set_textarea_list_image_portfolio();
			  }
		},
		
		error: function(err, file) {
		
		},
		beforeEach: function(file){
			if(!file.type.match(/^image\//)){
				alert('Only images are allowed!');
				return false;
			}
		},
		uploadStarted:function(i, file, len){
			jQuery('#drop-image-1').addClass('load-attachment');
		},
		
		progressUpdated: function(i, file, progress) {
			//$.data(file).find('.progress').width(progress);
		}
		
	});	
	jQuery('#drop-image-1').removeClass('no-event');
	
	//======================================
	
	jQuery('#list-image-portfolio-{iii} li.new ').removeClass('new');
	jQuery('#list-image-portfolio-{iii}').sortable({change:function(){
		set_textarea_list_image_portfolio();	
	}});
	
	
}

/*function drop_event(i){
	var drop_image = jQuery('#drop-image-'+i);
	var ele_id = '#drop-image-'+i;
	var post_id = jQuery('[name=post_id[{iii}]]').val();
	//console.log(jQuery(drop_image).parent().attr('class'));
	var textarea_id = '{iii}';
	console.log('#drop-image-'+i);
	if(jQuery('#drop-image-'+i).hasClass('no-event')){
		jQuery('#drop-image-'+i).filedrop({
				fallback_id: 'upload_button', 
				paramname:'pic',
				maxfiles: 2,
				maxfilesize: 8,
				data: { post_id : post_id, textarea_id : textarea_id,type_post:'portfolio',ele_id:ele_id},
				url: 'upload-media.php',
			uploadFinished:function(i,file,res){
				  if(res.status=='success'){
						  var thumb  = res.thumb;
						  
						  // border_top.remove();
						  jQuery(this).removeClass('load-attachment').addClass('has-image');
						  jQuery(this).css('background-image','url('+thumb+')');
						  jQuery(this).attr('rel',res.id);
						  set_textarea_list_image_portfolio();
				  }
			},
			
			error: function(err, file) {
			
			},
			beforeEach: function(file){
				if(!file.type.match(/^image\//)){
					alert('Only images are allowed!');
					return false;
				}
			},
			uploadStarted:function(i, file, len){
				jQuery(this).addClass('load-attachment');
			},
			
			progressUpdated: function(i, file, progress) {
				//$.data(file).find('.progress').width(progress);
			}
			
		});	
		jQuery('#drop-image-'+i).removeClass('no-event');
	}
	
	
	
	jQuery('#list-image-portfolio-{iii} li.new ').removeClass('new');
	jQuery('#list-image-portfolio-{iii}').sortable({change:function(){
		set_textarea_list_image_portfolio();	
	}});
}*/


function set_textarea_list_image_portfolio(){
	//list_image_portfolio
	var textarea = [];
	jQuery('#list-image-portfolio-{iii} li').each(function(index, element) {
		var style = (jQuery(this).hasClass('single-drop')?'single':'double');
		var data = [];
		jQuery(this).find('.border-drop').each(function(i, ele) {
            data[i] = jQuery(this).find('.drop-image').attr('rel');
        });
		var textarea_data =[]
		textarea_data['style'] = style;
		textarea_data['data'] = data;
		textarea.push(textarea_data);
	});
	//console.log(textarea);
	
	var obj_arr = convArrToObj(textarea);
	obj_arr = JSON.stringify(obj_arr);
	console.log(obj_arr);
	jQuery('.list_image_portfolio_{iii}').text(obj_arr);
}

function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    if (arr[i] !== undefined) rv[arr[i].id] = arr[i].name;
  return rv;
}



/*
 var uploadSizeMax = 50000000;
                            var uploadSizeTotal = 0;

                            Dropzone.options.images = {
                                //Configuration
                                maxThumbnailFilesize: 10,
                                acceptedFiles: ".jpg,.jpeg,.png",
                                autoProcessQueue: true,
                                uploadMultiple: false,
                                parallelUploads: 24,
                                maxFiles: 24,
                                addRemoveLinks: true,
                                dictRemoveFile: "x",
                                dictCancelUpload: "x",
                                dictDefaultMessage: "Drop image files here",
                                url: "<?php echo $this->getUrl('vendor/deal/filesDrop') ?>",

                                accept: function(file, done) {
                                        var isOk = true;

                                        if (!(file.type == "image/jpeg" || file.type == "image/png")) {

                                            isOk = false;
                                            alert("Error! We mostly accept JPG and PNG image files");
                                        }

                                        if (uploadSizeTotal + file.size > uploadSizeMax)
                                        {
                                            isOk = false;
                                            alert("Sorry, you have reached the max size allowed to upload (50M)");

                                            var _ref;
                                            if ((_ref = file.previewElement) != null) {
                                              _ref.parentNode.removeChild(file.previewElement);
                                            }
                                            return this._updateMaxFilesReachedClass();
                                        }

                                        if (isOk) 
                                        {
                                            //Add file size
                                            uploadSizeTotal += file.size;

                                            done();
                                        }                                     
                                },


                                sending: function(file, xhr, formData) {
                                        formData.append('form_key', '<?php echo Mage::getSingleton('core/session')->getFormKey() ?>');
                                        formData.append('vendor_id', '<?php echo $this->getVendorId() ?>');
                                },

                                success: function(file, responseText) {
                                        var input = document.createElement('input');
                                        input.type = 'hidden';
                                        input.name = 'images[]';
                                        input.value = responseText;
                                        input.id = file.size;

                                        var form = document.getElementById("newdeal");
                                        form.appendChild(input);

                                        file.previewElement.classList.add("dz-success");
                                },

                                removedfile: function(file) {
                                        remove(file.size);

                                        //Substract file size
                                        uploadSizeTotal -= file.size;

                                        var _ref;
                                        if ((_ref = file.previewElement) != null) {
                                          _ref.parentNode.removeChild(file.previewElement);
                                        }
                                        return this._updateMaxFilesReachedClass();
                                }

                            };



*/

