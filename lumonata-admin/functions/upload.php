<?
class upload{
	var $dest;
	
	//constructor
	function upload_constructor($DEST){	
			$this->dest=$DEST;
	}
	//upload file
	function upload_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME,$TEMP_FILE,$THUMB){
		if(empty($NEW_FILE_NAME)){ //using original filename
			if ($THUMB == 1) {
				$dest=$this->dest."thumbs/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 2){
				$dest=$this->dest."thumbs2/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 3){
				$dest=$this->dest."thumbs3/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 4){
				$dest=$this->dest."thumbs4/".$ORIGINAL_FILE_NAME;
			}else{
				$dest=$this->dest.$ORIGINAL_FILE_NAME;
			}
		}else{ //rename filename
			if ($THUMB == 1) {
				$dest=$this->dest."thumbs/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 2){
				$dest=$this->dest."thumbs2/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 3){
				$dest=$this->dest."thumbs3/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 4){
				$dest=$this->dest."thumbs4/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}else{
				$dest=$this->dest.$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}
		}
			$upload=move_uploaded_file($TEMP_FILE,$dest);
			if($upload){
				chmod($dest, 0644);
				return true;  
			}else{return false;}		
	}
		
	//rename filename
	function rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME){
		return $NEW_FILE_NAME.strchr($ORIGINAL_FILE_NAME,".");
	}
	//check filesize
	function file_size($FILE_SIZE,$MAX_SIZE){
		if($FILE_SIZE<=$MAX_SIZE || $FILE_SIZE=0)return true;
		else return false;
	}
	
	//delete file
	function delete_file($FILE_NAME,$NUM=0){
		
		if($NUM == 0){
			if(file_exists($this->dest.$FILE_NAME))	unlink($this->dest.$FILE_NAME);
		}elseif($NUM == 1){
			if(file_exists($this->dest."thumbs/".$FILE_NAME)) unlink($this->dest."thumbs/".$FILE_NAME);
		}elseif($NUM == 2){
			if(file_exists($this->dest."thumbs2/".$FILE_NAME)) unlink($this->dest."thumbs2/".$FILE_NAME);
		}elseif($NUM == 3){
			if(file_exists($this->dest."thumbs3/".$FILE_NAME)) unlink($this->dest."thumbs3/".$FILE_NAME);
		}elseif($NUM == 4){
			if(file_exists($this->dest."thumbs4/".$FILE_NAME)) unlink($this->dest."thumbs4/".$FILE_NAME);
		}
		return $this->dest.$FILE_NAME;
	}
	
	
	//delete thumbnails
	function delete_thumb($FILE_NAME){
		if(file_exists($this->dest.$FILE_NAME))	unlink($this->dest."thumbs/".$FILE_NAME);
	}
	
	//delete both of thumbnails
	function delete_thumb_both($FILE_NAME){
		if(file_exists($this->dest."thumbs/".$FILE_NAME)) unlink($this->dest."thumbs/".$FILE_NAME);
		if(file_exists($this->dest."thumbs2/".$FILE_NAME)) unlink($this->dest."thumbs2/".$FILE_NAME);
	}
	
	//delete file & thumbnail 1
	function delete_file_thumb($FILE_NAME){
		if( file_exists($this->dest.$FILE_NAME)) unlink($this->dest.$FILE_NAME);
		if( file_exists($this->dest."thumbs/".$FILE_NAME)) unlink($this->dest."thumbs/".$FILE_NAME);
	}
	
	//delete file, & both of thumbnails
	function delete_file_thumb_both($FILE_NAME){
		if( file_exists($this->dest.$FILE_NAME)) unlink($this->dest.$FILE_NAME);
		if( file_exists($this->dest."thumbs/".$FILE_NAME)) unlink($this->dest."thumbs/".$FILE_NAME);
		if( file_exists($this->dest."thumbs2/".$FILE_NAME)) unlink($this->dest."thumbs2/".$FILE_NAME);
	}
	
	//resize image	
	function upload_resize($ORIGINAL_FILE_NAME,$NEW_FILE_NAME,$TEMP_FILE,$FILE_EXT,$TWIDTH,$THEIGHT,$THUMB){
		
		if(empty($NEW_FILE_NAME)){ //using original filename
			if ($THUMB == 1) {
				$dest=$this->dest."thumbs/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 2){
				$dest=$this->dest."thumbs2/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 3){
				$dest=$this->dest."thumbs3/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 4){
				$dest=$this->dest."thumbs4/".$ORIGINAL_FILE_NAME;
			}else{
				$dest=$this->dest.$ORIGINAL_FILE_NAME;
			}
		}else{ //using rename filename
			if ($THUMB == 1) {
				$dest=$this->dest."thumbs/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 2){
				$dest=$this->dest."thumbs2/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 3){
				$dest=$this->dest."thumbs3/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 4){
				$dest=$this->dest."thumbs4/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}else{
				$dest=$this->dest.$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}
		}
		// This is the temporary file created by PHP
		$uploadedfile = $TEMP_FILE;

		// Create an Image from it so we can do the resize
		if ($FILE_EXT == "image/jpg"  || $FILE_EXT == "image/jpeg"  || $FILE_EXT == "image/pjpeg"){ 
			$src = imagecreatefromjpeg($uploadedfile);
		}else if ($FILE_EXT == "image/gif"){
			$src = imagecreatefromgif($uploadedfile);
		}else if ($FILE_EXT == "image/png"){
			$src = imagecreatefrompng($uploadedfile);
		}

		// Capture the original size of the uploaded image
		list($width,$height)=getimagesize($uploadedfile);
		if ($height <= $width && $width >= $TWIDTH) { // width lebih besar height
			$newwidth=$TWIDTH;
			$newheight=($height/$width)*$TWIDTH;
			if ($newheight > $THEIGHT){
				$newheight = $THEIGHT;
				$newwidth = ($width/$height)*$newheight;
			}
		}elseif($height <= $width && $width < $TWIDTH){
			$newwidth=$width;
			$newheight=($height/$width)*$newwidth;
			if ($newheight > $THEIGHT){
				$newheight = $THEIGHT;
				$newwidth = $width*$newheight/$height;
			}
		}elseif($height >= $width && $height >= $THEIGHT) {
			/*
			$newheight = $THEIGHT;
			$newwidth = ($width/$height)*$THEIGHT;
			if ($newwidth > $TWIDTH){
				$newwidth = $TWIDTH;
				$newheight = ($height/$width)*$newwidth;
			}
			*/
			
			
			$newwidth=$TWIDTH;
			$newheight=($height/$width)*$TWIDTH;
			/*if ($newheight > $THEIGHT){
				$newheight = $THEIGHT;
				$newwidth = ($width/$height)*$newheight;
			}*/
		}elseif($height >= $width && $height < $THEIGHT) {
			$newheight = $height;
			$newwidth = ($width/$height)*$newheight;
			if ($newwidth > $TWIDTH){
				$newwidth = $TWIDTH;
				$newheight = ($height/$width)*$newwidth;
			}
			
			/*
			$newwidth=$width;
			$newheight=($height/$width)*$newwidth;
			if ($newheight > $THEIGHT){
				$newheight = $THEIGHT;
				$newwidth = $width*$newheight/$height;
			}
			*/
		}
					
		$tmp=imagecreatetruecolor($newwidth,$newheight);

		// this line actually does the image resizing, copying from the original
		// image into the $tmp image
		imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
		// now write the resized image to disk. I have assumed that you want the
		// resized, uploaded image file to reside in the ./images subdirectory.
		//$filename = "images/". $_FILES['uploadfile']['name'];
		//imagejpeg($tmp,$dest,100);
		if ($FILE_EXT == "image/jpg"  || $FILE_EXT == "image/jpeg"  || $FILE_EXT == "image/pjpeg"){ 
			imagejpeg($tmp,$dest,100);
		}else if ($FILE_EXT == "image/gif"){
			imagegif($tmp,$dest);
		}else if ($FILE_EXT == "image/png"){
			imagepng($tmp,$dest);
		}

		imagedestroy($src);
		imagedestroy($tmp); //NOTE: PHP will clean up the temp file it created when the request		
// has completed.
		chmod($dest, 0644);
	}
	
	//create search engine friendly URL	
/*	function sef_url($URL){
		$karakter = array ("'","\\","/",":","*","?","<",">","`","~","!","@","#","$","%","^","&","(",")","_","+","=","|","}","{","[","]",";","\"","'",",","."," ");
		$sef_url  = $URL;
		for ($i=0;$i<count($karakter);$i++){
			$sef_url = str_replace($karakter[$i],"-",$sef_url);
			$sef_url = str_replace("--","-",$sef_url);	
		}
		$sef_url = str_replace("--","-",$sef_url);	
		$strlen = strlen($sef_url);			
		if (substr($sef_url,-1) == "-") { $sef_url = substr($sef_url,0,($strlen-1));}
		$strlen = strlen($sef_url);
		if (substr($sef_url,0,1) == "-") { $sef_url = substr($sef_url,1,$strlen); }
		return strtolower($sef_url);
	}
*/	
	//create search engine friendly URL	
	function sef_url($URL){
		$karakter = array ("\\","/",":","*","?","<",">","`","~","!","@","#","$","%","^","&","(",")","_","+","=","|","}","{","[","]",";","\"","'",",","."," ");
		$sef_url  = $URL;
		for ($i=0;$i<count($karakter);$i++){
			$sef_url = str_replace($karakter[$i],"-",$sef_url);
			$sef_url = str_replace("--","-",$sef_url);	
		}
		$sef_url = str_replace("--","-",$sef_url);	
		$strlen = strlen($sef_url);			
		if (substr($sef_url,-1) == "-") { $sef_url = substr($sef_url,0,($strlen-1));}
		$strlen = strlen($sef_url);
		if (substr($sef_url,0,1) == "-") { $sef_url = substr($sef_url,1,$strlen); }
		$sef_url = str_replace("ä","a",$sef_url);	
		$sef_url = str_replace("Ä","A",$sef_url);
		$sef_url = str_replace("é","e",$sef_url);	
		$sef_url = str_replace("ö","o",$sef_url);	
		$sef_url = str_replace("Ö","O",$sef_url);
		$sef_url = str_replace("ü","u",$sef_url);	
		$sef_url = str_replace("Ü","U",$sef_url);	
		$sef_url = str_replace("ß","B",$sef_url);
		$sef_url = str_replace("ä","a",$sef_url);	
		return strtolower($sef_url);
	}
	
	//crop image
    function cropImage($ORIGINAL_FILE_NAME,$NEW_FILE_NAME,$FILE_EXT, $nw, $nh, $THUMB) {  
  		if(empty($NEW_FILE_NAME)){//using original filename
			if ($THUMB == 1) {
				$dest=$this->dest."thumbs/".$ORIGINAL_FILE_NAME;
			}elseif ($THUMB == 2){
				$dest=$this->dest."thumbs2/".$ORIGINAL_FILE_NAME;
			}else{
				$dest=$this->dest.$ORIGINAL_FILE_NAME;
			}
			$source = $this->dest.$ORIGINAL_FILE_NAME;
		}else{ //using rename filename
			if ($THUMB == 1) {
				$dest=$this->dest."thumbs/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}elseif ($THUMB == 2){
				$dest=$this->dest."thumbs2/".$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}else{
				$dest=$this->dest.$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
			}
			$source =  $this->dest.$this->rename_file($ORIGINAL_FILE_NAME,$NEW_FILE_NAME);
		} 
			  
	  	$size = getimagesize($source);
	 	$w = $size[0];
	  	$h = $size[1]; 
	   
	  	if ($FILE_EXT == "image/jpg"  || $FILE_EXT == "image/jpeg"  || $FILE_EXT == "image/pjpeg"){ 
			$simg = imagecreatefromjpeg($source);
		}else if ($FILE_EXT == "image/gif"){
			$simg = imagecreatefromgif($source);
		}else if ($FILE_EXT == "image/png"){
			$simg = imagecreatefrompng($source);
		}   
	  
		$dimg = imagecreatetruecolor($nw, $nh);
		$wm = $w/$nw;
		$hm = $h/$nh;
		$h_height = $nh/2;
		$w_height = $nw/2;
		if($w>$h){
			$adjusted_width = $w / $hm;  
			$half_width = $adjusted_width / 2;
			$int_width = $half_width - $w_height;
			imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
		}elseif(($w <$h) || ($w == $h)){
			$adjusted_height = $h / $wm;
			$half_height = $adjusted_height / 2;
			$int_height = $half_height - $h_height;
			imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
		}else{}
	imagejpeg($dimg,$dest,100);

	}
	  	  	
}//end of class
?>