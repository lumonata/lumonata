<?  
require_once("../../../config.php");
require_once("../../../functions/db.php");
$type = $_GET['obj_val'];
$i = $_GET['i'];
if(empty($type)){
		echo "Please select Type";
}elseif ($type == 1){
	require_once("../../apps/Accommodations/accommodationCategories/accommodationCategories.php");	
	$accommodation_categories = new accommodation_categories();
	echo $accommodation_categories->select_option_ajax(0,$i);
}elseif ($type == 2){
	require_once("../../apps/Spa/spaCategories/spaCategories.php");	
	$spa_categories = new spa_categories();
	echo $spa_categories->select_option_ajax(0,$i);
}elseif ($type == 3){
	require_once("../../apps/Restaurants/restaurantCategories/restaurantCategories.php");	
	$restaurant_categories = new restaurant_categories();
	echo $restaurant_categories->select_option_ajax(0,$i);
}elseif ($type == 4){
	require_once("../../apps/Activities/activitiesCategories/activitiesCategories.php");	
	$activities_categories = new activities_categories();
	echo $activities_categories->select_option_ajax(0,$i);
}
?>