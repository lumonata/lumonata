<?
class paging{
	var $top_page;
	var $view;
	var $limit_view;
	var $num_rows;
	var $page_view;
	function constructor($top_page,$view,$num_rows,$page_view){
		$this->top_page=$top_page;
		$this->view=$view;
		$this->num_rows=$num_rows;
		$this->page_view=$page_view;
	}
	function page_count(){
		$kel=$this->num_rows/$this->view;
		if ($kel==floor($this->num_rows/$this->view)){
			return $kel;
		}
		else{
			return floor($this->num_rows/$this->view)+1;
		} 
	}
	function pagination($url){
		if ($this->num_rows == 0){
			$html = "";
		}else{
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		$html="<ul><li><a href=\"$url"."1\">First Page</a></li>";
		if ($this->top_page!=1)
		{
	
			$html.="<li><a href=\"$url".$prev."\">Prev</a></li>";
	
		}else{
	
			$html.="<li>Prev</li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li>$i</li>";
	
			}else{
	
				$html.= "<li><a href=\"$url".$i."\" >$i</a></li>";
	
			}
	
		}
	
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li><a href=\"$url".$next."\">Next</a></li>";
	
		}else{
			$html.= "<li>Next </li>";
	
		}
	
		$html.="<li><a href=\"$url".$this->page_count()."\">Last Page</a></li></ul>";
	}
		return $html;

	}
}

?>