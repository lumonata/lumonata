<?php
	ob_start();
	session_name("macavillarental");
	session_start();
	
	setcookie("member_logged_last", '', 	time()-3600 ,'/');
	setcookie("member_logged_ID", 	'', 	time()-3600 ,'/');
	setcookie("member_logged_type", '', 	time()-3600 ,'/');
	setcookie("product_id", 		'', 	time()-3600 ,'/');
	setcookie("product_type", 		'', 	time()-3600 ,'/');
	setcookie("modul", 				'', 	time()-3600 ,'/');
	
	setcookie("theOrigin", 			'', 	time()-3600 ,'/');
	setcookie("trueID", 			'', 	time()-3600 ,'/');
	
	ob_end_clean();  
	session_destroy();
	
	$next = 'https://localhost/macavillarental/member/logout/';
?>
<script type="text/javascript">
	window.location.replace("<?php echo $next; ?>");
</script>