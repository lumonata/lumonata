<?php
function is_gallery_category(){
	global $db;

	$cek_url = cek_url();
	if ($cek_url[0]=='gallery'){
		return true;
	}else{
		return false;
	}
}


function front_gallery(){
	//return "";
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='gallery';
	//set template
	set_template(PLUGINS_PATH."/gallery/template_front_gallery.html",'template_gallery');
	//set block

	//print_r($_POST);
    add_block('Bgallery_block','b_gallery','template_gallery');

    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/gallery');



	if ($cek_url[0]=='gallery'){
		if(!isset($cek_url[1])){
			if(count_rules("rule_id=62") > 0){
				/*
				$data = fetch_rule("rule_id=62");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);


				$navigation = navigation('gallery',$data);
				add_variable('navigation', $navigation);

				$rule_id=62;
				$categories =get_rule_child($rule_id,$type);
				add_variable('categories', $categories);
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);

				$urlNow='https://'.SITE_URL.'/gallery/';
				$titleNow=$data['lname']." - ".web_title();

				*/

				$data = fetch_rule("rule_id=62");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);


				$navigation = navigation('gallery',$data);
				add_variable('navigation', $navigation);

				$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
				$rule_id=$data['lrule_id'];
				$categories =json_decode(get_gallery_by_rule($rule_id,$type),true);
				$categories_content ='';
				$categories_content .=$categories['gallery_list'];

				$rule_id=62;
				$get_rule_child = get_rule_child($rule_id,$type);
				$get_rule_child='';
				if(!empty($get_rule_child)){
				$categories_content .= "<br style='clear:both;'> <br style='clear:both;'><br style='clear:both;'>".get_rule_child($rule_id,$type);
				}
				add_variable('categories', $categories_content);

				add_variable('image', $categories['gallery_hd']);
				if($categories['gallery_number']==0){
					$image =json_decode(get_front_right_image($data['lrule_id']),true);
					add_variable('image', $image['gallery_list']);
				}

				add_variable('gallery_status', 'gallery');

				$urlNow=HTTP.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['lname']." - ".web_title();
			}
		}else{
			if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);

				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);

				$rule_id=$data['lrule_id'];
				$categories =json_decode(get_gallery_by_rule($rule_id,$type),true);
				add_variable('categories', $categories['gallery_list']);

				add_variable('image', $categories['gallery_hd']);
				if($categories['gallery_number']==0){
					$image =json_decode(get_front_right_image($data['lrule_id']),true);
					add_variable('image', $image['gallery_list']);
				}

				add_variable('gallery_status', 'gallery');

				$urlNow=HTTP.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['lname']." - ".web_title();

			}
		}
	}

    parse_template('Bgallery_block','b_gallery',false);

	if(isset($_POST['pKEY'])=='is_use_ajax' && isset($_POST['pKEY2nd'])=='resize_image'){
		$w = $_POST['w'];
		$h = $_POST['h'];
		$href = $_POST['href'];


		$image_tmp=$href;
		$size = getimagesize($image_tmp);


		$width = $size[0];
		$height = $size[1];
		$m_top = 0;
		$m_left = 0;
		//console.log(width+' '+height);
		if($width > $w){
			$sizeSTR = "w=$w";
			$href = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.$href;
		}else if(height > h){
			$sizeSTR = "h=$h";
			$href = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.$href;
		}


		$image_tmp=$href;
		$size = getimagesize($image_tmp);

		$height = $size[1];
		$m_top = ($h - $height) / 2;
		$width = $size[0];
		$m_left = ($w - $width) / 2;
		$style = 'style="margin-left: '.$m_left.'px; margin-top: '.$m_top.'px;"';

		$image='<img src="'.$href.'" class="aktif" '.$style.'>';

    	$value=array('content'=>return_template('template_gallery'),'imageNew'=>$image,'href'=>$_POST['href']);
    	return json_encode($value);
    }

	if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_gallery'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_gallery');
    }
}

function get_gallery_by_rule($rule_id,$type){
	global $db;
	$type_url=$type;
	$metaKey = 'gallery';

	$sql=$db->prepare_query("SELECT a.*
                                        FROM lumonata_articles a,lumonata_rule_relationship b
                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
                                        ORDER BY a.lorder",$type,'publish',$rule_id);
	 $r=$db->do_query($sql);
	 $n=$db->num_rows($r);
	 $categories='';
	 if($n > 0){
		 $categories = '<ul  class="gallery-list">';
		 $no=0;
		 $aktif='';
		 $gallery_hd_temp='';
		 $gallery_hd='';
		 while($data=$db->fetch_array($r)){
		 	$image = get_additional_field($data['larticle_id'], 'image', $metaKey);
			$sizeSTR = 'w=140&h=97';
			$url = HTTP.SITE_URL.'/lumonata-plugins/gallery/files/thumbs/'.$image;
			$url_thumb = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.HTTP.SITE_URL.'/lumonata-plugins/gallery/files/thumbs2/'.$image;

		 	if($no==0){
		 		$aktif='aktif';
		 		$gallery_hd.='<img src="'.$url.'" class="aktif">';
		 	}else{
		 		$aktif='';
		 	}
		 	$gallery_hd_temp.='<img rel="'.$url.'" class="image_temp">';

		 	$categories .= '<li rel="'.$data['larticle_id'].'"><a rel="'.$no.'" href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list '.$aktif.'"><img src="'.$url_thumb.'" title="'.$data['larticle_title'].'"></a></li>';
		 	/*
		 	$categories .= '<li rel="'.$data['larticle_id'].'"><a href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list"><img src="'.$url_thumb.'" title="'.$data['larticle_title'].'"></a></li>';
		 	$categories .= '<li rel="'.$data['larticle_id'].'"><a href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list"><img src="'.$url_thumb.'" title="'.$data['larticle_title'].'"></a></li>';
		 	$categories .= '<li rel="'.$data['larticle_id'].'"><a href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list"><img src="'.$url_thumb.'" title="'.$data['larticle_title'].'"></a></li>';
		 	$categories .= '<li rel="'.$data['larticle_id'].'"><a href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list"><img src="'.$url_thumb.'" title="'.$data['larticle_title'].'"></a></li>';
		 	*/
		 	$no++;
		 }
		 $categories .= '</ul>';
	 }


	$display_none='';
	$display_none2='';
	if($n==0){
		$display_none= 'display_none';
	}else if($n==1){
		$display_none2='display_none';
	}
	$gallery_hd_temp='
					<div class="image-block-temp" style="display:none;">
                    	'.$gallery_hd_temp.'
                    </div>
	';
	//$gallery_hd_temp='';
	$gallery_hd='
		<div class="slider" id="slider">
                    	<div class="slider-mask">
                    	<span class="loading"></span>
                    	</div>
                    	<div class="topright '.$display_none.'" style="">
                    	<span class="number"><label>1</label> / '.$n.'</span>
                    	<a class="full" title="Enter Fullscreen"></a>
                    	</div>
                    	<a class="prev href_gallery_hd '.$display_none.' '.$display_none2.'" rel="prev"  style=""></a>
                    	<a class="next href_gallery_hd '.$display_none.' '.$display_none2.'" rel="next"  style=""></a>
                    	<div class="image-block">
                    		'.$gallery_hd.'
                    	</div>
                    	'.$gallery_hd_temp.'
                    </div>
	';
	 $array=array('gallery_list'=>$categories,'gallery_hd'=>$gallery_hd,'gallery_number'=>$n);
	 return json_encode($array);
}


function is_gallery_list(){
	global $db;

	$cek_url = cek_url();
	if ($cek_url[0]=='gallery'){
		return true;
	}else{
		return false;
	}
}

function is_gallery_photo_list(){
	global $db;

	$cek_url = cek_url();
	if ($cek_url[0]=='gallery' and !empty($cek_url[1]) and $cek_url[1]=='photo'){
		return true;
	}else{
		return false;
	}
}

function is_gallery_video_list(){
	global $db;

	$cek_url = cek_url();
	if ($cek_url[0]=='gallery' and !empty($cek_url[1]) and $cek_url[1]=='video'){
		return true;
	}else{
		return false;
	}
}


function gallery_list_front(){
	global $db;
	$cat='photo';
		$q=$db->prepare_query("Select * From lumonata_rules Where lsef=%s",$cat);
		$r=$db->do_query($q);
		$d=$db->fetch_array($r);
		$name=$d['lname'];
		$rule_id=$d['lrule_id'];
		$metaKey = 'gallery';

		$viewed=3;
        $cek_url = cek_url();
        if (isset($cek_url[1])){
        	$page_now = $cek_url[1];
        	$pageInt = (int)$page_now;
        }


        if(!empty($page_now)){
            $page= $page_now;
        }else{
            $page=1;
        }


        if(!empty($page_now)){
            $page= $page_now;
        }else{
            $page=1;
        }

        $limit=($page-1)*$viewed;

 		$sql_num_rows = $db->prepare_query("Select * From lumonata_rule_relationship rr
			Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
			Where rr.lrule_id=%d And a.larticle_status=%s And a.larticle_type=%s
			Order by lorder",$rule_id,'publish',$metaKey);
        $num_rows = count_rows($sql_num_rows);

		if (isset($pageInt) and $pageInt ==0){
        	header("location:".HTTP.SITE_URL."/".$metaKey."/");
        }else if (isset($pageInt) and $pageInt > $num_rows){
        	header("location:".HTTP.SITE_URL."/".$metaKey."/");
        }

        $q=$db->prepare_query("Select * From lumonata_rule_relationship rr
		Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
		Where rr.lrule_id=%d And a.larticle_status=%s And a.larticle_type=%s
		Order by lorder
		limit %d, %d
		",$rule_id,'publish',$metaKey,$limit,$viewed);
		$r=$db->do_query($q);
		$j=$db->num_rows($r);
		if (!empty($j)){
			$gallery_list='';
			while ($d=$db->fetch_array($r)){
				$image = get_additional_field($d['larticle_id'], 'image', $metaKey);
				$sizeSTR = 'w=300';
				$url = HTTP.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$url_thumb = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.HTTP.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$gallery_list .='
							<div class="image-list">
								<div class="image">
									<a href="'.$url.'" rel="gallery_photos">
									<img src="'.$url_thumb.'">
									</a>
								</div>
							</div>';
			}
		}

	$name = 'Photo Gallery';
	if ($num_rows > $viewed){
		$url = HTTP.SITE_URL.'/'.$metaKey.'/';
		$paging_front = paging_front($url,$num_rows,$page,$viewed,5);
	}else{
		$paging_front = '';
	}
	$script = "<script>
                $(document).ready(function() {
					$('a[rel=gallery_photos]').fancybox({
		  				'transitionIn'		: 'none',
		  				'transitionOut'		: 'none',
		  				'titlePosition' 	: 'over',
		  				'showNavArrows' 	: 'false',
		  				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
		  					 return '<span id=\"fancybox-title-over\">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		  				}
		  		   	});
                })
                </script>";
	$thecontent = '
			<div class="block-cotent">
                	<h1>'.$name.'</h1>
                    <div class="desc">
                    	<div class="gallery">
                    		'.$script.'
                    		'.$gallery_list.'
						</div>
                    </div>
                    <div class="navigation">
                    '.$paging_front.'
                    </div>
                </div>';
	return $thecontent;
}

function gallery_list_photo_front(){
	global $db;
	global $actions;

	$actions->action['meta_title']['func_name'][0] = 'Photo - '.web_title();
    	$actions->action['meta_title']['args'][0] = '';

	$cat='photo';
		$q=$db->prepare_query("Select * From lumonata_rules Where lsef=%s",$cat);
		$r=$db->do_query($q);
		$d=$db->fetch_array($r);
		$name=$d['lname'];
		$rule_id=$d['lrule_id'];
		$metaKey = 'gallery';

		$viewed=9;
        $cek_url = cek_url();
        if (isset($cek_url[2])){
        	$page_now = $cek_url[2];
        	$pageInt = (int)$page_now;
        }


        if(!empty($page_now)){
            $page= $page_now;
        }else{
            $page=1;
        }


        if(!empty($page_now)){
            $page= $page_now;
        }else{
            $page=1;
        }

        $limit=($page-1)*$viewed;

 		$sql_num_rows = $db->prepare_query("Select * From lumonata_rule_relationship rr
			Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
			Where rr.lrule_id=%d And a.larticle_status=%s And a.larticle_type=%s
			Order by lorder",$rule_id,'publish',$metaKey);
        $num_rows = count_rows($sql_num_rows);

		if (isset($pageInt) and $pageInt ==0){
        	header("location:".HTTP.SITE_URL."/".$metaKey."/".$cat."/");
        }else if (isset($pageInt) and $pageInt > ($num_rows / $viewed)){
        	//header("location:https://".SITE_URL."/".$metaKey."/".$cat."/");
        }

        $q=$db->prepare_query("Select * From lumonata_rule_relationship rr
		Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
		Where rr.lrule_id=%d And a.larticle_status=%s And a.larticle_type=%s
		Order by lorder
		limit %d, %d
		",$rule_id,'publish',$metaKey,$limit,$viewed);
		$r=$db->do_query($q);
		$j=$db->num_rows($r);
		if (!empty($j)){
			$gallery_list='';
			while ($d=$db->fetch_array($r)){
				$image = get_additional_field($d['larticle_id'], 'image', $metaKey);
				$sizeSTR = 'w=300';
				$url = HTTP.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$url_thumb = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.HTTP.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$gallery_list .='
							<div class="image-list">
								<div class="image">
									<a href="'.$url.'" rel="gallery_photos">
									<img src="'.$url_thumb.'">
									</a>
								</div>
							</div>';
			}
		}

	$name = 'Photo Gallery';
	if ($num_rows > $viewed){
		$url = HTTP.SITE_URL.'/'.$metaKey.'/'.$cat.'/';
		$paging_front = '<div class="navigation">'.paging_front($url,$num_rows,$page,$viewed,5).'</div>';
	}else{
		$paging_front = '';
	}
	$script = "<script>
                $(document).ready(function() {
					$('a[rel=gallery_photos]').fancybox({
		  				'transitionIn'		: 'none',
		  				'transitionOut'		: 'none',
		  				'titlePosition' 	: 'over',
		  				'showNavArrows' 	: 'false',
		  				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
		  					 return '<span id=\"fancybox-title-over\">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		  				}
		  		   	});
                })
                </script>";
	$thecontent = '
			<div class="block-cotent">
                	<h1>'.$name.'</h1>
                    <div class="desc">
                    	<div class="gallery">
                    		'.$script.'
                    		'.$gallery_list.'
						</div>
                    </div>

                    '.$paging_front.'

                </div>';
	return $thecontent;
}

function gallery_list_video_front(){
	global $db;
	global $actions;

	$actions->action['meta_title']['func_name'][0] = 'Video - '.web_title();
    	$actions->action['meta_title']['args'][0] = '';

	$cat='video';
		$q=$db->prepare_query("Select * From lumonata_rules Where lsef=%s",$cat);
		$r=$db->do_query($q);
		$d=$db->fetch_array($r);
		$name=$d['lname'];
		$rule_id=$d['lrule_id'];
		$metaKey = 'gallery';

		$viewed=3;
        $cek_url = cek_url();
        if (isset($cek_url[2])){
        	$page_now = $cek_url[2];
        	$pageInt = (int)$page_now;
        }


        if(!empty($page_now)){
            $page= $page_now;
        }else{
            $page=1;
        }


        if(!empty($page_now)){
            $page= $page_now;
        }else{
            $page=1;
        }

        $limit=($page-1)*$viewed;

 		$sql_num_rows = $db->prepare_query("Select * From lumonata_rule_relationship rr
			Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
			Where rr.lrule_id=%d And a.larticle_status=%s And a.larticle_type=%s
			Order by lorder",$rule_id,'publish',$metaKey);

        $num_rows = count_rows($sql_num_rows);

		if (isset($pageInt) and $pageInt ==0){
			//echo "M";
        	header("location:".HTTP.SITE_URL."/".$metaKey."/".$cat."/");
        }else if (isset($pageInt) and $pageInt > ($num_rows / $viewed)){
        	//echo "MM";
        	//header("location:https://".SITE_URL."/".$metaKey."/".$cat."/");
        }

        $q=$db->prepare_query("Select * From lumonata_rule_relationship rr
		Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
		Where rr.lrule_id=%d And a.larticle_status=%s And a.larticle_type=%s
		Order by lorder
		limit %d, %d
		",$rule_id,'publish',$metaKey,$limit,$viewed);
		$r=$db->do_query($q);
		$j=$db->num_rows($r);
		if (!empty($j)){
			$gallery_list='';
			$i = 0;
			while ($d=$db->fetch_array($r)){
				$article_title = $d['larticle_title'];
				$article_content = $d['larticle_content'];
				$embed = get_additional_field($d['larticle_id'], 'embed', $metaKey);
				$sizeSTR = 'w=172';
				$url = '';
				//$url = 'https://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				//$url_thumb = 'https://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=https://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$url_thumb = HTTP.SITE_URL.'/lumonata-plugins/gallery/images/ico-video-206.png';
				if (!empty($embed)){
					$url_player = get_url_embed($embed,1);
					$url_thumb = get_url_embed($embed,2);
				}
				$gallery_list .='
						<div class="video-list">
                            <div class="video">
                            		<div class="blockvideo blockvideo-'.$i.'">
                            		<script>
                						$(document).ready(function() {

                							MoveCenter_'.$i.'();
                							function MoveCenter_'.$i.'(){
	                							//var hh2=$(".blockvideo-'.$i.'").outerHeight();
												//var ww2=$(".blockvideo-'.$i.'").outerWidth();
												var hh2=155;
												var ww2=206;
												var top=(hh2 - 46)/2;
												var lef=(ww2 - 46)/2;
												$(".play-video-'.$i.'").css("left",lef+"px");
												$(".play-video-'.$i.'").css("top",top+"px");
												$(".play-video-'.$i.'").css("display","block");

											}



                						 })
                					</script>

										<img src="'.$url_thumb.'" style="width:206px;">
										<a href="'.$url_player.'" class="various iframe">
											<span class="play-video play-video-'.$i.'">
											</span>
										</a>
									</div>
                            </div>
                            <div class="desc">
                            	<h2>'.$article_title.'</h2>
                                '.$article_content.'
                            </div>
                        </div>
							';
				$i++;
			}
		}

	$name = 'Video Gallery';
	if ($num_rows > $viewed){
		$url = HTTP.SITE_URL.'/'.$metaKey.'/'.$cat.'/';
		$paging_front = '<div class="navigation">'.paging_front($url,$num_rows,$page,$viewed,5).'</div>';
	}else{
		$paging_front = '';
	}
	$script = "<script>
                $(document).ready(function() {
                	/*
					$('a[rel=gallery_video]').fancybox({
		  				'transitionIn'		: 'none',
		  				'transitionOut'		: 'none',
		  				'titlePosition' 	: 'over',
		  				'showNavArrows' 	: 'false',
		  				'hideOnContentClick' : 'false'
		  				}
		  		   	});
		  		   	*/

		  		   	$('.various').fancybox({
						'transitionIn'	: 'none',
						'transitionOut'	: 'none'
					});
                })
                </script>";
	$thecontent = '
			<div class="block-cotent">
                	<h1>'.$name.'</h1>
                    <div class="desc">
                    	<div class="gallery">
                    		'.$script.'
                    		'.$gallery_list.'
						</div>
                    </div>

                    '.$paging_front.'

                </div>';
	return $thecontent;
}

function get_url_embed($embed,$status){
	$ex = explode("http", $embed);
	//print_r($ex);
	//echo"<br/> URL ".
	$ex[1];

	$ex1 = explode("/", $ex[1]);
	//echo"<br/> / ".$ex1[2];

	if ($ex1[2]=='player.vimeo.com'){
		$ex2= explode('"', $ex1[4]);
		$url = 'https://www.vimeo.com/'.$ex2[0];
	}else if ($ex1[2]=='www.youtube.com'){
		$ex2= explode('"', $ex1[4]);
		$url = 'https://www.youtube.com/embed/'.$ex2[0];
	}


	$ex11 = explode('"', $ex[1]);
	//echo"<br/> URL Player ".
	$url_player = 'http'.$ex11[0];
	//$url_player = 'https://vimeo.com/29625069';
	$url_thumb = video_image($url);

	if ($status==1){
		return $url_player;
	}else{
		return $url_thumb;
	}

}

function video_image($url){
	$image_url = parse_url($url);
	//print_r($image_url);
	if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
		$array = explode("/", $image_url['path']);
		return "https://img.youtube.com/vi/".$array[2]."/hqdefault.jpg";
	} else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
		$hash = unserialize(file_get_contents("https://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
		return $hash[0]["thumbnail_medium"];
	}
}
?>
