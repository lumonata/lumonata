<?php
/*
    Plugin Name: Portfolio Mod
    Plugin URL: https://www.lumonata.com/
    Description: Plugin for arunna framework to add portfolio taxonomy
    Author: Andi Darmika
    Author URL: https://www.aankun.com/
    Version: 1.0

*/

function portfolioAddons(){
	add_actions("header_elements","the_portfolio_CSS");

	global $thepost;
	global $db;

	$i = $thepost->post_index;
    $post_id = $thepost->post_id;
	if($_GET['state']=='applications') $metaKey = $_GET['sub'];
	else $metaKey = $_GET['state'];

	set_template(PLUGINS_PATH."/portfolio/template.html",'portfolio');
	add_block('portfolioList','lPortfolio','portfolio');


	add_variable('iii',	$i);
	add_variable('HTTP',	  HTTP);
	add_variable('just_site_url',	  SITE_URL);
	add_variable('json_url',	  HTTP.SITE_URL.'/lumonata-plugins/portfolio/jquery.json.min.js');
	add_variable('imageFetchURL', HTTP.SITE_URL.'/lumonata-functions/ajax/ajax.getAtachedImages.php');
	add_variable('timthumbURL',   HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php');
	add_variable('postID', $post_id);

	//echo $_GET['state'];
	$clientName = get_additional_field( $post_id, 'the_project_client_name', $metaKey);
	$projURL = get_additional_field( $post_id, 'the_project_url', $metaKey);
	$othWork = get_additional_field( $post_id, 'other_work_list', $metaKey);
	$selectedThumb = get_additional_field( $post_id, 'the_selected_thumbnail', $metaKey);


	add_variable('the_project_client_name', $clientName);
	add_variable('the_project_url', $projURL);
	add_variable('other_work_list', str_replace('{','(', str_replace('}',')',$othWork)));
	add_variable('the_selected_thumbnail', $selectedThumb);

	$year = date('Y');
	$year_select = get_additional_field( $post_id, 'the_project_year', $metaKey);
	$year_options  ="";
	for($i=$year;$i>=1970;$i--){
		$year_options .=" <option value=\"$i\" ".($i==$year_select?"selected=\"selected\"" : "").">$i</option>";
	}

	add_variable('year_options',$year_options);

	//print_r($_POST);
	//echo date('F d Y - h i s',1307418412);

	parse_template('portfolioList','lPortfolio',true);
	return return_template('portfolio');
}

function link_portfolio_to_blog(){
	global $thepost;
	global $db;
	set_template(PLUGINS_PATH.'/portfolio/portfolio_to_blog.html','portfolio-2-blog-block');
	add_block('portfolio2BlogBlock','portfolio_2_blog_block','portfolio-2-blog-block');

	$i = $thepost->post_index;
    $post_id = $thepost->post_id;

	if($_GET['state']=='applications') $metaKey = $_GET['sub'];
	else $metaKey = $_GET['state'];

	$selected_blog = get_additional_field($post_id, 'article_portfolio', $metaKey);

	$q = $db->prepare_query("select larticle_id,larticle_title from lumonata_articles where larticle_type=%s and larticle_status=%s order by lorder",'articles','publish');
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	$releated_option_blog = "";
	if($n>0){
		while($dt= $db->fetch_array($r)){
			$releated_option_blog .="<option ".($selected_blog==$dt['larticle_id']? 'selected':'')." value=\"".$dt['larticle_id']."\">".$dt['larticle_title']."</option>";
		}
	}
	add_variable('releated_option_blog',$releated_option_blog);

	parse_template('portfolio2BlogBlock','portfolio_2_blog_block',false);
	return return_template('portfolio-2-blog-block');
}



function plugin_image_portfolio_attachment(){
	global $thepost;
	global $db;

	$i = $thepost->post_index;
    $post_id = $thepost->post_id;

	if($_GET['state']=='applications') $metaKey = $_GET['sub'];
	else $metaKey = $_GET['state'];

	set_template(PLUGINS_PATH."/portfolio/image_attachment_portfolio.html",'image_attachment_portfolio');
	add_block('Attachment_Portfolio','attachment_portfolio_block','image_attachment_portfolio');

	$plugin_url = HTTP.SITE_URL.'/lumonata-plugins/portfolio';
	add_variable('plugin_url',$plugin_url);
	add_actions('header_elements','get_javascript','dropzone');
	add_actions('header_elements','get_css','multi-upload.css');

	$list_image_portfolio = get_additional_field($post_id, 'list_image_portfolio', $metaKey);
	if($list_image_portfolio!=''){
		$arr_list_image_portfolio = json_decode($list_image_portfolio,true);
		$list_image_portfolio_li = set_temp_list_image_portfolio($arr_list_image_portfolio);
		add_variable('list_image_portfolio_li',$list_image_portfolio_li);
		//echo $list_image_portfolio;
		//add_variable('list_image_portfolio',"'".$list_image_portfolio."'");
	}
	//echo $list_image_portfolio;
	parse_template('Attachment_Portfolio','attachment_portfolio_block',false);
	return return_template('image_attachment_portfolio');
}

function set_temp_list_image_portfolio($dt){
	$html = "";
	foreach($dt as $li){
		$class = ($li['style']=='double'? 'double-drop':'single-drop');
		$html .= "<li class=\"$class new\">";
		$html .= temp_list_image_portfolio_drop_image($li['data']);
		$html .= "		<input type=\"button\" class=\"delete-drop-image\" />
					   </li>";
	}
	//print_r($dt);
	return $html;
}

function temp_list_image_portfolio_drop_image($dt){
	global $db;
	$html  = "";
	foreach($dt as $id){
		//echo $id.'#';
		$html_temp = "<div class=\"border-drop\">
									<div class=\"drop-image no-event\">
										<div class=\"desc-drop fallback\">
											<input name=\"pic\" type=\"file\" />
										</div>
									</div>
								</div>";
		if(!empty($id)){
			$q = $db->prepare_query("select  lattach_id,lattach_loc_thumb from lumonata_attachment where lattach_id = %d",$id);
			$r = $db->do_query($q);
			$n  =$db->num_rows($r);
			if($n>0){
				$dti = $db->fetch_array($r);
				$id = $dti['lattach_id'];
				$thumb = HTTP.SITE_URL.$dti['lattach_loc_thumb'];
				$html_temp = "<div class=\"border-drop\">
											<div class=\"drop-image no-event edit-drop-image\" data-rel=\"$id\" style=\"background-image: url($thumb);\">
												<!--<div class=\"dz-preview dz-success-load edit-dz-preview\" style=\"background-image: url($thumb);\">
												</div>-->
												<div class=\"desc-drop fallback\">
													<input name=\"pic\" type=\"file\" />
												</div>
											</div>
										</div>";
			}
		}

		$html .= $html_temp;
	}
	return $html;
}



function the_portfolio_CSS(){
	$plugin_url =  HTTP.SITE_URL.'/lumonata-plugins/portfolio';
	$text = '<style>
			#portfolioAditionalData { position:relative; }
			#portfolioAditionalData fieldset { padding:5px; position:relative; }
			#portfolioAditionalData fieldset label { display:block; padding:0px; }
			#portfolioAditionalData fieldset .textbox {
				-moz-box-sizing: border-box;
				-webkit-box-sizing : border-box;
				-o-box-sizing : border-box;
				box-sizing : border-box;
				height:32px !important;
				width:100% !important;
			}
			#portfolioAditionalData fieldset .textbox.smaller { width:45% !important; min-width:0; margin-right:10px; }

			#portfolioAditionalData span.othBlock { display:block; margin-bottom:5px; }
			#portfolioAditionalData a.justMoreButton { font-size:30px; font-weight:bold; text-decoration:none; position:absolute; right:15px; bottom:6px; display:block; line-height:1em; }
			#portfolioAditionalData a.justMoreButton:hover { color:#F60; }
			#portfolioAditionalData input.empty { color:#aaa; }

			#portfolioAditionalData span.imagesBlock,
			#portfolioAditionalData span.manageImage { display:block; float:left; width:100%; }
				#portfolioAditionalData span.imagesBlock a { width:100px; height:100px; overflow:hidden; float:left; }
			#portfolioAditionalData .clear { clear:both; }

			#portfolioAditionalData .justThumbnailWrapper { position:relative; float:left; margin:0 5px 5px 0; }
			#portfolioAditionalData span.imagesBlock a.setMainThumbnail { position:absolute; bottom:0; left:0; width:90px; height:auto; text-decoration:none; padding:5px; background:#000; color:#fff; text-align:center; font-weight:bold; display:none; }
			#portfolioAditionalData span.imagesBlock a.setMainThumbnail.selected,
			#portfolioAditionalData span.imagesBlock a.setMainThumbnail.hovered { display:block; }

			/*.button-add-single-attachment {width: 48px; height: 24px;border:none;background:url('.$plugin_url.'/img/button-add-attachment.png) no-repeat;background-size: 100px;padding: 0px;margin: 0px 5px 0 0;cursor:pointer;}
			.button-add-double-attachment {width: 48px; height: 24px;border:none;background:url('.$plugin_url.'/img/button-add-attachment.png) no-repeat;background-size: 100px;background-position: -53px 0px;padding: 0px;margin: 0px;cursor:pointer;}
			.list-images-portfolio {list-style: none;padding: 0px;margin: 0px;max-width: 600px;width: 100%;text-align: center;}
			.list-images-portfolio li legend {width: 100%;}
			.list-images-portfolio li {border: solid 1px #000;margin-bottom: 20px;height: 280px;position: relative;background-color: #eee;}
			.list-images-portfolio li:before{content: " ";position: absolute;top: 10px;left: 10px;right: 10px;bottom: 10px;border: 1px solid #000;}
			.border-drop {top: 11px;left: 11px;right: 11px;bottom: 11px;position: absolute;background-color: #fff;}
			.drop-image {width: 100%;height: 100%;position: relative;}
			.desc-drop {position: absolute;top: 50%;left: 50%;margin: -25px 0 0 -68px;}
			.double-drop .border-drop {width: 46.8%;border: solid 1px #000;}
			.double-drop .border-drop:first-child {left: inherit;}
			.list-images-portfolio li.double-drop:before {border: none;}
			.list-images-portfolio.theme {display: none;}
			.textarea-list-image-portfolio {position: fixed;left: -9999px;top: -9999px;}
			.drop-image.load-attachment{background:url('.$plugin_url.'/img/loader.gif) no-repeat center center;}
			.dsds{display:none;}*/
			</style>
			';
	return $text;
}

function get_portfolio_list(){
	$type='portfolio';
	global $db;
	set_template(TEMPLATE_PATH."/portfolio-list.html",'portfolio');

	add_block('portfolioLoop',  'lPortfolio', 'portfolio');
	add_block('portfolioBlock', 'bPortfolio', 'portfolio');

	if(!empty($args)){
		$id=post_to_id();
		if($args=='category')
			$sql=$db->prepare_query("SELECT a.*
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_type=%s AND a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder",$type,'publish',$id);
		else
			$sql=$db->prepare_query("SELECT a.*
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder",'publish',$id);


	}else{
		$sql=$db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s AND lshare_to=0 order by lorder",$type,'publish');
	}

	$r=$db->do_query($sql);
	$i=1;

	if(find_meta_data('thumb_portfolio_view_home')){ // [AM20120806]
		$n=get_meta_data('thumb_portfolio_view_home');
	}else{
		$n=6;
	}
	while($data=$db->fetch_array($r)){
		if( $i<=$n ){ // [AM20120806]
			$theBrief = explode('::breakHere::',wordwrap(strip_tags($data['larticle_content']),180,'::breakHere::'));


			//add_variable('the_thumb', getFirstImageAttached($data['larticle_id']));
			$metaKey = 'portfolio';
			$selectedThumb = get_additional_field( $data['larticle_id'], 'the_selected_thumbnail', $metaKey);
			if(!empty($selectedThumb)){
				$sizeSTR = 'h=200&w=280';
				$tbURL = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.HTTP.SITE_URL.$selectedThumb;
			}else{
				$tbURL = getFirstImageAttached($data['larticle_id']);
			}
			add_variable('the_thumb', $tbURL);



			add_variable('artilce_link', permalink($data['larticle_id']));
			add_variable('the_title', $data['larticle_title']);
			add_variable('the_brief', $theBrief[0]);

			//add_variable('post_date',date(get_date_format(),strtotime($data['lpost_date'])));
			//add_variable('the_user',the_user($data['lpost_by']));

			add_variable('the_categories',the_categories($data['larticle_id'],$data['larticle_type']));
			add_variable('the_tags',the_tags($data['larticle_id'],$data['larticle_type']));

			parse_template('portfolioLoop','lPortfolio',true);
			$i++;
		}else{
			break;
		}
	}
	//parse_template('portfolioLoop','lPortfolio',true);

	add_variable('site_url', SITE_URL);
	parse_template('portfolioBlock','bPortfolio',false);
	return return_template('portfolio');
}

function get_portfolio_page(){
	global $db;
	global $actions;
	$type='portfolio';
	$cek_url = cek_url();
	$pagging = 1;
	$type_rules = 'categories';
	if(!isset($cek_url[1]) || (isset($cek_url[1]) && is_numeric($cek_url[1]))){//all works
		$cat='all';
		$title = "Our Works";
		$curr_url = HTTP.SITE_URL."/$type";
		if(isset($cek_url[1]) && is_numeric($cek_url[1])) $pagging = $cek_url[1];
	}else if((isset($cek_url[1]) && $cek_url[1]=='tags' && !isset($cek_url[2])) || (isset($cek_url[1]) && $cek_url[1]=='tags' && isset($cek_url[2]) && is_numeric($cek_url[2])) ){//search by all tag
		$cat='all_tag';
		$type_rules  = "tags";
		if(isset($cek_url[2]) && is_numeric($cek_url[2])) $pagging = $cek_url[2];
		$curr_url = HTTP.SITE_URL."/$type/tags";
		$title = "Our Works";
		//echo 'huhuh';
	}else if((isset($cek_url[1]) && $cek_url[1]=='tags' && isset($cek_url[2]) &&  !is_numeric($cek_url[2]) && !isset($cek_url[3]))  || (isset($cek_url[1]) && $cek_url[1]=='tags' && isset($cek_url[2]) &&  !is_numeric($cek_url[2]) && isset($cek_url[3]) && is_numeric($cek_url[3]))  ){
		$cat=$cek_url[2];
		$type_rules  = "tags";
		if(isset($cek_url[3])) $pagging = $cek_url[3];
		$curr_url = HTTP.SITE_URL."/$type/tags/$cek_url[2]";
	}else if(isset($cek_url[1]) && !is_numeric($cek_url[1]) && $cek_url[1]!='tags'){//category
		$cat=$cek_url[1];
		$curr_url = HTTP.SITE_URL."/$type/".$cat;
		if(isset($cek_url[2])) $pagging = $cek_url[2];
	}


	$limit = get_meta_data('post_portfolio');
	$prPage=($limit!=""?$limit:12);

	$start = ($pagging-1) * $prPage;
	$limits = ' LIMIT '.$start.','.$prPage.' ';

	$mime_type = get_mime_type();
	$str = "";$nall=0;$q="";$qall="";
	$filter = "";
	if($cat=="all"  || $cat=="all_tag" ){
		$str = "select a.larticle_id,a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a3.lparent=%d
															group by a.larticle_id  order by a.lorder";
		$qall = $db->prepare_query($str,'publish',$type,0);//echo $qall;
		$q = $db->prepare_query($str.$limits,'publish',$type,0);
		$filter = get_filter_portfolio($type_rules,"");
		//$title = get_portfolio_category_name($cat, 'portfolio');
	}else if($type_rules=='tags'){
		$str = "select a.larticle_id,a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a3.lparent=%d and a3.lsef=%s  and a3.lrule=%s
														  group by a.larticle_id order by a.lorder";
		$qall = $db->prepare_query($str,'publish',$type,0,$cat,'tags');
		$q = $db->prepare_query($str.$limits,'publish',$type,0,$cat,'tags');
		$filter = get_filter_portfolio($type_rules,$cat);
		$title =  'Tag: '.get_portfolio_category_name($cat, 'portfolio','tags');
	}else{
		$str = "select a.larticle_id,a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
														   where a.larticle_status=%s and a.larticle_type=%s and a3.lparent=%d and a3.lsef=%s
														  group by a.larticle_id order by a.lorder";
		$qall = $db->prepare_query($str,'publish',$type,0,$cat);
		$q = $db->prepare_query($str.$limits,'publish',$type,0,$cat);

		$title =get_portfolio_category_name($cat, 'portfolio');
		$filter = get_filter_portfolio('categories',$cat);
	}

	$rall = $db->do_query($qall);
	$nall = $db->num_rows($rall);

	$r = $db->do_query($q);
	$n = $db->num_rows($r);

	if($n>0){
		$middle_content = generate_portfolio_thumb($r);
		$pagging_temp = generate_portfolio_pagging($nall,$limit,$curr_url,$pagging);
		$pagging_html = ($pagging_temp['status']=='exist'? $pagging_temp['pagging']:"") ;
		if(isset($_POST['pKEY']) && $_POST['pKEY']=='is_use_ajax'){
			$return = array("status"=>'success',"content"=>$middle_content.$pagging_html);
			echo json_encode($return);
			exit;
		}else{
			set_template(PLUGINS_PATH.'/portfolio/template_portfolio.html','portfolio');
			add_block('portfolioTheme','portfolio_theme','portfolio');
			add_variable('title',$title);

			$actions->action['meta_title']['func_name'][0] = $title." - ".web_title();
			$actions->action['meta_title']['args'][0] = '';

			add_variable('filter',$filter);
			add_variable('middle_content',$middle_content);
			add_variable('pagging',$pagging_html);
			parse_template('portfolioTheme','portfolio_theme',false);
			return return_template('portfolio');
		}
	}
}

function generate_portfolio_pagging($nall,$limit,$curr_ull,$p){
	$num_pagging = ceil($nall/$limit);
	$view_pagging = get_meta_data('post_view_pagging_portfolio');
	$return = array();
	if($num_pagging > 1){
		set_template(PLUGINS_PATH."/portfolio/template_portfolio_pagging.html",'porfolio-pagging');
		add_block('PortfolioPagging','Portfolio_Pagging','porfolio-pagging');
		//$range_start = ($p>1?$p:0);
		$range_start = 0;
		$range_end = $range_start + $view_pagging;
		$pagging = "";

		$pagging_show = 0;
		$pagging_exist = array();
		//echo $num_pagging;
		for($i=1;$i<=$num_pagging;$i++){
			if($i>=$range_start && $i<=$range_end){
				$pagging .= ($pagging!="" ? "&nbsp;&nbsp;/":"")."&nbsp;&nbsp;<a href=\"$curr_ull/$i\">$i</a>";
				array_push($pagging_exist,$i);
				$pagging_show++;
			}
		}
		add_variable('pagging',$pagging);

		/*if($pagging_show<$view_pagging){
			$min_show = $view_pagging - $pagging_show;
			$new_start = $range_start - $min_show;
			$min_pagging = "";
			//print_r($pagging_exist);
			for($i2=$min_show ; $i2<$range_start;$i2++){
				if($i2>=$new_start && $i2<$range_start && !in_array($i2,$pagging_exist)){
					$min_pagging .= "&nbsp;&nbsp;<a href=\"$curr_ull/$i2\">$i2</a>&nbsp;&nbsp;/";
					$pagging_show++;
				}
			}
			$pagging = $min_pagging.$pagging;
		}


		$range_to_start = $range_start - 1;
		if($range_start > 2 ){
			$pagging = "&nbsp;&nbsp;<a href=\"$curr_ull/1\">1</a>&nbsp;/".($num_pagging>($view_pagging+1)?"&nbsp;...&nbsp;/":"")."".$pagging;
		}else if($range_start > 1){
			//$pagging = "&nbsp;&nbsp;<a href=\"$curr_ull/1\">1</a>&nbsp;&nbsp;/".$pagging;
		}

		add_variable('pagging',$pagging);

		$range_to_end = $num_pagging - $range_end;
		if($range_to_end>1){
			add_variable('dotted','&nbsp;/&nbsp;...&nbsp;');
		}
		if($num_pagging > $range_end) add_variable('pagging_last',"&nbsp;/&nbsp;&nbsp;<a href=\"$curr_ull/$num_pagging\">$num_pagging</a>");*/

		parse_template('PortfolioPagging','Portfolio_Pagging',false);
		$return['status'] = 'exist';
		$return['pagging'] = return_template('porfolio-pagging',false);
	}else{
		$return['status'] = 'empty';
	}
	return $return;
}


/*function generate_portfolio_pagging($nall,$limit,$curr_ull,$p){
	$num_pagging = ceil($nall/$limit);
	$view_pagging = get_meta_data('post_view_pagging_portfolio');
	$return = array();
	if($num_pagging > 1){
		set_template(PLUGINS_PATH."/portfolio/template_portfolio_pagging.html",'porfolio-pagging');
		add_block('PortfolioPagging','Portfolio_Pagging','porfolio-pagging');
		$range_start = ($p>1?$p:0);
		$range_end = $range_start + $view_pagging;
		$pagging = "";

		$pagging_show = 0;
		$pagging_exist = array();
		for($i=1;$i<=$num_pagging;$i++){
			if($i>=$range_start && $i<=$range_end){
				$pagging .= ($pagging!="" ? "&nbsp;&nbsp;/":"")."&nbsp;&nbsp;<a href=\"$curr_ull/$i\">$i</a>";
				array_push($pagging_exist,$i);
				$pagging_show++;
			}
		}

		if($pagging_show<$view_pagging){
			$min_show = $view_pagging - $pagging_show;
			$new_start = $range_start - $min_show;
			$min_pagging = "";
			//print_r($pagging_exist);
			for($i2=$min_show ; $i2<$range_start;$i2++){
				if($i2>=$new_start && $i2<$range_start && !in_array($i2,$pagging_exist)){
					$min_pagging .= "&nbsp;&nbsp;<a href=\"$curr_ull/$i2\">$i2</a>&nbsp;&nbsp;/";
					$pagging_show++;
				}
			}
			$pagging = $min_pagging.$pagging;
		}


		$range_to_start = $range_start - 1;
		if($range_start > 2 ){
			$pagging = "&nbsp;&nbsp;<a href=\"$curr_ull/1\">1</a>&nbsp;/".($num_pagging>($view_pagging+1)?"&nbsp;...&nbsp;/":"")."".$pagging;
		}else if($range_start > 1){
			//$pagging = "&nbsp;&nbsp;<a href=\"$curr_ull/1\">1</a>&nbsp;&nbsp;/".$pagging;
		}

		add_variable('pagging',$pagging);

		$range_to_end = $num_pagging - $range_end;
		if($range_to_end>1){
			add_variable('dotted','&nbsp;/&nbsp;...&nbsp;');
		}
		if($num_pagging > $range_end) add_variable('pagging_last',"&nbsp;/&nbsp;&nbsp;<a href=\"$curr_ull/$num_pagging\">$num_pagging</a>");

		parse_template('PortfolioPagging','Portfolio_Pagging',false);
		$return['status'] = 'exist';
		$return['pagging'] = return_template('porfolio-pagging',false);
	}else{
		$return['status'] = 'empty';
	}
	return $return;
}*/


function generate_portfolio_thumb($r){
	global $db;
	$type='portfolio';
	set_template(PLUGINS_PATH.'/portfolio/template_portfolio_thumbs.html','portfolio-thumbs');
	add_block('ListThumbsPortfolio','list_thumbs_portfolio','portfolio-thumbs');
	add_block('ThumbsPortfolio','thumbs_portfolio','portfolio-thumbs');
	while($dt = $db->fetch_array($r)){
		//add_variable('link_portfolio',HTTP.SITE_URL."/$type/".$dt['sef_category']."/".$dt['lsef'].".html");
		add_variable('link_portfolio',HTTP.SITE_URL."/$type/".$dt['lsef'].".html");
		add_variable('title_portfolio',$dt['larticle_title']);
		//add_variable('thumb_porfolio',"https://".SITE_URL.$dt['lattach_loc_thumb']);
		//list($width_thumb, $height_thumb, $type_thumb, $attr_thumb) = getimagesize("https://".SITE_URL."/".$dt['lattach_loc_thumb']);
		//add_variable('width_thumb',$width_thumb);
		//add_variable('height_thumb',$height_thumb);
		$first_image = array();
		$list_image_portfolio = get_additional_field($dt['larticle_id'], 'list_image_portfolio', $type);
		//echo $list_image_portfolio.'##';
		if($list_image_portfolio!=""){
			$arr_list_image_portfolio = json_decode($list_image_portfolio,true);
			$first_image = get_first_image_on_list_image_portfolio($arr_list_image_portfolio,'');

		}
		//print_r($first_image);
		$thumb_portfolio = "";
		$blur_portfolio = "";
		if(!empty($first_image)){
			 $thumb_portfolio = 	$first_image['img_t'];
			 $blur_portfolio = $first_image['img_b'];
		}
		//echo $thumb_portfolio;
		add_variable('thumb_portfolio',$thumb_portfolio);
		add_variable('blur_portfolio',$blur_portfolio);
		/*if($first_image!=""){
			list($width_thumb, $height_thumb, $type_thumb, $attr_thumb) = getimagesize($first_image);
			add_variable('width_thumb',$width_thumb);
			add_variable('height_thumb',$height_thumb);
		}*/

		parse_template('ListThumbsPortfolio','list_thumbs_portfolio',true);
	}
	parse_template('ThumbsPortfolio','thumbs_portfolio',false);
	return return_template('portfolio-thumbs');
}

function get_first_image_on_list_image_portfolio($dt){
	global $db;
	$img = array();
	//$img = '';
	//print_r($dt);
	$first_id = 0;
	foreach($dt as $li){
		foreach($li['data'] as $id){
			if($first_id==0)$first_id =$id;
		}
	}
	if($first_id!=0){
		$q = $db->prepare_query("select lattach_loc_medium,lattach_loc_thumb,lattach_loc_blur from lumonata_attachment where lattach_id=%d",$first_id);
		$r = $db->do_query($q);
		$n = $db->num_rows($r);
		if($n>0){
			$dt_img = $db->fetch_array($r);
			$img['img_m'] = HTTP.SITE_URL.$dt_img['lattach_loc_medium'];
			$img['img_t'] = HTTP.SITE_URL.$dt_img['lattach_loc_thumb'];
			$img['img_b'] = HTTP.SITE_URL.$dt_img['lattach_loc_blur'];

		}
	}
	//echo $img;
	return $img;
}



function default_join_porfolio_additional_fields($join,$tab,$field,$key){
	$str = 	 "
				  $join
				  (select lapp_id,lvalue $field from lumonata_additional_fields where lkey='$key' ) as $tab on tab1.larticle_id=$tab.lapp_id";
	return $str;
}


function get_portfolio_page_details(){
	global $db;
	global $actions;
	$type = 'portfolio';
	$sef = get_portfolio_page_sef();
	//$category = get_portfolio_category_sef();
	$middle_content = "";
	if($sef!=""){
		$left_join = default_join_porfolio_additional_fields("left join","tab2","website_url","the_project_url");
		$left_join .= default_join_porfolio_additional_fields("left join","tab3","client_name","the_project_client_name");
		$left_join .= default_join_porfolio_additional_fields("left join","tab4","year","the_project_year");
		$left_join .= default_join_porfolio_additional_fields("left join","tab5","releated_blog","article_portfolio");
		$q = $db->prepare_query(" select tab1.*,tab2.website_url,tab3.client_name,tab4.year,tab5.releated_blog from
													(select a.larticle_id,a.larticle_title,a.lsef,a.larticle_content,a.lorder from lumonata_articles a
													 inner join lumonata_rule_relationship b on a.larticle_id=b.lapp_id  inner join lumonata_rules c on b.lrule_id = c.lrule_id
													 where a.larticle_status=%s and a.lsef=%s and a.larticle_type=%s ) as tab1
													$left_join",'publish',$sef,$type);//echo $q;
		$r = $db->do_query($q);
		$n = $db->num_rows($r);
		if($n>0){
			$dt  = $db->fetch_array($r);
			set_template(PLUGINS_PATH.'/portfolio/template_portfolio_detail.html','portfolio-detail');
			add_block('serviceBlock','service_block','portfolio-detail');
			add_block('paggingPortfolioDetail','pagging_portfolio_detail','portfolio-detail');
			add_block('PorfolioDetail','Porfolio_Detail','portfolio-detail');
			$title_portfolio = $dt['larticle_title'];
			add_variable('title',$title_portfolio);
			add_variable('content',$dt['larticle_content']);
			$website_url = $dt['website_url'];

			if(trim($website_url)!='')add_variable('website_url',"<p class=\"text text-10\"><a href=\"$website_url\" rel=\"nofollow\" title=\"$title_portfolio\" target=\"_blank\">Visit website &raquo;</a></p>");
			//add_variable('website_url',$dt['website_url']);
			$actions->action['meta_title']['func_name'][0] = $dt['larticle_title'];//." - ".web_title();
			$actions->action['meta_title']['args'][0] = '';

			//get attachment
			$image = "";
			$list_image_portfolio = get_additional_field($dt['larticle_id'], 'list_image_portfolio', $type);
			$first_img_medium = "";
			if($list_image_portfolio!=""){
				$arr_list_image_portfolio = json_decode($list_image_portfolio,true);
				$li_image_html = "";
				$index_image = 1;
				foreach($arr_list_image_portfolio as $li_image){
					if($index_image>1){
						$li_image_html .= "<div class=\"portfolio-style-".$li_image['style']." portfolio-image-style\">";

						foreach($li_image['data'] as $idImg){
							$colum_image = "<div class=\"colum-image\">&nbsp;</div>";
							$qatt = $db->prepare_query("select lattach_loc,lattach_loc_medium,lattach_loc_thumb,lattach_loc_blur from lumonata_attachment where lattach_id=%d ",$idImg);
							$ratt = $db->do_query($qatt);
							$natt = $db->num_rows($ratt);
							$image_colum = "";
							if($natt>0){
								$datt = $db->fetch_array($ratt);
								$url_large = HTTP.SITE_URL.$datt['lattach_loc'];
								$url_medium = HTTP.SITE_URL.$datt['lattach_loc_medium'];
								$url_blur  = HTTP.SITE_URL.$datt['lattach_loc_blur'];
								/*list($width_large, $height_large, $type_large, $attr_large) = getimagesize($url_large);
								list($width_medium, $height_medium, $type_medium, $attr_medium) = getimagesize($url_medium);*/



								$image_colum .="<img src=\"$url_blur\" data-original=\"".$url_large."\" class=\"large image blur\"    />";
								$image_colum .="<img src=\"$url_blur\"  data-original=\"".$url_medium."\" class=\"medium image blur\" />";
								//$image_colum .="<img data-original=\""."https://".SITE_URL.$datt['lattach_loc_thumb']."\" class=\"small image\" />";
								$colum_image = "<div class=\"colum-image\">$image_colum</div>";
								if($first_img_medium=='')$first_img_medium =$url_medium;
							}
							$li_image_html .= $colum_image;
							//print_r($dtImg);
						}
						$li_image_html .= "</div>";
						//print_r($li_image);
					}//end if($index_image>1)
					$index_image++;
				}//end foreach($arr_list_image_portfolio as $li_image)
				$image .= $li_image_html;//print_r($arr_list_image_portfolio);
			}
			add_variable('image',$image);


			//add_variable('addthis_title',"<meta property=\"og:title\" content=\"".$dt['larticle_title'].' - '.trim(web_title())."\" /> ");
			add_variable('addthis_title',"<meta property=\"og:title\" content=\"".$dt['larticle_title']."\" /> ");
			add_variable('addthis_desc',"<meta property=\"og:description\" content=\"".substr(strip_tags($dt['larticle_content']),0,300)."\" /> ");
			add_variable('addthis_image',"<meta property=\"og:image\" content=\"".$first_img_medium."\" />");


			//get tag
			$qtag = $db->prepare_query("select a3.lname tags, a3.lsef from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id where a.larticle_id=%d and a3.lrule=%s",$dt['larticle_id'],'tags');
			$rtag = $db->do_query($qtag);
			$ntag = $db->num_rows($rtag);
			if($ntag>0){
				$tags = "";
				while($dttag = $db->fetch_array($rtag)){
					$tags .= ($tags!=""?",&nbsp;":"")."<a href=\"".HTTP.SITE_URL."/$type/tags/".$dttag['lsef']."\" title=\"".$dttag['tags']."\">".$dttag['tags']."</a>";
				}
				add_variable('tags',$tags);
				parse_template('serviceBlock','service_block',true);
			}

			//get releated_blog
			$releated_blog = "";
			if(!empty($dt['releated_blog'])){
				//$qb = $db->prepare_query("select larticle_sef from lumonata_articles where larticle_status=%s and larticle_type=%s and larticle_id=%d",'publish','articles',$dt['releated_blog']);
				$qb = $db->prepare_query("select a.larticle_id,a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a.larticle_id = %d and a3.lparent=%d order by lorder desc limit 1 ",'publish','articles',$dt['releated_blog'],0);
															//echo $qb;
				$rb = $db->do_query($qb);
				$nb = $db->num_rows($rb);
				if($nb>0){
					$dtb = $db->fetch_array($rb);
					//print_r($dtb);
					//$blog_url = "https://".SITE_URL."/blog/""
					$blog_url= HTTP.SITE_URL.'/blog/'.$dtb['sef_category'].'/'.$dtb['lsef'].'.html';
					$blog_title = $dtb['larticle_title'];
					$releated_blog ="<p class=\"text text-10\"><a href=\"$blog_url\" title=\"$blog_title\" target=\"_blank\">Read More &raquo;</a></p>";
				}
			}
			add_variable('releated_blog',$releated_blog);

			add_variable('client_name',$dt['client_name']);
			add_variable('year',$dt['year']);
			//$current_url = HTTP.SITE_URL."/portfolio/$category/$sef.html";
			$current_url = HTTP.SITE_URL."/portfolio/$sef.html";
			add_variable('current_url',$current_url);
			$ci_email = get_meta_data('ci_email');
			if(trim($ci_email)!=''){
				add_variable('share_email',"<br><a href=\"mailto:$ci_email?body=$current_url\" rel=\"nofollow\">Email</a>");
			}
			add_variable('ci_email',get_meta_data('ci_email'));
			get_link_prev_next_portfolio_detail($dt['lorder']);

			parse_template('PorfolioDetail','Porfolio_Detail',false);
			$middle_content = return_template('portfolio-detail');
		}
	}//end sef empty

	return  $middle_content;
}

function get_link_prev_next_portfolio_detail($order){
	global $db;
	$type = 'portfolio';
	$exist_pagging = false;
	$qprev = $db->prepare_query("select a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a.lorder < %d and a3.lparent=%d order by lorder desc limit 1 ",'publish',$type,$order,0);
	$rprev = $db->do_query($qprev);
	$nprev = $db->num_rows($rprev);
	if($nprev>0){//print_r($dtprev);
		$exist_pagging = true;
		$dtprev = $db->fetch_array($rprev);
		//add_variable("prev",'<a href="'.HTTP.SITE_URL.'/'.$type.'/'.$dtprev['sef_category'].'/'.$dtprev['lsef'].'.html'.'">Prev</a>');
		add_variable("prev",'<a href="'.HTTP.SITE_URL.'/'.$type.'/'.$dtprev['lsef'].'.html'.'">Prev</a>');
	}else{
		add_variable("prev",'<a href="#" class="no-link">Prev</a>');
	}
	$qnext = $db->prepare_query("select a.larticle_title,a.lsef,a.lorder,a3.lsef sef_category from lumonata_articles a inner join lumonata_rule_relationship a2 on  a.larticle_id = a2.lapp_id  inner join lumonata_rules a3 on a2.lrule_id = a3.lrule_id
															where a.larticle_status=%s and a.larticle_type=%s and a.lorder > %d and a3.lparent=%d order by lorder asc limit 1 ",'publish',$type,$order,0);
	$rnext = $db->do_query($qnext);
	$nnext = $db->num_rows($rnext);
	$slash ='&nbsp;/&nbsp;';
	if($nnext>0){//print_r($dtnext);
		$exist_pagging = true;
		$dtnext = $db->fetch_array($rnext);
		//$slash = ($nprev>0? '&nbsp;/&nbsp;': '');
		//add_variable("next",$slash.'<a href="'.HTTP.SITE_URL.'/'.$type.'/'.$dtnext['sef_category'].'/'.$dtnext['lsef'].'.html'.'">Next</a>');
		add_variable("next",$slash.'<a href="'.HTTP.SITE_URL.'/'.$type.'/'.$dtnext['lsef'].'.html'.'">Next</a>');
	}else{
		add_variable("next",$slash.'<a href="#" class="no-link">Next</a>');
	}

	if($exist_pagging){
		parse_template('paggingPortfolioDetail','pagging_portfolio_detail',true);
	}
}


/*function get_portfolio_page_details(){
	$type = 'portfolio';
	global $db;
	set_template(TEMPLATE_PATH."/portfolio-details.html",'portfolio');
	add_block('portfolioBlock', 'bPortfolio', 'portfolio');


	$post_cat = get_portfolio_category();
	$post_sef = get_portfolio_page_sef();

	$sql = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s AND lsef=%s AND larticle_status=%s AND lshare_to=0 order by lorder",$type,$post_sef,'publish');
	$r   = $db->do_query($sql);
	$dat = $db->fetch_array($r);

	$clientName = get_additional_field( $dat['larticle_id'], 'the_project_client_name', $type);
	$projURL = get_additional_field( $dat['larticle_id'], 'the_project_url', $type);
	$othWork = json_decode( get_additional_field( $dat['larticle_id'], 'other_work_list', $type) );


	add_variable('projectDescription', filter_content($dat['larticle_content'],true));
	add_variable('the_project_client_name', $clientName);
	add_variable('the_project_title_name', $dat['larticle_title']);


	if(!empty($projURL)){
		$tmp = str_replace('http:','',$projURL);
		$tmp = str_replace('https://','',$tmp);
		$projURL = 'https://'.$tmp;

		add_variable('theWebsiteLink', '<a class="projectLink" href="'.$projURL.'">View Website ></a>');
	}else{
		add_variable('theWebsiteLink', '');
	}

	add_variable('imageAttached',   getImageAttached($dat['larticle_id']));
	add_variable('the_project_type', get_portfolio_category_name($post_cat, $type));

	$othList = '';
	if(is_array($othWork)){
	foreach($othWork as $work){
		$url = $work->url;
		if(empty($url)){ $url = '#'; }
		$othList .= '<li><a href="'.$url.'">'.$work->name.'</a></li>';
	}}else{
		//othWorksDisplayStatus
		add_variable('othWorksDisplayStatus', ' style="display:none;" ');
	}
	add_variable('other_works_list', $othList);


	$othID = get_portfolio_category_ID($post_cat, 'portfolio');
	$keyID = $dat['larticle_id'];
	add_variable('backToListLink', 'https://'.SITE_URL.'/portfolio/'.$post_cat.'/');
	$sql = $db->prepare_query("SELECT a.*
							   FROM lumonata_articles a,lumonata_rule_relationship b
							   WHERE a.larticle_type=%s AND a.larticle_status=%s
							   AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0 AND a.larticle_id<%d
							   ORDER BY a.lorder LIMIT 0,1",$type,'publish',$othID,$keyID);
	$r   = $db->do_query($sql);
	$da1 = $db->fetch_array($r);
	$nnn = $db->num_rows($r);
	if( $nnn>0 ){
		add_variable('navToPrevLink', permalink($da1['larticle_id']));
	}else{
		add_variable('navToPrevLink', '#');
		add_variable('navToPrevLinkCSS', ' style="color:#333; background:none;" ');
	}
	$sql = $db->prepare_query("SELECT a.*
							   FROM lumonata_articles a,lumonata_rule_relationship b
							   WHERE a.larticle_type=%s AND a.larticle_status=%s
							   AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0 AND a.larticle_id>%d
							   ORDER BY a.lorder LIMIT 0,1",$type,'publish',$othID,$keyID);
	$r   = $db->do_query($sql);
	$da1 = $db->fetch_array($r);
	$nnn = $db->num_rows($r);
	if( $nnn>0 ){
		add_variable('navToNextLink', permalink($da1['larticle_id']));
	}else{
		add_variable('navToNextLink', '#');
		add_variable('navToNextLinkCSS', ' style="color:#333; background:none;" ');
	}


	parse_template('portfolioBlock','bPortfolio',false);
	return return_template('portfolio');
}

*/






function is_custom_app_details(){
	$uri = get_uri();
	$prt = array_reverse(explode('.',$uri));
	$nnn = count($prt);
	if( $nnn>1 && $prt[0]=='html' ){
		return true;
	}else{
		return false;
	}
}
function get_portfolio_category(){
	$uri = get_uri();
	$prt = explode('.',$uri);
	$z   = explode('/',$prt[0]);

	if(isset($z[1])){ return $z[1]; }
}
function get_portfolio_category_name($lsef, $lgroup,$rule='categories'){
	global $db;
	$sql = $db->prepare_query("SELECT lname FROM lumonata_rules WHERE lsef=%s AND lgroup=%s AND lrule=%s",$lsef,$lgroup,$rule);
	$r   = $db->do_query($sql);
	$dat = $db->fetch_array($r);

	return $dat['lname'];
}
function get_portfolio_category_ID($lsef, $lgroup){
	global $db;

	if($lsef=='uncategorized'){$addition="OR lgroup='default'";}else{$addition='';}
	$sql = $db->prepare_query("SELECT lrule_id FROM lumonata_rules WHERE lsef=%s AND lgroup=%s $addition AND lrule='categories'",$lsef,$lgroup);
	$r   = $db->do_query($sql);
	$dat = $db->fetch_array($r);

	return $dat['lrule_id'];
}


function get_portfolio_page_num(){
	$uri = get_uri();
	$cat = get_portfolio_category();

	$tmp = str_replace('portfolio/','',$uri);
	$tmp = str_replace($cat.'/','',$tmp);

	return $tmp;
}

function get_custom_app_page_num($appname='portfolio'){
	$uri = get_uri();
	$cat = get_portfolio_category();

	$tmp = str_replace($appname.'/','',$uri);
	$tmp = str_replace($cat.'/','',$tmp);

	return $tmp;
}

function get_portfolio_page_sef(){
	$uri = get_uri();
	$z   = array_reverse(explode('/',$uri));
	$y   = explode('.',$z[0]);

	return $y[0];
}


function get_portfolio_category_sef(){
	$uri = get_uri();
	$z   = array_reverse(explode('/',$uri));
	return $z[1];
}





function getFirstImageAttached($article_id){
	global $db;
	$sizeSTR = 'h=200&w=280';
	if(empty($article_id))
	return;

	$url = '';
	$sql = $db->prepare_query("SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lorder ASC LIMIT 0,1 ",$article_id);
    $r = $db->do_query($sql);
	$d = $db->fetch_array($r);
	$url = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.HTTP.SITE_URL.$d['lattach_loc'];
	//$url = 'https://'.SITE_URL.$d['lattach_loc'];

	return $url;
}
function getImageAttached($article_id){
	global $db;
	if(empty($article_id))
	return;

	//========================================================================
	$metaKey = 'portfolio';
	$selectedThumb = get_additional_field( $article_id, 'the_selected_thumbnail', $metaKey);
	//========================================================================


	$all = '';
	$sql = $db->prepare_query("SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lorder ASC ",$article_id); // [AM20120806]
    $r = $db->do_query($sql);
	while($d = $db->fetch_array($r)){
		if($selectedThumb!=$d['lattach_loc']){
			$ur1 = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?w=600&src='.HTTP.SITE_URL.$d['lattach_loc'];
			$ur2 = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?w=500&src='.HTTP.SITE_URL.$d['lattach_loc'];
			$ur3 = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?w=300&src='.HTTP.SITE_URL.$d['lattach_loc'];

			$all .= '<img class="big"   src="'.$ur1.'" />';
			$all .= '<img class="med"   src="'.$ur2.'" />';
			$all .= '<img class="small" src="'.$ur3.'" />';
		}
	}

	return $all;
}



function portfolio_setting($tabs){ // [AM20120806]

	$alert = '';
	if(isset($_POST['save_portfolio_setting'])){
		$error = false;
		foreach ($_POST as $key=>$val){
			if( $key!= "save_portfolio_setting" ){
				if(find_meta_data($key))
					$update = update_meta_data($key,$val);
				else
					$update = set_meta_data($key,$val);

				if(!$update){ $error = true; }
			}
		}
		if($error){
			$alert = "<div class=\"alert_green_form\">Failed to update.</div>";
		}else{
			$alert = "<div class=\"alert_green_form\">Data has been updated.</div>";
		}
	}

	$thisPluginPath = PLUGINS_PATH.'/portfolio';
	set_template($thisPluginPath."/setting.html",'portfolioS');
	add_block('portfolioSetting','sportfolio','portfolioS');


	if(empty($_GET['tab']))
		$the_tab='portfolio_setting';
	else
		$the_tab=$_GET['tab'];
	add_variable('tabs',set_tabs($tabs,$the_tab));
	add_actions('section_title','Portfolio Setting');
	add_variable('title','Portfolio Setting');



	$setting_vars = array('thumb_portfolio_view_home','thumb_portfolio_view_detail');
	foreach($setting_vars as $c_var){
		add_variable($c_var, get_meta_data($c_var));
	}
	add_variable('alert', $alert);


	parse_template('portfolioSetting','sportfolio',false);
	$templates = return_template('portfolioS');
	return $templates;
}






add_privileges('administrator', 'portfolio', 'insert');
add_privileges('administrator', 'portfolio', 'update');
add_privileges('administrator', 'portfolio', 'delete');

add_apps_menu(array('portfolio'=>'Portfolio'));
add_actions("portfolio_additional_plugins","plugin_image_portfolio_attachment");
add_actions("link_to_blog","link_portfolio_to_blog");
add_actions("portfolio_additional_filed","additional_data","Other Portfolio Details","portfolioAddons");
add_actions("portfolio","get_admin_article","portfolio","Portfolio|Portfolio",array('portfolio_setting'=>'Setting')); // [AM20120806]

//add_actions('portfolio_page', 'get_portfolio_page');
add_actions("portfolio_setting_article_extends","portfolio_setting"); // [AM20120806]
//echo get_appname();
if(get_appname()=='portfolio'){
	if(is_custom_app_details()){
		add_actions('thecontent', 'get_portfolio_page_details');
	}else{
		add_actions('thecontent', 'get_portfolio_page');
	}
}
function cek_url(){
	$uri = get_uri();
	$ex = explode('/',$uri);
	return $ex;
}

function get_mime_type(){
	$mime_type = array("'image/jpeg'","'image/jpg'","'image/png'");
	$mime_type = implode(',',$mime_type);
	return $mime_type;
}

function get_filter_portfolio($type_rule='categories',$exclude=""){
	global $db;
	$filter = "";
	$q = $db->prepare_query("select lname,lsef from lumonata_rules where lgroup=%s and lcount > %d and lparent=%d  and lrule=%s order by lorder",'portfolio',0,0,$type_rule);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		$links = "";
		while($dt= $db->fetch_array($r)){
			$current_filter = ($dt['lsef']==$exclude?"class=\"current-filter\"":'');
			$links .="&nbsp; &nbsp; <a href=\"".HTTP.SITE_URL.'/portfolio/'.($type_rule=='tags'?'tags/':'').$dt['lsef']."\" onclick=\"return false;\" $current_filter>".$dt['lname']."</a>";
		}
		$current_all_work = ($exclude==""? "class=\"current-filter\"":'');
		$all_work = "<a href=\"".HTTP.SITE_URL.'/portfolio/'."\" onclick=\"return false;\" $current_all_work>All Works</a>";

		$filter = "<p class=\"text text-2\" id=\"fillter-container\">$all_work $links</p>";
	}
	return $filter;
}


/*
require_once("portfolio_theme.php");
add_actions("accomodations_additional_filed","features_text");
add_actions("accomodations_additional_filed","additional_data","Accommodation Facilites","features_text",true,array('<p>andik</p>'));
*/
?>
