<?php
if (isset($_POST['update_order']) && isset($_POST['state']) && $_POST['state']=='header_image' ){
	header_image_update_order();
	exit;
}
function header_image_update_order(){
	global $db;
	$items 		= $_POST['theitem'];
	$start 		= $_POST['start'];
	$app_name 	= 'page_header_image';
 	
	foreach($items as $key=>$val){
        	$order =  $key+$start;
        	
			$q	= $db->prepare_query("SELECT * from  lumonata_additional_fields WHERE lapp_id=%d AND lapp_name=%s",$val,$app_name);    
			$r	= $db->do_query($q);
			$d	= $db->fetch_array($r);
			
			$lvalue 			= json_decode($d['lvalue'],true);
			$lvalue['order'] 	= header_image_coztumize_order($order);
			$lvalue				= json_encode($lvalue);
		
			$qu = $db->prepare_query("UPDATE lumonata_additional_fields SET lvalue=%s WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s",$lvalue,$val,'phi_data',$app_name);
			
        	$db->do_query($qu);           
     }
}
function get_admin_header_image(){
		///jika diklik load more
        if(isset($_POST['loadMore']) && ($_POST['loadMore']=='header_image') ){
			get_header_image_list_ajax();			
			exit;
		}
		
        $post_id=0;
		
        if(is_add_new()){
			$table = "lumonata_additional_fields";
			$field = "lapp_id";
			$where = "";
			$post_id = hi_setCode($table,$field,$where);
            return add_new_header_image($post_id) ;
        }elseif(is_edit()){ 
            return edit_header_image($_GET['id']);
        }elseif(is_edit_all() && isset($_POST['select'])){
            return edit_page();
        }elseif(is_delete_all()){
                add_actions('section_title','Delete Header');
				$warning="<h1>Header image</h1>
                <div class=\"tab_container\">
                    <div class=\"single_content\">";
                $warning.="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this header image:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these header images:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
						$table = "lumonata_additional_fields";
						$cond = "Where lapp_id = $val AND lapp_name='page_header_image' AND lkey='phi_data'";
                        $d=hi_get_field_costum_table($table,$cond);
                        $warning.="<li>".$d['text1']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('header_image')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
				$warning.="</div></div>";
                
                return $warning;
        }elseif(is_confirm_delete()){
                foreach($_POST['id'] as $key=>$val){
                        delete_header_image($val);
                }
        }
        
        //Display Users Lists
        if(is_num_header_image()>0){
                //add_actions('header_elements','get_javascript','jquery_ui');
                //add_actions('header_elements','get_javascript','articles_list');
                return get_header_image_list();
        }else{			
        	return add_new_header_image();
		}
    }
	
function get_header_image_list($type='header_image'){
	global $db;
	$app_name = 'page_header_image';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('header_image')."&page=";
	
	
	if(is_search()){ 	
		 $sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s AND AF.lvalue like %s ORDER BY AF.lvalue",
			'phi_search','page_header_image',"%".$_POST['s']."%");	
		 $num_rows=count_rows($sql);
		 
		 $sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s AND AF.lvalue like %s ORDER BY AF.lvalue limit %d, %d",
			'phi_search','page_header_image',"%".$_POST['s']."%",$limit,$viewed);	
	}else{
		$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue",
			'phi_data',$app_name);		
		$num_rows=count_rows($sql);
		
		$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue limit %d, %d",
			'phi_data',$app_name,$limit,$viewed);
	}
	
	
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<li>".button("button=add_new",get_state_url("header_image")."&prc=add_new")."</li>
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	
	$url_ajax = HTTP.SITE_URL.'/header-image-ajax/';
	$list.= get_header_image_load_more_js();
	
	$list.="<h1>Header Images</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('header_image')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".search_box_header_image($url_ajax,'list_item','state=header_image&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					   <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('header_image')."\" />
					   <input type=\"hidden\" name=\"siteURL\" value=\"".HTTP.SITE_URL."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"header_image\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
									</ul>
							</div>
						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />
								<div class=\"pages_title\" style=\"width:25%;\">Article</div>
								<div class=\"title_category\">Image</div>
								<div class=\"pages_title\" style=\"width:25%;\">URL</div>		
								<div class=\"pages_title\" style='max-width:25%;' >Title</div>																
							</div>
							<div id=\"list_item\">";
							
	$list.=header_image_list($result,$start_order);
	
	$list .= "</div>";
       
	if ($num_rows>$viewed){ $list .= get_header_image_load_more_link($page);}     
          	               	
	$list.="		
			</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									
								</ul>   
							</div>
					</div>
					<!-- 
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
					-->
				</div>
			</div>
		";
	/*$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-admin/javascript/articles_list.js" ></script>';*/
	$list .='<script type="text/javascript" src="'.HTTP.site_url().'/lumonata-plugins/header_image/js/list.js" ></script>';
	$list .='<script type="text/javascript" language="javascript">
					$(document).ready( function(){
        
        $("input[name=select_all]").removeAttr("checked");
        $("input[name=select[]]").each(function(){
            $("input[name=select[]]").removeAttr("checked");
        });
        
        $("input[name=select_all]").click(function(){
                var checked_status = this.checked;
                
                $(".select").each(function(){
                        this.checked = checked_status;
                        if(checked_status){ //checked all chekcbox if select all checked
                            $("input[name=edit]").removeClass("btn_edit_disable");
                            $("input[name=edit]").addClass("btn_edit_enable");
                            $("input[name=edit]").removeAttr("disabled");
                            
                            $("input[name=delete]").removeClass("btn_delete_disable");
                            $("input[name=delete]").addClass("btn_delete_enable");
                            $("input[name=delete]").removeAttr("disabled");
                            
                            $("input[name=publish]").removeClass("btn_publish_disable");
                            $("input[name=publish]").addClass("btn_publish_enable");
                            $("input[name=publish]").removeAttr("disabled");
                            
                            $("input[name=unpublish]").removeClass("btn_save_changes_disable");
                            $("input[name=unpublish]").addClass("btn_save_changes_enable");
                            $("input[name=unpublish]").removeAttr("disabled");
                        }else{
                            $("input[name=edit]").removeClass("btn_edit_enable");
                            $("input[name=edit]").addClass("btn_edit_disable");
                            $("input[name=edit]").attr("disabled", "disabled");
                            
                            $("input[name=delete]").removeClass("btn_delete_enable");
                            $("input[name=delete]").addClass("btn_delete_disable");
                            $("input[name=delete]").attr("disabled", "disabled");
                            
                            $("input[name=publish]").removeClass("btn_publish_enable");
                            $("input[name=publish]").addClass("btn_publish_disable");
                            $("input[name=publish]").attr("disabled", "disabled");
                            
                            $("input[name=unpublish]").removeClass("btn_save_changes_enable");
                            $("input[name=unpublish]").addClass("btn_save_changes_disable");
                            $("input[name=unpublish]").attr("disabled", "disabled");
                            
                        }
                });
        });        
       
});
			</script>';
		
	add_actions('section_title','Header Image');
	add_actions('admin_tail',popup_header_image_delete());
	return $list;
}

	
function get_header_image_list_ajax($type='header_image'){
	global $db;
	$app_name = 'page_header_image';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	$page = $_POST['page']+1;   
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('header_image')."&page=";
	
	
	if(is_search()){ 	
		 $sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s AND AF.lvalue like %s ORDER BY AF.lvalue",
			'phi_search','page_header_image',"%".$_POST['s']."%");	
		 $num_rows=count_rows($sql);
		 
		 $sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s AND AF.lvalue like %s ORDER BY AF.lvalue limit %d, %d",
			'phi_search','page_header_image',"%".$_POST['s']."%",$limit,$viewed);	
	}else{
		$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue",
			'phi_data',$app_name);		
		$num_rows=count_rows($sql);
		
		$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue limit %d, %d",
			'phi_data',$app_name,$limit,$viewed);
	}
	
	
	
	$result=$db->do_query($sql);
	$numData = $db->num_rows($result);
	$start_order=($page - 1) * $viewed + 1; //start order number       
	
	if($numData>0){
        $list .= header_image_list($result);
        $list .= get_header_image_load_more_link($page);
	} 

	echo $list;
	
}

function header_image_list($result,$i=1){
        global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }		
        while($d=$db->fetch_array($result)){
        		$article_id = get_additional_field($d['lapp_id'], 'phi_id',$d['lapp_name']);
				if(!empty($article_id)) $name	= get_article_title($article_id); 
				if(empty($name)){
					$fetch_rule = fetch_rule('rule_id='.$article_id);
					$name = $fetch_rule['lname'];
				}
				
				//ihd = image_header_data
				$ihd = get_image_header_data($d['lapp_id']);
				$image= $ihd['img'];
				
				
				if (!empty($image)){
					$image_url = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$image;
					$url_thumb = HTTP.SITE_URL.'/app/tb/tb.php?w=100&h=70&q=90&src='.$image_url;
					$image = '
					<a href="'.$image_url.'" rel="view_image" class="cboxElement">
					<img src="'.$url_thumb.'" style="width:50px;min-height:20px;"/>
					</a>';
				}				
			
				$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['lapp_id']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['lapp_id']."\" style='margin-left:13px;' />
                                
								
								<div class=\"pages_title\" style=\"width:25%;\">".$name."</div>
								<div class=\"title_category\">".$image."</div>
								<div class=\"pages_title\" style=\"width:25%;\">".$ihd['url']."</div>
								<div class=\"pages_title\" style='max-width:25%;'>".$ihd['text1']."</div>
								
								
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['lapp_id']."\">
                                                <a href=\"".get_state_url('header_image')."&prc=edit&id=".$article_id."&app_id=".$d['lapp_id']."\">Edit</a> |
                                                <!--<a href=\"javascript:;\" rel=\"delete_".$d['lapp_id']."\">Delete</a>-->
                                                <a href=\"javascript:;\" class=\"delete_link\" id=\"".$d['lapp_id']."\" rel=\"".$name."\">Delete</a>

                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                		$('a[rel=view_image]').colorbox();
                                		$('.iframe_new').colorbox({iframe:true, width:'560px', height:'340px'});
                                        $('#theitem_".$d['lapp_id']."').mouseover(function(){
                                                $('#the_navigation_".$d['lapp_id']."').show();
                                        });
                                        $('#theitem_".$d['lapp_id']."').mouseout(function(){
                                                $('#the_navigation_".$d['lapp_id']."').hide();
                                        });
                                </script>
                                
                        </div>";
                $msg="Are you sure to delete ".$ihd['text1']."?";
				$url = HTTP.SITE_URL.'/header-image-ajax/';
                add_actions('admin_tail','delete_confirmation_box_header_image',$d['lapp_id'],$msg,$url,"theitem_".$d['lapp_id'],'state=header_image&prc=delete&id='.$d['lapp_id']);                
                $i++;
        }
        return $list;
    }

function add_new_header_image($post_id=0){
	set_header_image_template();
	$i 		= 0;
	$id 	= '';
	$button	= "";
	$button	.="
	<li>".button("button=save_changes&label=Save")."</li>
	<li>".button("button=cancel",get_state_url(HI_PLUGIN_NAME))."</li>
	<li>".button("button=add_new",get_state_url(HI_PLUGIN_NAME)."&prc=add_new")."</li>
	";
	
		
	
	add_actions('section_title','Header Image - Add New');
	add_variable('title_Form','Header Image');
	add_variable('option_app_type', option_app_type());
	add_variable('option_article', option_article());
	add_variable('existing_image', existing_image());
	
	add_variable('display_combo_article_fieldset', 'display: none;');
	
	

	if (isset($_POST['save_changes'])){
		//validation
		if ($_POST['article_id'][$i]==''){			
			$error = '<div class="error_red">Please choose article.</div>';
			add_variable('error',$error);
		}elseif (isset($_FILES['image']['name'][$i]) && ($_FILES['image']['name'][$i]=='') && (empty($_POST['exist_img'][$i]))){			
			$error = '<div class="error_red">Please choose image.</div>';
			add_variable('error',$error);
		}else{
			if (save_add_header_image()){
				$error = '<div class="error_green">Add header image has save succesfully.</div>';
				add_variable('error',$error);
				
				add_variable('val_title','');
			}else{
				$error = '<div class="error_red">Something wrong, please try againt.</div>';
				add_variable('error',$error);
			}
		}
		
	}
	add_variable('existing_image', existing_image());
	
	/* B Show Data*/
		$thaUrl = HTTP.SITE_URL.'/header-image-ajax/';
		add_variable('thaUrl',$thaUrl);
		add_variable('post_id',$post_id);
		add_variable('index_sp',0);
		add_variable('val_order',0);
	/* E Show Data*/
	
	add_variable('i',$i);
	add_variable('button',$button);
	parse_template('loopPage','lPage',false);
	 
	return return_header_image_template();
	
}

function edit_header_image($post_id=0){
		global $db;
        global $thepost;
        $index=0;
        $button="";
        set_header_image_template();
		
            $button.="
           	<li>".button("button=save_changes&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('header_image'))."</li>
            <li>".button("button=add_new",get_state_url('header_image')."&prc=add_new")."</li>
            ";

		$app_id = $_GET['app_id'];
		
		
        //set the page Title
        add_actions('section_title','Header image - Edit');
      
			//echo "is_single_edit";
				add_variable('title_Form','Edit header image');
				/* B Show Record */
				
				if (isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit'){
					//echo "Edit";
					$i = 0;
					$id = $_GET['id'];					
					
					if (save_edit_header_image($i,$id)){
						$error = '<div class="error_green">Edit header has succesfully.</div>';
						add_variable('error',$error);
						
					}else{
						$error = '<div class="error_red">Something wrong, please try againt.</div>';
						add_variable('error',$error);
					}
				}
				
				$table = "lumonata_additional_fields";
				$cond = "Where lapp_id='".$app_id."' AND lkey='phi_data' AND lapp_name='page_header_image'";
				$d = hi_get_field_costum_table($table,$cond);
				
				$app_id=$_GET['app_id'];
				$phi_data = json_decode(get_additional_field($app_id, 'phi_data', 'page_header_image'),true);
				add_variable('option_app_type', option_app_type($phi_data['app_type']));				
				add_variable('option_article',option_article($post_id,$phi_data['app_type']));
				add_variable('readonly','readonly="readonly"');
				add_variable('post_id',$post_id);
				add_variable('app_id',$app_id);
				
	
				$thaUrl = HTTP.SITE_URL.'/header-image-ajax/';
				add_variable('thaUrl',$thaUrl);
                add_variable('val_title',$d['text1']);
                add_variable('val_subtitle',$d['text2']);				
				add_variable('val_url',$d['url']);
				add_variable('val_description_left',base64_decode($d['description_left']));
				add_variable('val_description_right',base64_decode($d['description_right']));				
				add_variable('val_order',$d['order']);
				$avatar = '';
				if (!empty($d['img'])){
					$url_avatar = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$d['img'];
					$avatar  = '<div class="box-exist-img">';
					$avatar .= '<img class="exist-img" src="'.HTTP.site_url().'/app/tb/tb.php?w=50&h=50&src='.$url_avatar.'">';
					$avatar .= '<img class="exist-img img_300 exist-img-current" src="'.HTTP.site_url().'/app/tb/tb.php?w=300&src='.$url_avatar.'">';
					$avatar .= '</div>';
					$avatar .="<script type=\"text/javascript\" language=\"javascript\">
				                    $(function(){
				                        $('.box-exist-img').mouseover(function(){
				                        	$('.exist-img-current').css('display','block');
										})
										$('.box-exist-img').mouseleave(function(){
											$('.exist-img-current').css('display','none');
										})
				                    });
				                </script>";
					
				}
				add_variable('avatar','Current image : <br>'.$avatar);
				
				add_variable('existing_image',existing_image($index,$d['img']));	
				/*E Show Record  */

				add_variable('i',$index);
                parse_template('loopPage','lPage',false);
           
        
        
       
        add_variable('button',$button);
        return return_header_image_template();
    }
		
function save_add_header_image(){
	global $db;
	$i		=0;	
	
	$app_type = $_POST['app_type_id'][$i];
	$app_id = hi_setCode("lumonata_additional_fields","lapp_id");	
	$art_id	= $_POST['article_id'][$i];
	$title 	= $_POST['title'][$i];
	$brief	= $_POST['sub_title'][$i];
	$url 	= $_POST['url'][$i];
	$description_left 	= $_POST['description_left'][$i];
	$description_right 	= $_POST['description_right'][$i];
	$order	= header_image_coztumize_order($_POST['order'][$i]);
	//image
	$img_name = '';
	if (isset($_FILES['image']['name'][$i]) && !empty($_FILES['image']['name'][$i])){
		$img = upload_image_header_image($i,$art_id,$title);
		$img_name = $img['img_name'];
	}elseif (isset($_POST['exist_img'][$i]) && !empty($_POST['exist_img'][$i])){			
		$img_name = $_POST['exist_img'][$i];
	}else{
		$error= 'Please choose an image';
	}
	if ($img_name==''){
		return false;
	}
	//additional filed
	$app_name 	= 'page_header_image';
	$key1		= 'phi_id';
	$key2		= 'phi_data';
	$key3		= 'phi_search';
	$val1		= $art_id;
	$val2		= array("order"=>$order,"img"=>$img_name,"url"=>$url,"text1"=>$title,"text2"=>$brief,"app_type"=>$app_type,"description_left"=>$description_left,"description_right"=>$description_right);
	$val2		= json_encode($val2);
	$val3		= $title;		
	
	add_additional_field($app_id,$key1,$val1,$app_name);
	add_additional_field($app_id,$key2,$val2,$app_name);
	add_additional_field($app_id,$key3,$val3,$app_name);		
	return true;
		
}
		
function save_edit_header_image($i,$article_id){
	global $db;
	$i=0;
	
	$app_type = $_POST['app_type_id'][$i];
	$app_id = $_POST['app_id'][$i];	
	$art_id	= $_POST['article_id'][$i];
	$title 	= $_POST['title'][$i];
	$brief	= $_POST['sub_title'][$i];
	$url 	= $_POST['url'][$i];
	$description_left 	= base64_encode($_POST['description_left'][$i]);
	$description_right 	= base64_encode($_POST['description_right'][$i]);
	$order	= header_image_coztumize_order($_POST['order'][$i]);
	//image
	$img_name = '';
	if (isset($_FILES['image']['name'][$i]) && !empty($_FILES['image']['name'][$i])){
		$img = upload_image_header_image($i,$art_id,$title);
		$img_name = $img['img_name'];
	}elseif (isset($_POST['exist_img'][$i]) && !empty($_POST['exist_img'][$i])){			
		$img_name = $_POST['exist_img'][$i];
	}else{
		$error= 'Please choose an image';
	}
	//additional filed
	$app_name 	= 'page_header_image';
	$key1		= 'phi_id';
	$key2		= 'phi_data';
	$key3		= 'phi_search';
	$val1		= $art_id;
	$val2		= array("order"=>$order,"img"=>$img_name,"url"=>$url,"text1"=>$title,"text2"=>$brief,"app_type"=>$app_type,"description_left"=>$description_left,"description_right"=>$description_right);
	$val2		= json_encode($val2);
	$val3		= $title;		
	
	edit_additional_field($app_id,$key1,$val1,$app_name);
	edit_additional_field($app_id,$key2,$val2,$app_name);
	edit_additional_field($app_id,$key3,$val3,$app_name);		
	return true;
}

function save_rule_header_image($i,$article_id,$app_name='header_image',$action='add'){
	global $db;
	
	$categories = $_POST['categories'][$i];
	if ($action=='add'){
		$sql=$db->prepare_query("INSERT INTO lumonata_rule_relationship(
						lapp_id,
						lrule_id,
						lorder_id
						) Values (
						%d,%d,%d
						)",$article_id,$categories,0);
		$r=$db->do_query($sql);
	}else if ($action='edit'){
		$sql=$db->prepare_query("UPDATE lumonata_rule_relationship SET
						lrule_id=%d
						WHERE lapp_id=%d
						",$categories,$article_id);
		$r=$db->do_query($sql);
	
	}
}

function delete_header_image($app_id){
	global $db;
	
	$query = $db->prepare_query("Delete From lumonata_additional_fields Where lapp_id = %d AND lapp_name=%s",$app_id,'page_header_image');
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}

function delete_header_image_ajax(){
	global $db;
	add_actions('is_use_ajax', true);
	
	$app_id=$_POST['app_id'];
	$query = $db->prepare_query("Delete From lumonata_additional_fields Where lapp_id = %d AND lapp_name=%s",$app_id,'page_header_image');
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}

/*
function delete_header_image_size_price($article_id){
	global $db;
	delete_image_header_image($article_id);
	$query = $db->prepare_query("Delete From  lumonata_additional_fields Where lapp_id = %d",$article_id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}

function delete_image_header_image($article_id){
	global $db;
	$query = $db->prepare_query("Select * From lumonata_attachment Where larticle_id = %d",$article_id);
	$result = $db->do_query($query);
	$d = $db->fetch_array($result);
	$attach_loc = $d['lattach_loc'];
	if (!empty($attach_loc)){
		$file_name_1 = $attach_loc;
		$file_name_2 = $d['lattach_loc_thumb'];
		
		$destination1=PLUGINS_PATH."/slide/files/".$file_name_1;
		$destination2=PLUGINS_PATH."/slide/files/".$file_name_2;
		
		delete_file($destination1);
		delete_file($destination2);
		
		$query = $db->prepare_query("Delete From  lumonata_attachment Where larticle_id = %d",$article_id);
		$result = $db->do_query($query);
	}
	
}
*/	
function upload_image_header_image($i,$article_id,$name){
	 global $db;
  	 $thealert="";
	 $i = 0;
	 $file_name = $_FILES['image']['name'][$i];
	 $file_size = $_FILES['image']['size'][$i];
	 $file_type = $_FILES['image']['type'][$i];
	 $file_source = $_FILES['image']['tmp_name'][$i];

	 if (!empty($file_name)){
		//delete_image_header_image($article_id);
	 
		 if(is_allow_file_size($file_size)){
			if(is_allow_file_type($file_type,'image')){
				 if (empty($name)){
					$fix_file_name=time();
					$name = time();
					$time = '';
				 }else{
					$fix_file_name=file_name_filter($name);
					$name = $name;
					$time = '-'.time();
				 }
				 $file_ext=file_name_filter($file_name,true);

				 $file_name_1=$fix_file_name.$time.$file_ext;
				 $file_name_2=$fix_file_name.$time.'-thumbnail'.$file_ext;

				 $destination1=PLUGINS_PATH."/header_image/files/".$file_name_1;
				 $destination2=PLUGINS_PATH."/header_image/files/".$file_name_2;
				 $width = 940;
				 $height = 350;
				 upload_resize($file_source, $destination2, $file_type, $width,$height);
				 upload($file_source,$destination1);

				 
				$upload_date=date('Y-m-d H:i:s',time());
				$date_last_update=date('Y-m-d H:i:s',time());
				
				$alt_text = '';
				$caption = '';
				$title=rem_slashes($name);
				$alt_text=rem_slashes($alt_text);
				$caption=rem_slashes($caption);
				$mime_type=$file_type;
				//echo
				$sql=$db->prepare_query("INSERT INTO lumonata_attachment(
						larticle_id,
												
						lattach_loc,
						lattach_loc_thumb,
						ltitle,
						
						lalt_text,
						lcaption,
						upload_date,
						date_last_update,
						
						mime_type,
						lorder)
                        VALUES(
						%d,
						%s,%s,%s,
						%s,%s,%s,%s,
						%s,%d)",
						$article_id,
						$file_name_1,$file_name_2,$title,
						$alt_text,$caption,$upload_date,$date_last_update,
						$mime_type,1);
    
				if(reset_order_id("lumonata_attachment")){
					
					if ($db->do_query($sql)){
						$upload_img = array('status'=>'true','img_name'=>$file_name_1,'alert'=>'');
					}
				}
		   }else{
				$thealert="<div class=\"alert_yellow\">The maximum file size is 2MB</div>";
				$upload_img=array('status'=>'failed','img_name'=>'','alert'=>$thealert);
		   }
	   }else{
				$thealert="<div class=\"alert_yellow\">The maximum file size is 2MB</div>";
				$upload_img=array('status'=>'failed','img_name'=>'','alert'=>$thealert);
						 
	   }
	   
	}else{
		$thealert = 'Image source is empty';
		$upload_img=array('status'=>'failed','img_name'=>'','alert'=>$thealert);
	}
	
	return $upload_img;
}

function get_image_header_image($article_id,$status=''){
	global $db;
	$sql = $db->prepare_query("Select * From lumonata_attachment Where larticle_id =%d",$article_id);
	$r = $db->do_query($sql);
	$j = $db->num_rows($r);
	if (!empty($j)){
		$d = $db->fetch_array($r);
		if (empty($status)){
			$image = $d['lattach_loc'];
		}else if ($status=='thumb'){
			$image = $d['lattach_loc_thumb'];
		}else if ($status=='medium'){
			$image = $d['lattach_loc_medium'];
		}else if ($status=='large'){
			$image = $d['lattach_loc_large'];
		}  
		
		return $image;
	}else{
		return "";
	}
}

function set_header_image_template(){
        //set template
        set_template(PLUGINS_PATH."/".HI_PLUGIN_NAME."/template_admin.html",'template_header_image');
        //set block
        add_block('loopPage','lPage','template_header_image');
        add_block('pageAddNew','pAddNew','template_header_image');
}

function return_header_image_template($loop=false){
       
        parse_template('pageAddNew','pAddNew',$loop);
        return return_template('template_header_image');
}

function search_box_header_image($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\">
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"search\" class=\"searchbutton\" value=\"yes\" />
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'post_search_header_image',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}

function delete_confirmation_box_header_image($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'post_delete_header_image',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}

function is_num_header_image(){
	global $db;
	$sql = $db->prepare_query("SELECT * From lumonata_additional_fields Where lapp_name=%s AND lkey=%s",'page_header_image','phi_data');
	$num_rows=count_rows($sql);
	return $num_rows;
}

function header_image_ajax(){
	global $db;
	add_actions('is_use_ajax', true);
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_combo_article'){
		echo option_article('',$_POST['app_type']);
	}
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_delete_header_image'){
		$id = $_POST['val_id'];
		if (delete_header_image($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_search_header_image'){
		if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
			$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	    }else{
			$w="";
	    }
	    
	    $sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s AND AF.lvalue like %s ORDER BY AF.lvalue",
			'phi_search','page_header_image',"%".$_POST['val_s']."%");		
			$num_rows=count_rows($sql);
	    
	    $r=$db->do_query($sql);
        if($db->num_rows($r) > 0){
			//echo article_list($r,$_POST['state']);
			echo header_image_list($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_add_size_price'){
		$index = $_POST['val_index'];
		$index_sp = $_POST['val_index_sp'];
		$select_size = $_POST['val_select_size'];
		$price = $_POST['val_price'];

		$size_price_list = '
		<div class="list_item clearfix" id="size_price_list_'.$index.'_'.$index_sp.'" style="background:#eee; height:auto; padding:5px;">
			<div class="avatar block_size">'.$select_size.'</div>
			<div class="avatar block_price">'.$price.'</div>
			<div class="avatar block_button_delete">
				<input type="hidden" name="size_list['.$index.'][]" value="'.$select_size.'" class="medium_textbox size_list_'.$index.'" id="size_list_'.$index.'_'.$index_sp.'" />
				<input type="hidden" name="price_list['.$index.'][]" value="'.$price.'" class="medium_textbox" id="price_list_'.$index.'_'.$index_sp.'" />
				
				<input class="btn_delete_enable button_delete_sp" name="delete" value="Delete" type="button"
				rel="'.$index.'_'.$index_sp.'">
			</div>
		</div>';
		
		echo $size_price_list;
	}
	
}

function option_article($selected_id='',$app_type=''){
	global $db;
	$option = '<option value="">Select article</option>';
	if(!empty($app_type)){
		$q = $db->prepare_query("SELECT * FROM lumonata_rules WHERE lgroup=%s ORDER BY lorder",$app_type);
		$r = $db->do_query($q);
		while ($d=$db->fetch_array($r)){
			if ($d['lrule_id']==$selected_id){
				$option .= '<option selected value="'.$d['lrule_id'].'">'.$d['lname'].'</option>';
			}else{
				$option .= '<option value="'.$d['lrule_id'].'">'.$d['lname'].'</option>';
			}
		
		}
		
		$q = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder",$app_type);
	}else{
		$q = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s OR larticle_type=%s ORDER BY lorder",'pages','articles');
	}
	
	$r = $db->do_query($q);
	if($app_type<>'gallery'){	
		while ($d=$db->fetch_array($r)){
			if ($d['larticle_id']==$selected_id){
				$option .= '<option selected value="'.$d['larticle_id'].'">'.$d['larticle_title'].'</option>';
			}else{
				$option .= '<option value="'.$d['larticle_id'].'">'.$d['larticle_title'].'</option>';
			}
			
		}
	}
	
	
	return $option;
}

function option_app_type($selected_id=''){
	global $db;
	$option = '<option value="">Select App Type</option>';
	
	$array = array(
			"articles"=>"Articles",
			"pages"=>"Pages",
			"aboutus"=>"About Us",
			"cuisine"=>"Cuisine",
			"facilities"=>"Facilities",
			"programs"=>"Programs",			
			"ratesreservations"=>"Rates & Reservations",
			"villas"=>"Villas",
			"wellness"=>"Wellness",
			"contact-us"=>"Contact Us",
			"gallery"=>"Gallery");
	foreach ($array as $key => $val){
		if ($key==$selected_id){
			$option .= '<option selected value="'.$key.'">'.$val.'</option>';
		}else{
			$option .= '<option value="'.$key.'">'.$val.'</option>';
		}
	}
	return $option;
}

/*-----------ADDITIONAL FUNCTION, hi=header_image -------------*/
function existing_image($i=0,$selected_filename=''){
	global $db;
	$q = $db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s",'page_header_image','phi_data');
	$r = $db->do_query($q);
	$img = '';
	$prev_img = '';
	
	$arr_img = array();
	while ($d=$db->fetch_array($r)){
		$arr = json_decode($d['lvalue'],true);
		
		if (in_array($arr['img'],$arr_img)){
			
		}else{
			array_push($arr_img, $arr['img']);
		}		
	}	
	
	$j=0;
	foreach ($arr_img as $key=>$val){
		$filename = ROOT_PATH.'/lumonata-plugins/header_image/files/'.$val;
		if (file_exists($filename)){
			$checked = '';
			$selected = '';
			if ($val==$selected_filename){
				$selected = 'selected';
				$checked = 'checked';
			}
			$image = HTTP.site_url().'/lumonata-plugins/header_image/files/'.$val;
			$img .= '<div class="a-ei" style="position: relative;" rel="'.$j.'">';
			$img .= '<img class="exist-img '.$selected.'" id="img_'.$j.'" src="'.HTTP.site_url().'/app/tb/tb.php?w=50&h=50&src='.$image.'">';
			$img .= '<img class="exist-img img_300" id="img_300_'.$j.'" src="'.HTTP.site_url().'/app/tb/tb.php?w=300&src='.$image.'">';
			$img .= '<input '.$checked.' class="radio_exist_img" rel="'.$j.'" type="radio" name="exist_img['.$i.']" value="'.$val.'">';
			$img .= '</div>';
			$j++;
		}
	}
	$img.="<script type=\"text/javascript\" language=\"javascript\">
                    $(function(){
                        $('.a-ei').mouseover(function(){
                        	var id = $(this).attr('rel');
                        	$('.img_300').css('display','none');
                        	$('#img_300_'+id).css('display','block');
						})
						$('.a-ei').mouseleave(function(){
							$('.img_300').css('display','none');
						})
                    });
                </script>";
	return $img; 
}
function get_image_header_data($app_id,$app_name='page_header_image',$key='phi_data'){
	global $db;
	$query = $db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id=%d",$app_name,$key,$app_id);
	$result = $db->do_query($query);
	$data = $db->fetch_array($result);
	if (isset($data['lvalue']) && (!empty($data['lvalue']))){
		$arr = json_decode($data['lvalue'],true);
		return $arr;
	}else{
		return false;
	}
}
function hi_setCode($table,$field){
	global $db;
	//echo 
	$query = $db->prepare_query("SELECT MAX(".$field.") from ".$table."");
	$result = $db->do_query($query);
	$data = $db->fetch_array($result);
	if($data[0]==NULL){
		return "1";	
	}else{
		$cnt=$data[0]+1;
		return $cnt;
	}
}
function hi_get_field_costum_table($table,$condition){
	//defined database class to global variable
	global $db;
	//echo 
	$sql=$db->prepare_query("SELECT *  
				FROM $table 
				$condition
				");
	$r=$db->do_query($sql);
	$d=$db->fetch_array($r);
	$arr = json_decode($d['lvalue'],true);
	//print_r($arr);
	return $arr;
}
function header_image_coztumize_order($order){
	if (strlen($order)<2){
		$order = '000'.$order;
	}elseif((strlen($order)<3)){
		$order = '00'.$order;
	}elseif((strlen($order)<4)){
		$order = '0'.$order;
	}	
	return $order;
}



function get_header_image_load_more_js($type="articles"){    	
    	$type = $_GET['state'];
    	$js= "
						<script type=\"text/javascript\">
							$(function(){
								$('.loadMore').live(\"click\",function(){
									var PAGE = $(this).attr(\"id\");
									var TYPE = \"$type\";
									if(PAGE){
										$(\"#loadMore\"+PAGE).html('<img src=\"".HTTP.TEMPLATE_URL."/images/loading.gif\" border=\"0\">');		
										$.ajax({
											type: \"POST\",
											url: \"".HTTP.SITE_URL."/lumonata-admin/?state=".$_GET['state']."&category_name=".$_GET['category_name']."\",
											data: \"page=\"+ PAGE + \"&loadMore=\" + TYPE,
											cache: false,
											success: function(html){																						
												$(\"#list_item\").append(html);
												$(\"#loadMore\"+PAGE).remove(); // removing old more button
											}
										});
									}else{
										$(\".morebox\").html('The End');// no results
									}
									return false;
								});
							});
						</script>
          			  ";
    	return $js;
}
function get_header_image_load_more_link($pageID=''){
    	 $link = '
          			<div id="loadMore'.$pageID.'" class="morebox">
						<a href="#" class="loadMore" id="'.$pageID.'" rel="1">Load more...</a>
					</div>          		
          		   ';
    	 return $link;
}
function popup_header_image_delete(){
		 $pop = '   
		 	<div style="display: none;" id="delete_confirmation_wrapper">
			<div class="fade"></div>
			<div class="popup_block">
				<div class="popup">
					<div class="alert_yellow">Are sure want to delete Welcome to Arunna?</div>
					<div style="text-align:right;margin:10px 5px 0 0;">
						<button id="delete_yes" class="button" value="yes" name="confirm_delete" type="submit">Yes</button>
						<button id="delete_no" class="button" value="no" name="confirm_delete" type="button">No</button>
						<button id="delete_cancel" class="button" value="cancel" name="confirm_delete" type="button">Cancel</button>						
					</div>
				</div>
			</div>
			</div> ';
		 echo $pop;
		
}

function sync_header_image_search(){
	global $db;
	$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue",
			'phi_data','page_header_image');	
	$r=$db->do_query($sql);
	while ($d=$db->fetch_array($r)){
		$ihd = get_image_header_data($d['lapp_id']);
		
		//additional filed$title 	= $_POST['title'][$i];
	$app_name 	= 'page_header_image';
	$key3		= 'phi_search';
	$val3		= $ihd['text1'];		

	edit_additional_field($d['lapp_id'],$key3,$val3,$app_name);		
	}
}
add_actions("header-image-ajax_page","header_image_ajax");
add_actions("delete-header-image-ajax_page","delete_header_image_ajax");
?>