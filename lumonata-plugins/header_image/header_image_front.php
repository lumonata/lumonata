<?php
function get_blog_image($id){
	global $db;
	$q	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lvalue=%d",'page_header_image','phi_id',$id);
	$r	= $db->do_query($q);
	$n	= $db->num_rows($r);
	if ($n==0){
	 	return get_blog_image_default();
	}else{
		while ($d=$db->fetch_array($r)){
			$arr_app_id[] = $d['lapp_id'];
		}
		$ids = implode(", ", $arr_app_id);
	
		/*------*/
		$query	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id in ($ids) ORDER BY lvalue",'page_header_image','phi_data');
		$result	= $db->do_query($query);
		$n=$db->num_rows($result);
		$no=0;
		$aktif='';
		$gallery_hd='';
		$image .= '<ul  class="gallery-list" style="display:none;">';
		while ($data=$db->fetch_array($result)){
			$arr 	 = json_decode($data['lvalue'],true);
			$sizeSTR = 'w=140&h=97';
			$url = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$arr['img'];
			$url_thumb = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.$url;
			
		 	if($no==0){
		 		$aktif='aktif';
		 		$gallery_hd.='<img src="'.$url.'" class="aktif">';
		 	}else{
		 		$aktif='';
		 	}
			
			$image .= '<li><a rel="'.$no.'" href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list '.$aktif.'"><img src="'.$url_thumb.'" title="'.$data['larticle_title'].'"></a></li>';
			$no++;	
		}
		
	 	return $gallery_hd;
	}
}

function get_front_right_image($id){
	
	global $db;
	$q	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lvalue=%d",'page_header_image','phi_id',$id);
	$r	= $db->do_query($q);
	$n	= $db->num_rows($r);
	if ($n==0){
		$array=array('gallery_list'=>get_header_image_default());
	 	return json_encode($array);
	}else{
		while ($d=$db->fetch_array($r)){
			$arr_app_id[] = $d['lapp_id'];
		}
		$ids = implode(", ", $arr_app_id);
	
		/*------*/
		$query	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id in ($ids) ORDER BY lvalue",'page_header_image','phi_data');
		$result	= $db->do_query($query);
		$n=$db->num_rows($result);
		$no=0;
		$aktif='';
		$gallery_hd='';
		$image .= '<ul  class="gallery-list" style="display:none;">';
		while ($data=$db->fetch_array($result)){
			$arr 	 = json_decode($data['lvalue'],true);
			$sizeSTR = 'w=140&h=97';
			$url = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$arr['img'];			
			$ex = explode(".", $arr['img']);
			$thumbnail = $ex[0].'-thumbnail.'.$ex[1];
			$url_thumb = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$thumbnail;
			$url_thumb = HTTP.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src='.$url_thumb;
			
		 	if($no==0){
		 		$aktif='aktif';
		 		//$gallery_hd.='<img src="'.$url.'" class="aktif dasd">';
				$gallery_hd.='<div style="background: url('.$url.') no-repeat center center;-webkit-background-size: cover;background-size: cover;" class="new-list-gallery aktif"></div>';
		 	}else{
		 		$aktif='';
		 	}
			
			$header_image = '<div style="background: url('.$url.') no-repeat center center;-webkit-background-size: cover;background-size: cover;" class="new-list-gallery"></div>';
			/*
				
			*/
			
			$image .= '<li>
							<a rel="'.$no.'" href="'.$url.'" title="'.$data['larticle_title'].'" class="href_gallery_list '.$aktif.'">
									<!--<img src="'.$url_thumb.'" title="'.$data['larticle_title'].'">-->
									$header_image
							</a>
					  </li>';
			$no++;	
		}
		/*------*/
		$image .= '</ul>';
		
		$display_none='';
		$display_none2='';
		if($n==0){
			$display_none= 'display_none';
		}else if($n==1){
			$display_none2='display_none';
		}
		
		$gallery_hd='
			<div class="slider" id="slider">
	                    	<div class="slider-mask"><span class="loading"></span></div>
	                    	<div class="topright '.$display_none.'" style="">
	                    	<span class="number"><label>1</label> / '.$n.'</span>
	                    	<a class="full" title="Enter Fullscreen"></a>
	                    	</div>
	                    	<a class="prev href_gallery_hd '.$display_none.' '.$display_none2.'" rel="prev"  style=""></a>
	                    	<a class="next href_gallery_hd '.$display_none.' '.$display_none2.'" rel="next"  style=""></a>
	                    	<div class="image-block">
	                    		'.$gallery_hd.'
	                    	</div>
                    	</div>
		';
		$array=array('gallery_list'=>$image.$gallery_hd);
	 	return json_encode($array);
	}
}

function Xget_front_right_image($id){
	global $db;
	$q	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lvalue=%d",'page_header_image','phi_id',$id);
	$r	= $db->do_query($q);
	$n	= $db->num_rows($r);
	if ($n==0){
		return get_header_image_default();
	}else{
		while ($d=$db->fetch_array($r)){
			$arr_app_id[] = $d['lapp_id'];
		}
		$ids = implode(", ", $arr_app_id);
	
		/*------*/
		$query	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id in ($ids) ORDER BY lvalue",'page_header_image','phi_data');
		$result	= $db->do_query($query);
		while ($data=$db->fetch_array($result)){
			$arr 	 = json_decode($data['lvalue'],true);
			$img = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$arr['img'];
			//$image[] ='<img src="'.$img.'" >';
			$image ='<img src="'.$img.'" >';
			
		}
		/*------*/
	
		return $image;
	}
}

function get_front_header_image($page_id=''){
	global $db;	
	$caption_home = '';
	if (is_home()){
		$id=84;
		$caption_home = 'home';
		return;
	}elseif ($page_id==''){		
		$id = post_to_id();
	}else{
		$id = $page_id;
	}
	
	if ($id==''){		
		$header_image = get_header_image_default();
		return $header_image;
	}
	
	$q	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lvalue=%d",'page_header_image','phi_id',$id);
	$r	= $db->do_query($q);
	$n	= $db->num_rows($r);
	
	if ($n==0){
		$title = get_article_title($id);
		$header_image = get_header_image_default($title);
		return $header_image;
	}else{
		$num_li =0;
		$li = "";
		while ($d=$db->fetch_array($r)){
			$query	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id=%d",'page_header_image','phi_data',$d['lapp_id']);
			$result	= $db->do_query($query);		
			while ($data=$db->fetch_array($result)){
				$title 	 = '';
				$brief 	 = '';
				$arr 	 = json_decode($data['lvalue'],true);		
				
				if (isset($arr['text1']) && !empty($arr['text1'])){
					$title = '<h3>'.$arr['text1'].'</h3>';
				}
				
				if (isset($arr['text2']) && !empty($arr['text2'])){
					$brief = '<h5>'.$arr['text2'].'</h5>';
				}
				$caption = '<div class="caption '.$caption_home.'">
               					'.$title.'
                    			'.$brief.'
                 			</div>';
				if (isset($arr['url']) && !empty($arr['url'])){
					$image 	 = '<a href="'.$arr['url'].'"><img src="'.HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$arr['img'].'"></a>';
				}else{
					$image 	 = '<img src="'.HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$arr['img'].'">';
				}
						
				$li  	.= '<li style="max-width:100%;">';
				$li		.= $image;
				$li		.= $caption;
				$li		.= '</li>';
				
				$num_li++;
			}
		}		
		$class = '';
		if ($num_li>1){
			$class='class="rslides"';
		}
		$slide = '<ul '.$class.'>'.$li.'</ul>';
		return $slide;
	}	
}

function get_page_header_images($id){
	global $db;
	$q	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lvalue=%d",'page_header_image','phi_id',$id);
	$r	= $db->do_query($q);
	$n	= $db->num_rows($r);	
	if ($n==0){	
		return;
	}else{		
		while ($d=$db->fetch_array($r)){
			$arr_app_id[] = $d['lapp_id']; 
		}
		$ids = implode(", ", $arr_app_id);	
		
		/*------*/
		$query	= $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id in ($ids) ORDER BY lvalue",'page_header_image','phi_data');
		$result	= $db->do_query($query);				
		while ($data=$db->fetch_array($result)){				
			$arr 	 = json_decode($data['lvalue'],true);						
			$image[] = HTTP.SITE_URL.'/lumonata-plugins/header_image/files/'.$arr['img'];						
		}
		/*------*/	
		
		return $image;
	}
}

function get_blog_image_default($title=''){
	if ($title==''){
		$title = get_post_title();
	}

	//pengecualian
	if (is_details('articles')){
		$title = get_post_title();
	}

	$header_image = '<img src="'.get_theme_img().'/header-blog-default.jpg">';
	
	return $header_image;
}


function get_header_image_default($title=''){
	if ($title==''){
		$title = get_post_title();
	}

	//pengecualian
	if (is_details('articles')){
		$title = get_post_title();
	}


	//$header_image = '<img src="'.get_theme_img().'/header-default.jpg">';
	
	
	$url=get_theme_img().'/header-default.jpg';
	$url_thumb=get_theme_img().'/header-default.jpg';
	$image='';
	$gallery_hd='';
	$image .= '<ul  class="gallery-list" style="display:none;">';
	
	if($no==0){
 		$aktif='aktif';
 		$gallery_hd.=$header_image;
 	}else{
 		$aktif='';
 	}
	$header_image = '<div style="background: url('.$url.') no-repeat center center;-webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; width:100%; height:100%;position: absolute;
top: 0;
left: 0;
z-index: 70;
overflow: auto;"></div>';
	/*$image .= '<li>
					<a rel="'.$no.'" href="'.$url.'" class="href_gallery_list '.$aktif.'">
						<img src="'.$url_thumb.'">
					</a>
				</li>';*/
	$image .= '<li>
					<a rel="'.$no.'" href="'.$url.'" class="href_gallery_list '.$aktif.'">
						$header_image
					</a>
				</li>';
				
	$image .= '</ul>';
	$gallery_hd = $header_image;
	$display_none='';
	$display_none2='';
	if($n==0){
		$display_none= 'display_none';
	}else if($n==1){
		$display_none2='display_none';
	}
	$gallery_hd='
		<div class="slider" id="slider">
                    	<div class="slider-mask"><span class="loading"></span></div>
                    	<div class="topright '.$display_none.'" style="">
                    	<span class="number"><label>1</label> / '.$n.'</span>
                    	<a class="full" title="Enter Fullscreen"></a>
                    	</div>
                    	<a class="prev href_gallery_hd '.$display_none.' '.$display_none2.'" rel="prev"  style=""></a>
                    	<a class="next href_gallery_hd '.$display_none.' '.$display_none2.'" rel="next"  style=""></a>
                    	<div class="image-block">
                    		'.$gallery_hd.'
                    	</div>
                    </div>
	';
	//$array=array('gallery_list'=>$image.$gallery_hd);
	return $image.$gallery_hd;
	//return $header_image;
}

function Xget_header_image_default($title=''){ //ga kepake
	if ($title==''){
		$title = get_post_title();
	}	
	
	//pengecualian
	if (is_details('articles')){
		$title = get_post_title();
	}elseif(is_category('catname=after-care')){
		$title = 'AFTER-CARE';
	}elseif(is_page("page_name=contact-us")){
		$title = 'CONTACT US';
	}
	
	$caption = '<div class="caption"><h3>'.strtoupper($title).'</h3></div>';
	$header_image = '<ul><li><img src="'.get_theme_img().'/header-default.jpg">'.$caption.'</li></ul>';	
	return $header_image;
}
function Xget_article_id(){ //ga kepake
	global $db;
	if (is_home()){
		$id = 9;
	}elseif (is_permalink()){
		$permalink = get_uri();		
		$q = $db->prepare_query("SELECT larticle_id FROM lumonata_articles WHERE lsef=%s",$permalink);
		$r = $db->do_query($q);
		$d = $db->fetch_array($r);		
		$id = $d['larticle_id'];
	}elseif (isset($_GET['page_id']) && (!empty($_GET['page_id']))){
		$id = $_GET['page_id'];
	}else{
		return false;
	}
	
	
	return $id;
}
?>
