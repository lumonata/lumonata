<?php
/*
    Plugin Name: Newsletter
    Plugin URL: http://www.lumonata.com/
    Description: Plugin for arunna framework
    Author: Andi Darmika
    Author URL: http://www.aankun.com/
    Version: 1.0
    
*/

//========================================================================================= ADMIN
add_privileges('administrator', 'newsletter', 'insert');
add_privileges('administrator', 'newsletter', 'update');
add_privileges('administrator', 'newsletter', 'delete');


add_apps_menu(array('newsletter'=>'Newsletter'));
if(is_edit_all() && !(is_save_draft() || is_publish())){
	add_actions("header_elements","the_additional_fields_CSS");
    foreach($_POST['select'] as $index=>$post_id){
		add_actions("newsletter_additional_filed_".$index,"additional_data","Additional Data","newsletter_additional_fields");
    }
}else{
	add_actions("config_article_extends","newsletter_config");
	add_actions("members_article_extends","newsletter_members");
	
	add_actions("header_elements","the_additional_fields_CSS");
	add_actions("newsletter_additional_filed","additional_data","Additional Data","newsletter_additional_fields");
}
add_actions("newsletter","get_admin_article","newsletter","Newsletter|Newsletters",array('config'=>'Config', 'members'=>'Members'));



function the_additional_fields_CSS(){
	global $actions;	
	foreach($actions->action['header_elements']['args'] as $index => $hEle){
		if(isset($hEle[0]) && $hEle[0]=='jquery_ui'){
			$actions->action['header_elements']['func_name'][$index] = 'force_include_new_jQueryUI';
			$actions->action['header_elements']['args'][$index] = '';
		}
	}
	
	set_template(PLUGINS_PATH."/newsletter/css.html",'theStyles2');
	add_block('villasAditionalCSS','cssVillas','theStyles2');
	
	parse_template('villasAditionalCSS','cssVillas',true);
	$template = return_template('theStyles2');
	//$template .= '<link rel="stylesheet"  media="all"  type="text/css" href="http://'.SITE_URL.'/lumonata-plugins/newsletter/UI/jquery-ui.css" />';
	
	return $template;
}

function force_include_new_jQueryUI(){
	$template  = '';
	$template .= '<script type="text/javascript" src="'.HTTP.SITE_URL.'/lumonata-plugins/newsletter/ui/jquery-ui.js" ></script>';
	$template .= '<link rel="stylesheet"  media="all"  type="text/css" href="'.HTTP.SITE_URL.'/lumonata-plugins/newsletter/ui/jquery-ui.css" />';
	return $template;
}

function newsletter_config($tabs){
	$alert = '';
	if(isset($_POST['save_changes'])){
		$error = false;
		foreach ($_POST as $key=>$val){
			if( $key!= "save_changes" ){
				if(find_meta_data($key))
					$update = update_meta_data($key,$val);
				else 
					$update = set_meta_data($key,$val);
					
				if(!$update){ $error = true; }
			}
		}
		if($error){
			$alert = "<div class=\"alert_green_form\">Failed to update.</div>";
		}else{
			$alert = "<div class=\"alert_green_form\">Data has been updated.</div>";
		}
	}
	
	$thisPluginPath = PLUGINS_PATH.'/newsletter';
	set_template($thisPluginPath."/config.html",'newsletterC');
	add_block('newsletterConfig','cNewsletter','newsletterC');
	
	
	if(empty($_GET['tab']))
		$the_tab='config';
	else
		$the_tab=$_GET['tab'];
	add_variable('tabs',set_tabs($tabs,$the_tab));
	add_actions('section_title','Newsletter Configuration');
	add_variable('title','Newsletter Configuration');
	
	$config_vars = array('email_per_send','newsletter_notif_email','newsletter_smtp');
	foreach($config_vars as $c_var){
		add_variable($c_var, get_meta_data($c_var));
	}
	add_variable('alert', $alert);	
	
	
	parse_template('newsletterConfig','cNewsletter',false);
	$templates = return_template('newsletterC');	
	return $templates;
}

function notifyForNewSubscriber($email , $name){
	$alertEmail 	= get_meta_data('newsletter_notif_email');
	$smtp_server 	= get_meta_data('newsletter_smtp');
	$sendFrom 		= $alertEmail;
	
	$sendTo   = $email;
	$sitename = get_meta_data('web_title');
	
	ini_set("SMTP", $smtp_server);
	ini_set("sendmail_from", $sendFrom);
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";			
	$headers .= "From: Newsletter Notification - $sitename <$sendFrom>\r\n";
	
	$subject  = "Newsletter Notification - ".$sitename;
	
	$theMailContent1  = '<h1>Thank You</h1>';
	$theMailContent1 .= '<p>Dear '.$name.',<br />Thank you for registering in our newsletter.</p>';
	$theMailContent1 .= '<p>Best,<br />'.$sitename.'</p>';
	
	$theMailContent2  = '<h1>New Subscriber</h1>';
	$theMailContent2 .= '<p>There are a new newsletter subscriber by :</p>';
	$theMailContent2 .= '<p>name : '.$name.'<br />';
	$theMailContent2 .= 'email : '.$email.'</p>';
	$theMailContent2 .= '<p>Login to admin panel to moderate the new member.</p>';
	
	mail($sendTo, 		"$subject", "$theMailContent1", "$headers");
	mail($alertEmail, 	"$subject", "$theMailContent2", "$headers");
}

function newsletter_members($tabs){
	if(isset($_GET['a']) && $_GET['a']=='add'){
		return newsletter_members_new($tabs);	
	}else if(isset($_GET['a']) && $_GET['a']=='edit'){
		return newsletter_members_edit($tabs);	
	}else{
		return newsletter_members_list($tabs);
	}
}

function newsletter_members_edit($tabs){
	global $db;
	$thisPluginPath = PLUGINS_PATH.'/newsletter';
	$thisPluginURL  = SITE_URL.'/lumonata-plugins/newsletter';
	set_template($thisPluginPath."/members-mod.html",'newsletterC');
	add_block('newsletterLoop','nLoop','newsletterC');
	add_block('newsletterConfig','cNewsletter','newsletterC');	
	
	
	$alert = '';
	$error = false;
	$vars = array('full_name','status');
	$statusVal['pending'] = 0;
	$statusVal['approved'] = 1;
	
	if(isset($_POST) && isset($_POST['publish'])){
		$empty = false;
		foreach($vars as $var){ if(empty($_POST[$var])){ $empty = true; } }
		
		if(!$empty){
			$str = "UPDATE lumonata_newsletter_member SET lname=%s, lstatus=%d WHERE lmember_id=%d";
			$sql = $db->prepare_query($str, $_POST['full_name'], $statusVal[$_POST['status']], $_GET['id']);
			if($que = $db->do_query($sql)){
				$alert = "<div class=\"alert_green_form\">Update success.</div>";
				$error = false;
			}else{
				$alert = "<div class=\"alert_red_form\">Somethings wrong, please try again later!</div>";
				$error = true;
			}
		}else{
			$alert = "<div class=\"alert_red_form\">Please fill all fields!</div>";
			$error = true;
		}
	}
	add_variable('alert',$alert);
	
	
	
	//========================================================================================
	$str = "SELECT * FROM lumonata_newsletter_member WHERE lmember_id=%d";
	$sql = $db->prepare_query($str, $_GET['id']);
	$que = $db->do_query($sql);
	$dat = $db->fetch_array($que);
	
	add_variable('full_name',$dat['lname']);
	add_variable('email',$dat['lemail']);
	add_variable('email_disabled', ' disabled="disabled" ');
	add_variable('status_'.$dat['lstatus'], ' checked="checked" ');
	//========================================================================================			
	
	
	add_variable('plugin_url',$thisPluginURL);
	
	if(empty($_GET['tab']))
		$the_tab='config';
	else
		$the_tab=$_GET['tab'];
	add_variable('tabs',set_tabs($tabs,$the_tab));
	
	add_actions('section_title','Add Newsletter Member');
	add_variable('title','Add Newsletter Member');

	$button = ""; $rule = 'members'; $group = 'newsletter';
	if(is_admin_application())
		$url = get_application_url($group)."&tab=".$rule;
	else
		$url = get_state_url($group)."&tab=".$rule;

	if(!is_contributor() && !is_author()){
		$button.="<li>".button("button=add_new",$url."&a=add")."</li>
		<li>".button("button=publish&label=Save")."</li>
		<li>".button("button=cancel",$url)."</li>";
					
	}
	add_variable('button',$button);
	
	parse_template('newsletterLoop','nLoop',true);
	parse_template('newsletterConfig','cNewsletter',false);
	$templates = return_template('newsletterC');	
	return $templates;
}


function newsletter_members_new($tabs){
	global $db;
	$thisPluginPath = PLUGINS_PATH.'/newsletter';
	$thisPluginURL  = SITE_URL.'/lumonata-plugins/newsletter';
	set_template($thisPluginPath."/members-mod.html",'newsletterC');
	add_block('newsletterLoop','nLoop','newsletterC');
	add_block('newsletterConfig','cNewsletter','newsletterC');
	
	$alert = '';
	$error = false;
	$vars = array('full_name','email','status');
	$statusVal['pending'] = 0;
	$statusVal['approved'] = 1;
	
	if(isset($_POST) && isset($_POST['publish'])){
		$empty = false;
		foreach($vars as $var){ if(empty($_POST[$var])){ $empty = true; } }
		
		
		if(!$empty){
			if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $_POST['email'])) {
				//echo $_POST['emailAddress'];
				$str = "SELECT * FROM lumonata_newsletter_member WHERE lemail=%s";
				$sql = $db->prepare_query($str, $_POST['email']);
				$que = $db->do_query($sql);
				$nnn = $db->num_rows($que);
				if($nnn<1){
					$str = "INSERT INTO lumonata_newsletter_member (lmember_id, lemail, lname, lstatus) VALUES (%s, %s, %s, %d)";
					$sql = $db->prepare_query($str, '', $_POST['email'], $_POST['full_name'], $statusVal[$_POST['status']]);
					if($que = $db->do_query($sql)){
						$alert = "<div class=\"alert_green_form\">New member added successfully.</div>";
						$error = false;
					}else{
						$alert = "<div class=\"alert_red_form\">Somethings wrong, please try again later!</div>";
						$error = true;
					}
				}else{
					$alert = "<div class=\"alert_red_form\">The email you input is already exist!</div>";
					$error = true;
				}
			}else{
				$alert = "<div class=\"alert_red_form\">Invalid email address!</div>";
				$error = true;
			}
		}else{
			$alert = "<div class=\"alert_red_form\">Please fill all fields!</div>";
			$error = true;
		}
	}
	add_variable('alert',$alert);
	if($error){
		foreach($_POST as $index => $var){ 
			if($index=='status'){
				add_variable('status_'.$statusVal[$var], ' checked="checked" ');
			}else{
				add_variable($index, $var);
			}
		}
	}
	
	
	add_variable('plugin_url',$thisPluginURL);
	
	if(empty($_GET['tab']))
		$the_tab='config';
	else
		$the_tab=$_GET['tab'];
	add_variable('tabs',set_tabs($tabs,$the_tab));
	
	add_actions('section_title','Add Newsletter Member');
	add_variable('title','Add Newsletter Member');

	$button = ""; $rule = 'members'; $group = 'newsletter';
	if(is_admin_application())
		$url = get_application_url($group)."&tab=".$rule;
	else
		$url = get_state_url($group)."&tab=".$rule;

	if(!is_contributor() && !is_author()){
		$button.="<li>".button("button=add_new",$url."&a=add")."</li>
		<li>".button("button=publish&label=Publish")."</li>
		<li>".button("button=cancel",$url)."</li>";
					
	}
	add_variable('button',$button);
	
	parse_template('newsletterLoop','nLoop',true);
	parse_template('newsletterConfig','cNewsletter',false);
	$templates = return_template('newsletterC');	
	return $templates;
}

function newsletter_members_list($tabs){
	global $db;
	$thisPluginPath = PLUGINS_PATH.'/newsletter';
	$thisPluginURL  = SITE_URL.'/lumonata-plugins/newsletter';
	set_template($thisPluginPath."/members.html",'newsletterC');
	add_block('newsletterLoop','nLoop','newsletterC');
	add_block('newsletterConfig','cNewsletter','newsletterC');
	
	add_variable('plugin_url',$thisPluginURL);
	
	if(empty($_GET['tab']))
		$the_tab='config';
	else
		$the_tab=$_GET['tab'];
	add_variable('tabs',set_tabs($tabs,$the_tab));
	add_actions('section_title','Newsletter Subscriber List');
	add_variable('title','Newsletter Subscriber List');
	
	
	$button = ""; $rule = 'members'; $group = 'newsletter';
	if(is_admin_application())
		$url = get_application_url($group)."&tab=".$rule;
	else
		$url = get_state_url($group)."&tab=".$rule;

	if(!is_contributor() && !is_author()){
		$button.="<li>".button("button=add_new",$url."&a=add")."</li>";
					
	}
	add_variable('button',$button);
	
	
	
	
	$s = $db->prepare_query("SELECT * FROM lumonata_newsletter_member ORDER BY lmember_id");
	$r = $db->do_query($s);
	$iii = 1;
	while($d = $db->fetch_array($r)){
		add_variable('iii',$iii);
		add_variable('member_name', $d['lname']);
		add_variable('member_email', $d['lemail']);
		add_variable('pID', $d['lmember_id']);
		add_variable('eURL', $url."&a=edit&id=".$d['lmember_id']);
		
		if($d['lstatus']==1){
			add_variable('member_status', '<a rel="'.$d['lmember_id'].'" class="status update">approved</a>');
		}else{
			add_variable('member_status', '<a rel="'.$d['lmember_id'].'" class="status update notApproved">pending</a>');
		}
		parse_template('newsletterLoop','nLoop',true);
		$iii++;
	}
	parse_template('newsletterConfig','cNewsletter',false);
	$templates = return_template('newsletterC');	
	return $templates;
}




function newsletter_additional_fields(){	
	global $thepost;
	global $db;
	
	
	//echo $_GET['tab'];
	//if(){}
	//echo web_title();
	
	
	
	$i = $thepost->post_index;
    $post_id = $thepost->post_id;
	if($_GET['state']=='applications') $metaKey = $_GET['sub'];
	else $metaKey = $_GET['state'];
	
	$thisPluginPath = PLUGINS_PATH.'/newsletter';
	set_template($thisPluginPath."/additional-fields.html",'aditionalFields_'.$i);
	add_block('villasAditionalData','dataVillas','aditionalFields_'.$i);
	
	
	add_variable('iii',	$i);
	add_variable('just_site_url', SITE_URL);
	add_variable('postID', $post_id);
	
	if(isset($_POST['additional_fields'])){
		edit_additional_field($post_id, 'publish_start_date', strtotime($_POST['additional_fields']['publish_start_date'][$i]), $metaKey);
		
		
		if(count_additional_field($post_id, 'sent_email', $metaKey)==0){
			add_additional_field($post_id, 'sent_email', 0, $metaKey);
		}
		if(count_additional_field($post_id, 'all_sent', $metaKey)==0){
			add_additional_field($post_id, 'all_sent', 'false', $metaKey);
		}
	}
	
	
	$str = "SELECT lsef FROM lumonata_articles WHERE larticle_id=%d";
	$sql = $db->prepare_query($str, $post_id);
	$res = $db->do_query($sql);
	$nnn = $db->num_rows($res);
	$article = $db->fetch_array($res);
	
	
	$postSef = $article['lsef'];
	if(isset($_POST['sef_box'][$i])){ $postSef=$_POST['sef_box'][$i]; }
	$htmlFname = 'template.html';
	
	$publish_start_date = get_additional_field( $post_id, 'publish_start_date', $metaKey );
	if(!empty($publish_start_date)){
		add_variable('publish_start_date', date('m/d/Y',$publish_start_date));
	}
	
	$selected_template = get_additional_field( $post_id, 'selected_template', $metaKey);
	if(empty($selected_template)){ $selected_template='_default'; }
	add_variable('selected_template', $selected_template);
	
	add_variable('curTime', date('m/d/Y', time()));
	
	

	if(!empty($_FILES)){
		$tmpFile  = $_FILES['additional_fields']['tmp_name']['the_template'][$i];
		$flType   = $_FILES['additional_fields']['type']['the_template'][$i];
		
		$name = str_replace(array('.zip',' '),array('','-'),$_FILES['additional_fields']['name']['the_template'][$i]);
		$tgFolder = $thisPluginPath.'/templates/'.$name.'/';
		
		
		if(!empty($flType)){
			/*
			if (file_exists($tgFolder)) {
				$dir = substr_replace($tgFolder,'',-1,1);
				$objects = scandir($dir);
				foreach ($objects as $object) {
					if ($object != "." && $object != "..") {
						if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
					}
				}
				reset($objects); 
				rmdir($dir);
			}
			*/
			if($flType=='application/zip'){
				custom_file_unzip($tmpFile,$tgFolder);
			}else{
				add_variable('error_alert', '<script>alert("invalid file type uloaded, please upload only a valid zip file.");</script>');
			}
		}
	}
	
	
	//===========================================================================
	$dir = $thisPluginPath.'/templates';
	$objects = scandir($dir);
	$all = ''; $iii = 1;
	foreach ($objects as $object) {
		if ($object != "." && $object != "..") {
			if (filetype($dir."/".$object) == "dir"){
				//echo $object.'<br>';
				$fileExistAlert = '';
				if(!file_exists($dir."/".$object.'/'.$htmlFname)){
					$fileExistAlert = '<p style="font-size:0.8em; padding:0; margin:0; color:#c00"><strong>'.$htmlFname.'</strong> file seems to be missing.</p>';
				}
				
				$previewURL = HTTP.SITE_URL.'/newsletter-preview/'.$object.'/';
				if(!empty($postSef)){$previewURL .= $postSef.'/';}
				
				$deleteLink = '';
				if($iii>1){ $deleteLink = '<a style="cursor:pointer; margin-right:5px;" class="previewButton delete" rel="'.$object.'" >delete</a>'; }
				
				$all .= '<tr class="'.$object.'" >
							<td style="border-top:solid 1px #ccc;">'.$object.$fileExistAlert.'</td>
							<td style="width:160px; text-align:right; border-top:solid 1px #ccc;">
								'.$deleteLink.'<a href="'.$previewURL.'" class="previewButton justPreview" >preview</a>
							</td>
							<td style="width:20px; text-align:center;border-top:solid 1px #ccc;">
								<input type="radio" name="template_select_'.$i.'" rel="template_select_'.$i.'" value="'.$object.'" />
							</td>
						</tr>';
				$iii++;
			}
		}
	}
	add_variable('availableTemplateList', $all);
	add_variable('uiCSSinclude', '<link rel="stylesheet"  media="all"  type="text/css" href="'.HTTP.SITE_URL.'/lumonata-plugins/newsletter/UI/jquery-ui.css" />');
	
	parse_template('villasAditionalData','dataVillas',false);
	$templates = return_template('aditionalFields_'.$i);	
	return $templates;
}

if(get_appname()=='delete-template'){
	add_actions('thecontent', 'delete_template_beserta_isinya');
}
function delete_template_beserta_isinya(){
	add_actions('is_use_ajax',true);
	
	$thisPluginPath = PLUGINS_PATH.'/newsletter';
	
	$uri = explode('/',get_uri());
	$folderName = $uri[1];
	$htmlFname = 'template.html';
	
	$tgFolder = $thisPluginPath.'/templates/'.$folderName.'/';
	
	$dir = substr_replace($tgFolder,'',-1,1);
	$objects = scandir($dir);
	foreach ($objects as $object) {
		if ($object != "." && $object != "..") {
			if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
		}
	}
	reset($objects); 
	rmdir($dir);
	
	return 'delete complete';
}

//========================================================================================= ADMIN  ^^^

function custom_file_unzip($zipFile, $targetDir){
	$zip = new ZipArchive;
	$res = $zip->open($zipFile);
	
	if ($res === TRUE) {
		$zip->extractTo($targetDir);
		$zip->close();
		return true;
	} else {
		echo false;
	}
}


if(get_appname()=='newsletter-preview'){
	add_actions('thecontent', 'preview_newsletter_mail_template');
}
function preview_newsletter_mail_template(){
	global $db;
	add_actions('is_use_ajax',true);
	
	$uri = explode('/',get_uri());
	$folderName = $uri[1];
	$htmlFname = 'template.html';
	
	$thisPluginPath = PLUGINS_PATH.'/newsletter/templates/'.$folderName;
	set_template($thisPluginPath."/".$htmlFname ,'mail_preview');
	add_block('newsletterMail','mPreview','mail_preview');
	
	$nnn = 0;
	if(!empty($uri[2])){
		$str = "SELECT * FROM lumonata_articles WHERE lsef=%s";
		$sql = $db->prepare_query($str, $uri[2]);
		$res = $db->do_query($sql);
		$nnn = $db->num_rows($res);
		$article = $db->fetch_array($res);
	}
	
	//print_r($article);
	if($nnn>0 && !empty($uri[2])){
		add_variable('newsletterTitle', $article['larticle_title']);
		add_variable('newsletterContent', $article['larticle_content']);
		//echo $article['larticle_content'];
		
		$sql = $db->prepare_query("SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lorder ASC LIMIT 0,1",$article['larticle_id']);
		$r = $db->do_query($sql);
		$url = '';
		$d = $db->fetch_array($r);
		
		if( !empty($d['lattach_loc']) ){
			$url = '<img src="'.HTTP.SITE_URL.$d['lattach_loc'].'" />';
		}else{
			$url = '';
		}			
		add_variable('firstAttchedImage', $url );
		
	}else{
		add_variable('newsletterTitle', 'Newsletter Title Is Here');
		add_variable('firstAttchedImage', '<img src="'.HTTP.SITE_URL.'/lumonata-plugins/newsletter/templates/_default/sample-image.jpg" width="100%" />');
		add_variable('newsletterContent', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>');
	}
	
	//theme_url
	add_variable('theme_url', SITE_URL.'/lumonata-plugins/newsletter/templates/'.$folderName );
	
	
	parse_template('newsletterMail','mPreview',false);
	$templates = return_template('newsletterMail');	
	return $templates;
}

?>