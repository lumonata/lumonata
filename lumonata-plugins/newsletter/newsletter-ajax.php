<?php
/*
SEND MODULE FOR NEWSLETTER   
*/

require("../../lumonata_config.php");
$db = new db(HOSTNAME,DBUSER,DBPASSWORD,DBNAME);
require_once("../../lumonata-functions/settings.php");

if(!isset($_COOKIE['user_type'])){ break; }

if($_POST['act']=='toggleStatus'){
	$s = $db->prepare_query("SELECT * FROM lumonata_newsletter_member WHERE lmember_id=%d", $_POST['key']);
	$r = $db->do_query($s);
	$data = $db->fetch_array($r);
	
	if($data['lstatus']==1){
		$s = $db->prepare_query("UPDATE lumonata_newsletter_member SET lstatus=0 WHERE lmember_id=%d", $_POST['key']);
		if($r = $db->do_query($s)){
			echo 'pending';
		}else{
			echo 'failed';
		}
	}else{
		$s = $db->prepare_query("UPDATE lumonata_newsletter_member SET lstatus=1 WHERE lmember_id=%d", $_POST['key']);
		if($r = $db->do_query($s)){
			echo 'approved';
		}else{
			echo 'failed';
		}
	}
}

if($_POST['act']=='deleteMember'){
	$s = $db->prepare_query("DELETE FROM lumonata_newsletter_member WHERE lmember_id=%d", $_POST['key']);
	if($r = $db->do_query($s)){
		echo 'success';
	}else{
		echo 'failed';
	}
}

?>