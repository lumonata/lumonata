<?php
/*
SEND MODULE FOR NEWSLETTER   
*/

require("../../lumonata_config.php");
$db = new db(HOSTNAME,DBUSER,DBPASSWORD,DBNAME);
//kses.php
require_once("../../lumonata-functions/kses.php");
require_once("../../lumonata-functions/settings.php");

//===================================================
$theme=get_meta_data('front_theme','themes');
				
define('TEMPLATE_PATH',ROOT_PATH.'/lumonata-content/themes/'.$theme);
define('TEMPLATE_URL',SITE_URL.'/lumonata-content/themes/'.$theme);

define('FRONT_TEMPLATE_PATH',ROOT_PATH.'/lumonata-content/themes');
define('FRONT_TEMPLATE_URL',SITE_URL.'/lumonata-content/themes');

if(!defined('SITE_URL'))
	define('SITE_URL',get_meta_data('site_url'));

define('PLUGINS_PATH',ROOT_PATH.'/lumonata-plugins');
define('APPS_PATH',ROOT_PATH.'/lumonata-apps');
define('FUNCTIONS_PATH',ROOT_PATH.'/lumonata-functions');
define('CLASSES_PATH',ROOT_PATH.'/lumonata-classes');
define('ADMIN_PATH',ROOT_PATH.'/lumonata-admin');
define('CONTENT_PATH',ROOT_PATH.'/lumonata-content');
//===================================================
//echo TEMPLATE_PATH;
require_once("../../lumonata-functions/template.php");



//sent_email
//all_sent
//selected_template
//publish_start_date
$r = $db->do_query("DROP TEMPORARY TABLE IF EXISTS newsletter_selected_news");
$r = $db->do_query("CREATE TEMPORARY TABLE newsletter_selected_news ( 
					`news_id` bigint(20), 
					`sent_email` bigint(20),  
					`all_sent` varchar(20),  
					`selected_template` text, 
					`publish_start_date` bigint(20) )");

//==============================================================================================
$rule = '';
$s = $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lvalue=%s",'newsletter','all_sent','false');
$r = $db->do_query($s);
while($d = $db->fetch_array($r)){
	$s_q = $db->prepare_query("INSERT INTO `newsletter_selected_news` ( `news_id` , `sent_email` , `all_sent`, `selected_template`, `publish_start_date` )
			VALUES ( %d, %d, %s, %s, %d )", $d['lapp_id'],'','false','','');
	$db->do_query($s_q);
	
	$rule .= ' lapp_id='.$d['lapp_id'].' OR ';
}


//if there are newsletter not sent yet ^_^
if(!empty($rule)){
	$rule = ' AND ('.substr_replace($rule,'',-3).' )';
	$s = $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lkey=%s ".$rule,'sent_email');
	$r = $db->do_query($s);
	while($d = $db->fetch_array($r)){
		$s_q = $db->prepare_query("UPDATE newsletter_selected_news SET sent_email=%d WHERE news_id=%d", $d['lvalue'], $d['lapp_id']);
		$db->do_query($s_q);
	}
	
	$s = $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lkey=%s ".$rule,'selected_template');
	$r = $db->do_query($s);
	while($d = $db->fetch_array($r)){
		$s_q = $db->prepare_query("UPDATE newsletter_selected_news SET selected_template=%s WHERE news_id=%d", $d['lvalue'], $d['lapp_id']);
		$db->do_query($s_q);
	}
	
	$s = $db->prepare_query("SELECT * FROM lumonata_additional_fields WHERE lkey=%s ".$rule,'publish_start_date');
	$r = $db->do_query($s);
	while($d = $db->fetch_array($r)){
		$s_q = $db->prepare_query("UPDATE newsletter_selected_news SET publish_start_date=%d WHERE news_id=%d", $d['lvalue'], $d['lapp_id']);
		$db->do_query($s_q);
	}
	
	//===========================================================================================
	
	//$s = $db->prepare_query("SELECT COUNT(lemail) AS n_email FROM lumonata_newsletter_member WHERE lstatus=%d", 1);
	//$r = $db->do_query($s);
	//$tmp = $db->fetch_array($r);
	
	$config_vars = array('email_per_send','newsletter_notif_email','newsletter_smtp');
	$config = array();
	foreach($config_vars as $c_var){
		$config[$c_var] = get_meta_data($c_var);
	}
	
	$s = $db->prepare_query("SELECT * FROM lumonata_articles LEFT JOIN newsletter_selected_news 
							   ON lumonata_articles.larticle_id=newsletter_selected_news.news_id 
							   WHERE publish_start_date<=%d AND publish_start_date>0", time());
	$r = $db->do_query($s);
	while($d = $db->fetch_array($r)){
		send_newsletter_now($d, $config);
	}
}else{
	//all newsletter sent to approved members at the time
}





//===========================================================================================
function send_newsletter_now($news, $config){
	global $db;
	$email_sent = $news['sent_email'];
	
	//==========================================================================
	$htmlFname = 'template.html';
	$folderName = $news['selected_template'];
	$unique = '__'.$news['larticle_id'];
	
	$thisPluginPath = PLUGINS_PATH.'/newsletter/templates/'.$folderName;
	set_template($thisPluginPath."/".$htmlFname ,'mail_preview'.$unique);
	add_block('newsletterMail','mPreview','mail_preview'.$unique);
		
	add_variable('theme_url', SITE_URL.'/lumonata-plugins/newsletter/templates/'.$folderName );
	
	
	
	$limit = ' LIMIT '.$email_sent.','.$config['email_per_send'].' ';
	//============================================================================
	//============================================================================
	$s1 = "SELECT * FROM lumonata_rule_relationship LEFT JOIN lumonata_rules ON lumonata_rule_relationship.lrule_id=lumonata_rules.lrule_id WHERE lapp_id=%d";
	$q1 = $db->prepare_query($s1, $news['larticle_id']);
	$r1 = $db->do_query($q1);
	$n1 = $db->num_rows($r1);
	if($n1>1){
		$rule = '';
		while($d1 = $db->fetch_array($r1)){ $rule .= ' lcat_id='.$d1['lrule_id'].' OR '; }
		$rule = substr_replace(trim($rule),'',-2,2);
		$s = $db->prepare_query("SELECT * FROM lumonata_newsletter_member WHERE lstatus=%d AND (".$rule.") ORDER BY lmember_id ASC ", 1);
	}else if($n1==1){
		$d1 = $db->fetch_array($r1);
		if($d1['lsef']=='uncategorized'){
			$s = $db->prepare_query("SELECT * FROM lumonata_newsletter_member WHERE lstatus=%d ORDER BY lmember_id ASC ", 1);
		}else{
			$s = $db->prepare_query("SELECT * FROM lumonata_newsletter_member WHERE lstatus=%d AND lcat_id=%d ORDER BY lmember_id ASC ", 1, $d1['lrule_id']);
		}
	}else{
		$s = $db->prepare_query("SELECT * FROM lumonata_newsletter_member WHERE lstatus=%d ORDER BY lmember_id ASC ", 1);
	}
	$sn = $s;
	$s  = $s.$limit;
	//============================================================================
	//============================================================================
	//echo $s;
	
	$rn = $db->do_query($sn);
	$total_emails = $db->num_rows($rn);
	

	$r = $db->do_query($s);
	$nnnnnn = $db->num_rows($r);
	$send_error = false;
	$iii = 0;
	while($member = $db->fetch_array($r)){
		$smtp_server = $config['newsletter_smtp'];
		$sendFrom = $config['newsletter_notif_email'];
		$sendTo   = $member['lemail'];
		$sitename = get_meta_data('web_title');
		
		//----------------------------------------------------------------
		add_variable('newsletterTitle', $news['larticle_title']);
		add_variable('newsletterContent', $news['larticle_content']);
		//echo $article['larticle_content'];
		
		$sql = $db->prepare_query("SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lorder ASC LIMIT 0,1",$news['larticle_id']);
		$r2 = $db->do_query($sql);
		$url = '';
		$d = $db->fetch_array($r2);
		
		if( !empty($d['lattach_loc']) ){
			$url = '<img src="'.HTTP.SITE_URL.$d['lattach_loc'].'" />';
		}else{
			$url = '';
		}			
		add_variable('firstAttchedImage', $url );
		add_variable('unsubscribeLink', HTTP.SITE_URL.'/unsubscribe-newsletter/'.base64_encode($sendTo).'/' );
		add_variable('newsPageLink', HTTP.SITE_URL.'/newsletter/'.$news['lsef'].'/' ); //newsPageLink
		//----------------------------------------------------------------
		
		parse_template('newsletterMail','mPreview',false);
		$theMailContent = return_template('mail_preview'.$unique);	
		
		//----------------------------------------------------------------
		
		
		
		
		ini_set("SMTP", $smtp_server);
		ini_set("sendmail_from", $sendFrom);
		
		$subject  = $news['larticle_title'];
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";			
		$headers .= "From: $sitename <$sendFrom>\r\n";
		
		echo '=================<br>';
		//echo '|'.$theMailContent.'|';
		echo 'send to : '.$member['lemail'].'<br>';
		$send = mail($sendTo,"$subject","$theMailContent","$headers");
		if(!$send){
			echo 'not success<br><br>';
			$send_error = true;
		}else{
			echo 'success<br><br>';
			$sentSuccess[] = $member;
		}
	}
	
	
	
	
	//$sentSuccess[]['email'] = $news['larticle_title']
	if(count($sentSuccess)>0){
		echo '------ count success : '.count($sentSuccess);
		$smtp_server = $config['newsletter_smtp'];
		$sendFrom = $config['newsletter_notif_email'];
		$sendTo   = $member['lemail'];
		$sitename = get_meta_data('web_title');
		
		ini_set("SMTP", $smtp_server);
		ini_set("sendmail_from", $sendFrom);
		
		$sendTo   = $config['newsletter_notif_email'];
		
		$sitename = get_meta_data('web_title');
		$subject  = "Newsletter Send Report";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";			
		$headers .= "From: Newsletter send report - $sitename <cron@balirealtyhv.com>\r\n";
		
		$iii = 1; $list = '';
		//print_r($sentSuccess);
		foreach($sentSuccess as $member){
			$list .= $iii.'. '.$member['lname'].' - '.$member['lemail'].'<br>';
			$iii++;
		}
		
		$content  = '<p><strong>"'.$news['larticle_title'].'"</strong> has been sent to following email address ('.count($sentSuccess).' of '.$nnnnnn.'):</p>';
		$content .= '<p>'.$list.'</p>';
		
		$send = mail($sendTo,"$subject","$content","$headers");

		if($send){
			echo '-- report success --';
		}else{
			echo '-- report failed --';
		}
	}
	
	
	if(!$send_error){
		$metaKey = 'newsletter';
		$sentEmails = $email_sent + $config['email_per_send'];
		
		if($sentEmails>=$total_emails){
			$sentEmails=$total_emails;
			edit_additional_field($news['larticle_id'], 'all_sent', 'true', $metaKey);
		}
		
		edit_additional_field($news['larticle_id'], 'sent_email', $sentEmails, $metaKey);
	}
	
}
?>