<?php
/*
    Plugin Name: Event Baners
    Plugin URL: https://www.lumonata.com/
    Description: Plugin for arunna framework that can set banners with 2 backgrounds, one fixed and one repeted
    Author: Andi Darmika
    Author URL: https://www.aankun.com/
    Version: 1.0

*/

function event_banner_config_page(){

	$alert="";
	add_actions('section_title','Event Banner');

	$imageType[] = array('type' => 'image/jpeg', 'ext'=>'.jpg');
	$imageType[] = array('type' => 'image/png',  'ext'=>'.png');
	$imageType[] = array('type' => 'image/gif',  'ext'=>'.gif');

	if(isset($_POST['deleted_image'])){
		$tmp = get_meta_data($_POST['deleted_image']);
		$filename = '../'.$tmp;
		unlink($filename);
		$update = update_meta_data($_POST['deleted_image'],'');
	}
	if(isset($_POST['save_changes'])){
		foreach ($_POST as $key=>$val){
			if($key!="save_changes" || $key!='deleted_image'){
				if(find_meta_data($key))
					$update = update_meta_data($key,$val);
				else
					$update = set_meta_data($key,$val);
			}
		}
		//print_r($_FILES);
		if(isset($_FILES)){
		$invalidFile = false;
		$uploadFailed = false;
		foreach ($_FILES as $key=>$val){
			if(!empty($val['name']) ){
				$typeOK = false;
				foreach($imageType as $type){ if($type['type']==$val['type']){ $typeOK=true; $selType=$type; } }
				if($typeOK){
					$tmp = get_meta_data($key);
					if(!empty($tmp)){ $filename = '../'.$tmp; unlink($filename); }

					if ( move_uploaded_file($val["tmp_name"], '../lumonata-plugins/eventBanners/images/'.$key.$selType['ext']) ){
						if(find_meta_data($key))
							$update = update_meta_data($key,'/lumonata-plugins/eventBanners/images/'.$key.$selType['ext']);
						else
							$update = set_meta_data($key,'/lumonata-plugins/eventBanners/images/'.$key.$selType['ext']);
					}else{
						//echo 'upload failed [] ';
						$uploadFailed = true;
					}
				}else{
					//echo 'invalid file type';
					$invalidFile = true;
				}
			}
		}}

		if($update && !$invalidFile && !$uploadFailed)
			$alert="<div class=\"alert_green_form\">Data has been updated.</div>";
		else if($update && $invalidFile && !$uploadFailed)
			$alert="<div class=\"alert_green_form\">Data partially updated<br />but one/some file are invalid, please upload .jpg, .png and .gif only.</div>";
		else if($update && !$invalidFile && $uploadFailed)
			$alert="<div class=\"alert_green_form\">Data partially updated<br />but one/some file are failed to upload.</div>";
		else
			$alert="<div class=\"alert_green_form\">Some/all data failed to update/uploaded.</div>";
	}

	$event_title=get_meta_data("event_title");
	$static_event_image=get_meta_data("static_event_image");
	$repeat_event_image=get_meta_data("repeat_event_image");
	$custom_badge_image=get_meta_data("custom_badge_image");

	if(!empty($static_event_image)){ $staticPreview = '<a href="'.HTTP.SITE_URL.$static_event_image.'" class="eventImagePreview" style="margin-left:15px;">preview</a>
													   <a href="#deleteThis" class="deleteThisImage" rel="static_event_image" style="margin-left:15px;">delete</a>'; }else{$staticPreview='';}
	if(!empty($repeat_event_image)){ $repeatPreview = '<a href="'.HTTP.SITE_URL.$repeat_event_image.'" class="eventImagePreview" style="margin-left:15px;">preview</a>
													   <a href="#deleteThis" class="deleteThisImage" rel="repeat_event_image" style="margin-left:15px;">delete</a>'; }else{$repeatPreview='';}
	if(!empty($custom_badge_image)){ $badgePreview  = '<a href="'.HTTP.SITE_URL.$custom_badge_image.'" class="eventImagePreview" style="margin-left:15px;">preview</a>
													   <a href="#deleteThis" class="deleteThisImage" rel="custom_badge_image" style="margin-left:15px;">delete</a>'; }else{$badgePreview='';}

	$return="
			<script>
			jQuery(document).ready(function(){
				jQuery('a.eventImagePreview').colorbox({
					maxWidth:\"100%\",
					maxHeight:\"100%\"
				});

				jQuery('a.deleteThisImage').click(function(){
					var rel = jQuery(this).attr('rel');
					jQuery('[name=deleted_image]').val(rel);
					jQuery('#theDeleteImageForm').submit();
				});
			});
			</script>
			<h1>Event Banner</h1>
			<p>This settings will change your meta data on your home page</p>
				<div class=\"tab_container\">
    				<div class=\"single_content\">
						$alert
        				<form method=\"post\" action=\"#\" enctype=\"multipart/form-data\">
        					<fieldset>
								<p><label>Event Title:</label></p>
								<input type=\"text\" id=\"event_title\" name=\"event_title\" value=\"$event_title\" class=\"textbox\" style=\"height:20px;\" />
						    </fieldset>
							<fieldset>
								<p><label>Static Image:</label></p>
								<input type=\"file\" id=\"static_event_image\" name=\"static_event_image\" />".$staticPreview."
						    </fieldset>
							<fieldset>
								<p><label>Repeated Image:</label></p>
								<input type=\"file\" id=\"repeat_event_image\" name=\"repeat_event_image\" />".$repeatPreview."
						    </fieldset>
							<fieldset>
								<p><label>Badge Image:</label></p>
								<input type=\"file\" id=\"custom_badge_image\" name=\"custom_badge_image\" />".$badgePreview."
						    </fieldset>
						    <div class=\"button_wrapper clearfix\">
						       <ul class=\"button_navigation\">
							   <li>".save_changes_botton()."</li>
						       </ul>
						    </div>
        				</form>
						<form method=\"post\" id=\"theDeleteImageForm\" >
							<input type=\"text\" id=\"deleted_image\" name=\"deleted_image\" value=\"\" class=\"textbox\" />
						</form>
        			</div>
        		</div>";
	return $return;
}
/*This is the first important step that you have to do.
 * Who are allowed to access the application
 * */
add_privileges('administrator', 'eventBanner', 'insert');
add_privileges('administrator', 'eventBanner', 'update');
add_privileges('administrator', 'eventBanner', 'delete');




$event_title=get_meta_data("event_title");
$static_event_image=get_meta_data("static_event_image");
$repeat_event_image=get_meta_data("repeat_event_image");
$custom_badge_image=get_meta_data("custom_badge_image");

add_actions("theEventTitle",$event_title);
add_actions("eventStaticImage",$static_event_image);
add_actions("eventRepeatImage",$repeat_event_image);
add_actions("eventBadgesImage",$custom_badge_image);
/*
 * Add sub menu under applications menu
 *
 * */
add_apps_menu(array('eventBanner'=>'Event Banner'));
/*
 * After adding sub menu, Now we have to add actions that will taking the process to set the meta data for the home page
 * Please keep in mind that the name of the menu variable(metadata), must be the same with the name of actions varibale bellow
 * */
add_actions("eventBanner","event_banner_config_page");

/* Add the Plugin at Applications Set
// add_actions("plugin_menu_set","<option value=\"metadata\">Meta Data</option>");
*/

/* Add Plugin at Main Menu

// Add the array to main menu
add_main_menu(array('metadata'=>'Meta Data'));

//If the Menu has sumenu
add_sub_menu('metadata',array('meta_title'=>'Meta Title','meta_desc'=>'Meta Description'));

//Don't Forget to set the previlage for each user type
add_privileges('administrator','metadata','insert');

// Configure the CSS so the menu will appear at main menu
function menu_css(){
    return "<style type=\"text/css\">.lumonata_menu ul li.meta_data{
                                background:url('../lumonata-plugins/metadata/images/ico-themes.png') no-repeat left top;
                                }</style>";
}

//Function executed when sub menu Meta Title hit
function meta_title($tes){
    add_actions('section_title','Meta');
    return $tes;
}

//Attemp the CSS to header
add_actions('header_elements','menu_css');

//Add actions when Meta Title sub menu were hit. In this action, it wil call meta_title function above
add_actions('meta_title','meta_title','tes');
*/


/*
 * 	Here are how to add plugin that call the get_admin_article function with additional tabs

	//$tabs=array('meta_settings'=>'Settings');
	//add_actions('metadata','get_admin_article','metadata','Hotel|Hotels',$tabs);
 *
 */
?>
