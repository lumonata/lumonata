<?php 
/*
    Plugin Name: Mail Chimp
    Plugin URL: http://www.lumonata.com/
    Description: Plugin for arunna framework
    Author: Adi Juliartha
    Author URL: http://www.adijuliartha.com/
    Version: 1.0    
*/

function subscribe_newsletter(){
	add_actions('is_use_ajax',true);
	$email = $_POST['email'];
	$name = $_POST['name'];
	$api_key = get_meta_data('api_key_mailchimp_newsletter');
	$list_id = get_meta_data('list_id_mailchimp_newsletter');
	//echo "$email,$api_key,$list_id";
	$return = array();
	if(subscribe_mailchimp($email,$name,$list_id,$api_key)){
		$return['status'] = 'success';
		$return['message']='Thank you for signing up newsletter. Please check your email to confirm email.';
	}else{
		$return['status'] = 'failed';
		$return['message']='Failed signing up. Please try again later.';
	}
	echo json_encode($return);
}
add_actions('mailchimp-suscribe_page','subscribe_newsletter');

function subscribe_mailchimp($email,$name,$list_id,$api_key){//return true;
	require_once(ROOT_PATH.'/mailchimp.php');
	$Mailchimp = new Mailchimp( $api_key );
	$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
	try {
		$subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => htmlentities($email),'fname'=>$name));
		if (!empty( $subscriber['leid']))  return true;
	} catch (Exception $e) {
		return false;
	}
}

?>