<?php 
function send_to_admin_old($name,$from,$content,$subject){
	require_once(ROOT_PATH.'/PHPMailer/PHPMailerAutoload.php');
	$mail = new PHPMailer;

	$SMTP_PORT 	= 2525;
	$SMTP_SERVER = get_meta_data('smtp');	
	$email_smtp			= get_meta_data('email_user_smtp');
	$pass_smtp			= json_decode(base64_decode(get_meta_data('pass_email_user_smtp')));
	$pass_smtp 			= $pass_smtp->p_e_u_smtp;

	$web_name 		= trim(get_meta_data('web_name'));
	$admin_email 		= get_meta_data('ci_email');
	//echo "$SMTP_SERVER,$SMTP_PORT,$email_smtp,$pass_smtp,$web_name,$admin_email	";//exit;
	
	try {
		$mail->CharSet = "utf-8";
		
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = "mail.lumonata.com"; // SMTP server
		$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
		                                           // 1 = errors and messages
		                                           // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
	    // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($from,$name);
		$mail->AddAddress($admin_email,$web_name); //karena kirim ke diri sendiri
		$mail->SetFrom("adijuliartha@gmail.com", $name);
		$mail->Subject = $subject;
		$mail->MsgHTML($content);
		
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";  
		$mail->send();
		
		return true;
	}catch (phpmailerException $e) {
		return false;
	}catch (Exception $e) {
		return false;
	}
}//mail.lumonata.com



function send_to_admin_old_2($name,$from,$content,$subject){
	require_once(ROOT_PATH.'/PHPMailer/PHPMailerAutoload.php');
	$mail = new PHPMailer;

	$SMTP_PORT 	= 587;
	$SMTP_SERVER = get_meta_data('smtp');	
	$email_smtp			= get_meta_data('email_user_smtp');
	$pass_smtp			= json_decode(base64_decode(get_meta_data('pass_email_user_smtp')));
	$pass_smtp 			= $pass_smtp->p_e_u_smtp;

	

	$web_name 		= trim(get_meta_data('web_name'));
	$admin_email 		= get_meta_data('ci_email');
	//echo "$SMTP_SERVER,$SMTP_PORT,$email_smtp,$pass_smtp,$web_name,$admin_email	";//exit;
	
	try {

		$mail->CharSet = "utf-8";
		//$mail->SMTPDebug  = 1;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;

		$mail->SMTPSecure = false;
		$mail->SMTPAutoTLS = false;
		//$mail->SMTPSecure = "tls";
		//$mail->Port       = $SMTP_PORT; 
		
		/*$mail->Port = 25;
		$mail->SMTPAuth = false;
		$mail->SMTPSecure = false;*/

		
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($from,$name);
		$mail->AddAddress($admin_email,$web_name); //karena kirim ke diri sendiri
		$mail->SetFrom($email_smtp, $name);
		$mail->Subject = $subject;
		$mail->MsgHTML($content);
		
		$mail->send();
		
		return true;


		/*Z$mail->SMTPDebug  = 2;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->Port       = $SMTP_PORT; 
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($from,$name);
		$mail->AddAddress($admin_email,$web_name); //karena kirim ke diri sendiri
		$mail->SetFrom($email_smtp, $name);
		$mail->Subject = $subject;
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($content);
		//print_r($mail); exit;
		$mail->Send();
		return true;*/
	}catch (phpmailerException $e) {
		return false;
	}catch (Exception $e) {
		return false;
	}
}//mail.lumonata.com

function send_to_admin($name,$from,$content,$subject){
	require_once(ROOT_PATH.'/PHPMailer/PHPMailerAutoload.php');
	$mail = new PHPMailer;

	$SMTP_PORT 	= 587;
	$SMTP_SERVER 		= get_meta_data('smtp');	
	$email_smtp			= get_meta_data('email_user_smtp');
	$pass_smtp			= json_decode(base64_decode(get_meta_data('pass_email_user_smtp')));
	$pass_smtp 			= $pass_smtp->p_e_u_smtp;
	$web_name 			= trim(get_meta_data('web_name'));
	$admin_email 		= get_meta_data('ci_email');
	//echo "$SMTP_SERVER,$SMTP_PORT,$email_smtp,$pass_smtp,$web_name,$admin_email	";//exit;
	
	try {

		$mail->CharSet = "utf-8";
		//$mail->SMTPDebug  = 1;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($from,$name);
		$mail->AddAddress($admin_email,$web_name); //karena kirim ke diri sendiri
		$mail->SetFrom($email_smtp, $name);
		$mail->Subject = $subject;
		$mail->MsgHTML($content);
		$mail->send();
		return true;
	}catch (phpmailerException $e) {
		return false;
	}catch (Exception $e) {
		return false;
	}
}




function validate_capcha(){
	if(isset($_POST['response'])){
		if(isset($_POST['use_postman']) && $_POST['use_postman']=='yoman')return true;
		$secret_key = get_meta_data("capcha_secret_key");
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		
		$params = array();
		$params['remoteip'] = $_SERVER['REMOTE_ADDR'];
		$params['secret']   = $secret_key;
		$params['response'] = $_POST['response'];
		
		$prm_string = http_build_query($params);
		$requestURL = 'https://www.google.com/recaptcha/api/siteverify?'.$prm_string;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $requestURL,
			CURLOPT_SSL_VERIFYPEER => false
		));
		
		$data = curl_exec($curl);
		curl_close($curl);
		
		$response = json_decode($data);
		if($response->success)return true;			
		else return false;
	}
}



function ajax_contact(){
	$return  = array();
	//	$valid_capcha = validate_capcha();
	$valid_capcha = true;	
	if($valid_capcha){
		$name = $_POST['name'];
		$email = $_POST['email'];
		$describe_product_service = $_POST['describe_product_service'];
		$service_interest = $_POST['service_interest'];
		$project_timeline = $_POST['project_timeline'];
		$budget = $_POST['budget'];
		$newsletter = $_POST['newsletter'];
		$more_detail = $_POST['more_detail'];
		
		$is_email = isValidEmail($email);
		if($name!='' && $email!=''  &&  $is_email && $describe_product_service!='' && $newsletter!=''){
			set_template(PLUGINS_PATH.'/contact_info/template_inquiry.html','email-temp');
			add_block('inquiryBlock','inquiry_block','email-temp');
			
			add_variable('name',$name);
			add_variable('email',$email);
			add_variable('describe_product_service',nl2br($describe_product_service));
			add_variable('service_interest',(trim($service_interest) !="" ? nl2br($service_interest) : '-'));
			add_variable('project_timeline',(trim($project_timeline) !="" ? nl2br($project_timeline):'-'));
			add_variable('budget',(trim($budget)!=""? $budget:'-'));
			add_variable('more_detail',(trim($more_detail)!=""? nl2br($more_detail):'-'));
			
			parse_template('inquiryBlock','inquiry_block',false);
			$message = return_template('email-temp');	
			$subject = "Inquiry Form";
			if(send_to_admin($name,$email,$message,$subject)){
				$api_key = get_meta_data('api_key_mailchimp_newsletter');
				$list_id = get_meta_data('list_id_mailchimp_newsletter');
				//echo "$newsletter=='yes',$email,$name,$list_id,$api_key ";
				//print_r($_POST);
				//$newsletter = "yes";
				if($newsletter == 1) {
					subscribe_mailchimp($email,$name,$list_id,$api_key);
				}
				$return['status']='success';
				$return['message']='Thank you for your inquiry we will get back to you as soon as possible.';
			}else{
				$return['status']='failed';
				$return['message']='Failed send email. Please try again later.';
			}
			
		}else{
			$return['status']='failed';
			$return['message']='Failed send email. Please try again later.';
		}
		
	}else{
		$return['status'] = 'failed';
		$return['message'] = 'Error capcha validation. Failed to send inquiry.';
	}
	
	echo json_encode($return);
	exit;
}

function isValidEmail($email){
	return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $email);
}

function is_suscribe_email(){
	$api_key = get_meta_data('api_key_mailchimp_newsletter');
	$list_id = get_meta_data('list_id_mailchimp_newsletter');
	$emails = $_POST['email'];
	
	require_once(ROOT_PATH.'/mailchimp.php');
	$Mailchimp = new Mailchimp( $api_key );
	$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
	try {
		//$subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => htmlentities($email),'fname'=>$name));
		//echo echo 'dasda';
		
		//$subscriber = $Mailchimp_Lists->memberInfo($list_id, $emails);
		$subscriber = $Mailchimp_Lists->memberInfo( $list_id, array( 'email' => htmlentities($email),'fname'=>'no-name'));
		print_r($subscriber);
		//if (!empty( $subscriber['leid']))  return true;
	} catch (Exception $e) {
		echo $e;
		return false;
	}
	
	
	//memberInfo($id, $emails)
	
	//if(subscribe_mailchimp($email,$name,$list_id,$api_key)){}	
}
	
add_actions('is-suscribe-email_page','is_suscribe_email');
add_actions('send-inquiry_page','ajax_contact');
?>