<?php
/*
    Plugin Name: Contact Info
    Plugin URL: http://www.lumonata.com/
    Description: Plugin for arunna framework.
    Author: Andi Darmika
    Author URL: http://www.aankun.com/
    Version: 1.0
    
*/

function contact_info_config_page(){
	set_template(PLUGINS_PATH."/contact_info/template.html",'cInfoArea');
	add_block('contactInfo','cInfo','cInfoArea');
	
	//======================================================================================================Save Part  
	if(isset($_POST['save_changes'])){
		$error = false;
		foreach ($_POST as $key=>$val){
			if( $key!= "save_changes" ){
				if(find_meta_data($key))
					$update = update_meta_data($key,$val);
				else 
					$update = set_meta_data($key,$val);
					
				if(!$update){ $error = true; }
			}
		}
		if($error){
			$alert = "<div class=\"alert_green_form\">Failed to update.</div>";
		}else{
			$alert = "<div class=\"alert_green_form\">Data has been updated.</div>";
		}
	}
	//======================================================================================================Save Part  ^^^
	$alert_config             = get_meta_data( 'alert_config' );
    if( $alert_config == 0 )
    {
        add_variable( 'alert_config_inactive', 'selected' );
        add_variable( 'alert_config_active', '' );
    }
    else
    {
        add_variable( 'alert_config_inactive', '' );
        add_variable( 'alert_config_active', 'selected' );
    }
	
	add_variable('theSaveButton', save_changes_botton());
	
	add_variable('ci_email_address', get_meta_data("ci_email_address"));
	add_variable('ci_phone_number', get_meta_data("ci_phone_number"));
	add_variable('ci_text_ta', get_meta_data("ci_text_ta"));
	add_variable('ci_link_ta', get_meta_data("ci_link_ta"));

	add_variable('ci_bbm', get_meta_data("ci_bbm"));
	add_variable('ci_wap', get_meta_data("ci_wap"));
	add_variable('ci_viber', get_meta_data("ci_viber"));	
	add_variable('ci_address', get_meta_data("ci_address"));
	add_variable('ci_opening_hour', get_meta_data("ci_opening_hour"));
	
	add_variable('ci_skype', get_meta_data("ci_skype"));
	add_variable('ci_linkedin', get_meta_data("ci_linkedin"));
	add_variable('ci_instagram', get_meta_data("ci_instagram"));
	add_variable('ci_facebook_url', get_meta_data("ci_facebook_url"));
	add_variable('ci_facebook_iframe',get_meta_data("ci_facebook_iframe"));
	add_variable('ci_google_plus', get_meta_data("ci_google_plus"));
	add_variable('ci_youtube', get_meta_data("ci_youtube"));		
	add_variable('ci_twitter_url', get_meta_data("ci_twitter_url"));
	add_variable('ci_medium_url', get_meta_data("ci_medium_url"));
	add_variable('ci_email', get_meta_data("ci_email"));
	
	
	parse_template('contactInfo','cInfo',true);
	return return_template('cInfoArea');
}

add_privileges('administrator', 'contactInfo', 'insert');
add_privileges('administrator', 'contactInfo', 'update');
add_privileges('administrator', 'contactInfo', 'delete');

add_apps_menu(array('contactInfo'=>'Contact Info'));
add_actions("contactInfo","contact_info_config_page");

require_once('contact_actions.php')
/*
$event_title = get_meta_data("event_title");
$static_event_image = get_meta_data("static_event_image");
$repeat_event_image = get_meta_data("repeat_event_image"); 
add_actions("theEventTitle", $event_title);
add_actions("eventStaticImage", $static_event_image);
add_actions("eventRepeatImage", $repeat_event_image);
*/

?>