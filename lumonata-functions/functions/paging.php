<?
class paging{
	var $top_page;
	var $view;
	var $limit_view;
	var $num_rows;
	var $page_view;
	function paging($top_page,$view,$num_rows,$page_view){
		$this->top_page=$top_page;
		$this->view=$view;
		$this->num_rows=$num_rows;
		$this->page_view=$page_view;

	}
	function page_count(){
		$kel=$this->num_rows/$this->view;
		if ($kel==floor($this->num_rows/$this->view)){
			return $kel;
		}
		else{
			return floor($this->num_rows/$this->view)+1;
		}
	}
	function pagination($url){
		$globalAdmin = new globalAdmin();
		$BNext = $globalAdmin->getValueField2("lumonata_alert","llabel","lname","buttonNext","llang_id",LANGUAGE_ID);
		$BPrev = $globalAdmin->getValueField2("lumonata_alert","llabel","lname","buttonPrev","llang_id",LANGUAGE_ID);
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		//echo $url;
		$html="<div id=\"paging\"><ul>";
		//<li><a href=\"$url"."1/\">First Page</a></li>
		if ($this->top_page!=1)
		{
			//$html.="<li><a href=\"$url".$prev."/#contentlist\" title=\"Prev\"><img src=\"https://".THEME_URL."/images/prev.png\"></a></li>";
			$html.="<li><a href=\"$url".$prev."/\" title=\"Prev\"><span id=\"prevs\">$BPrev</span></a></li>";

		}else{

			$html.="<li><span id=\"prev\">$BPrev</span></li>";

		}

		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;

		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){

			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			$page_go=$this->page_count();

		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan

		for($i=$top_go;$i<=$page_go;$i++)

		{

			if ($i==$this->top_page)

			{
				$html.= "<li><span id=\"huruf\">$i</span></li>";

			}else{
				//$html.= "<li><a href=\"$url".$i."/#contentlist\" title=\"$i\" >$i</a></li>";
				$html.= "<li><a href=\"$url".$i."/\" title=\"$i\" ><span id=\"hurufs\">$i</span></a></li>";

			}

		}

		if ($this->top_page!=$this->page_count())

		{
			//$html.="<li><a href=\"$url".$next."/#contentlist\" title=\"Next\"><img src=\"https://".THEME_URL."/images/next.png\"></a></li>";
			$html.="<li><a href=\"$url".$next."/\" title=\"Next\"><span id=\"nexts\">$BNext</span></a></li>";

		}else{
			$html.= "<li><span id=\"next\">$BNext</span></li>";

		}
		//<li><a href=\"$url".$this->page_count()."/\">Last Page</a></li>
		$html.="</ul></div>";


		return $html;

	}

	function searchPagination($url){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);

		$html="<div id=\"paging\"><ul><li><a href=\"$url"."1/\">First Page</a></li>";

		if ($this->top_page!=1)
		{

			$html.="<li><a href=\"$url".$prev."/\" title=\"Prev\">Prev</a></li>";

		}else{

			$html.="<li>Prev</li>";

		}

		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;

		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){

			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			$page_go=$this->page_count();

		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan

		for($i=$top_go;$i<=$page_go;$i++)

		{

			if ($i==$this->top_page)

			{

				$html.= "<li><span style=\"font-size:14px;\">$i</span></li>";

			}else{

				$html.= "<li><a href=\"$url".$i."\" title=\"$i\" >$i</a></li>";

			}

		}

		if ($this->top_page!=$this->page_count())

		{

			$html.="<li><a href=\"$url".$next."\" title=\"Next\">Next</a></li>";

		}else{
			$html.= "<li>Next</li>";

		}

		$html.="<li><a href=\"$url".$this->page_count()."\">Last Page</a></li></ul></div>";


		return $html;

	}

	function jsPagination($url){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);

		$html="<ul><li><a href=\"$url"."1')\">&lt;&lt;</a></li>";

		if ($this->top_page!=1)
		{

			$html.="<li><a href=\"$url".$prev."')\" title=\"Prev\">&lt;</a></li>";

		}else{

			$html.="<li><span style=\"font-size:11px;\">&lt;</span></li>";

		}

		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;

		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){

			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			$page_go=$this->page_count();

		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan

		for($i=$top_go;$i<=$page_go;$i++)

		{

			if ($i==$this->top_page)

			{

				$html.= "<li><span style=\"font-size:11px;\">$i</span></li>";

			}else{

				$html.= "<li><a href=\"$url".$i."')\" title=\"$i\" >$i</a></li>";

			}

		}

		if ($this->top_page!=$this->page_count())

		{

			$html.="<li><a href=\"$url".$next."')\" title=\"Next\">&gt;</a></li>";

		}else{
			$html.= "<li><span style=\"font-size:11px;\">&gt;</span></li>";

		}

		$html.="<li><a href=\"$url".$this->page_count()."')\">&gt;&gt;</a></li></ul>";


		return $html;

	}
	function nextPrevPagination($url,$prev_icon,$next_icon){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);

		//$html="<ul><li><a href=\"$url"."1/\"></a></li>";
		$html="<ul>";
		if ($this->top_page!=1)
		{

			$html.="<li style=\"padding:20px 0 0 0;\">
					<a href=\"$url".$prev."/#tab\" title=\"Prev\">
						<img src=\"$prev_icon\" border=\"0\" width=\"10\" />
					</a>
					</li>";

		}else{

			$html.="<li><img src=\"$prev_icon\" border=\"0\" width=\"10\" /></li>";

		}

		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;

		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){

			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			$page_go=$this->page_count();

		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}

			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan

		for($i=$top_go;$i<=$page_go;$i++)

		{

			if ($i==$this->top_page)

			{

				$html.= "<li>$i</li>";

			}else{

				$html.= "<li><a href=\"$url".$i."/\" title=\"$i\" >$i</a></li>";

			}

		}
		$html.=" / ".$this->page_count();
		if ($this->top_page!=$this->page_count())

		{

			$html.="<li><a href=\"$url".$next."/#tab\" title=\"Next\">&nbsp;&nbsp;<img src=\"$next_icon\" border=\"0\" width=\"10\" /></a></li>";

		}else{
			$html.= "<li>&nbsp;<img src=\"$next_icon\" border=\"0\" width=\"10\" /></li>";

		}

		//$html.="<li><a href=\"$url".$this->page_count()."/\">Last Page</a></li></ul>";
		$html.="</ul>";

		return $html;

	}
}

?>
