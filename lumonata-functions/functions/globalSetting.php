<?php
	class globalSetting extends db{
		var $adminList;
		var $frontList;
		var $autoThumbs;
		var $autoCode;
		var $address;
		var $email;
		var $cc;
		var $bcc;
		var $metaTitle;
		var $metaKeywords;
		var $metaDescriptions;
		var $latestProducts;
		var $meta_title;
		var $meta_key;
		var $meta_desc;
		var $commentStatus;
		var $publishStatus;
		
		function globalSetting(){
			$query="select * from lumonata_global";
			$result = $db->do_query($query); 
			$global=$db->fetch_array($result);	
			
			$this->adminList= $global['ladmin_list'];
			$this->frontList= $global['lfront_list'];
			$this->autoThumbs=$global['lthumb_sys'];
			$this->autoCode=$global['lcode_sys'];
			$this->latestProducts=$global['lnew_upload'];
			$this->address=$global['laddress'];
			$this->email=$global['lemail'];
			$this->cc=$global['lcc'];
			$this->bcc=$global['lbcc'];
			$this->meta_title=$global['lmeta_title'];
			$this->meta_key=$global['lmeta_desc'];
			$this->meta_desc=$global['lmeta_key'];
			$this->setMetaTitle($global['lmeta_title']);
			$this->setMetaKeywords($global['lmeta_key']);
			$this->setMetaDescriptions($global['lmeta_desc']);
			$this->commentStatus=$global['lcomments'];
			$this->publishStatus=$global['lpublish'];
			$this->latestUpload=$global['llatest_upload'];
		}
		function getAdminList(){
			return $this->adminList;
		}
		function getFrontList(){
			return $this->frontList;
		}
		function getLatestUpload(){
			return $this->latestUpload;
		}
		function hp(){
			return ($this->autoThumbs==1)?true:false;
		}
		function isAutoCode(){
			return ($this->autoCode==1)?true:false;
		}
		function isAllowComments(){
			return ($this->commentStatus==1)?true:false;
		}
		function isPublishChecked(){
			return ($this->publishStatus==1)?true:false;
		}
		function getAddress(){
			return $this->address;
		}
		function getEmail(){
			return $this->email;
		}
		function getCc(){
			return $this->cc;
		}
		function getBcc(){
			return $this->bcc;
		}
		function getCommentStatus(){
			return $this->commentStatus;
		}
		function getMetaTitle(){
			return $this->metaTitle;
		}
		function getMetaKeywords(){
			return $this->metaKeywords;
		}
		function getMetaDescriptions(){
			return $this->metaDescriptions;
		}
		
		function setMetaTitle($metaTitle=''){
			if(!empty($metaTitle))
				//$this->metaTitle=$metaTitle." - ".$this->meta_title;
				$this->metaTitle=$metaTitle;
			else
				$this->metaTitle=$this->meta_title;
		}
		function setMetaKeywords($metaKey=''){
			if(!empty($metaKey))
				$this->metaKeywords=$metaKey;
			else
				$this->metaKeywords=$this->meta_key;
		}
		function setMetaDescriptions($metaDesc=''){
			if(!empty($metaDesc))
				$this->metaDescriptions=$metaDesc;
			else
				$this->metaDescriptions=$this->meta_desc;
		}
		
		function getLatestProducts(){
			return $this->latestProducts;
		}
		
		
	}
	
?>