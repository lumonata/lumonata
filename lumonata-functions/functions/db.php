<?
class db
{// db.class.php, Database connection / query functions.
	var $hostname;// Hostname that the object should connect to
	var $username;// Username that the object should use
	var $password;// Password that the object should use
	var $database;// Database that the object should use
	var $query_num;// counts the total queries that the object has done. Some BBs do this. Might be of use for optimization etc
	var $online_site;
	function set_cred($hostname,$username,$password,$online_site)
    {
		 $this->hostname = $hostname;
		 $this->username = $username;
		 $this->password = $password;
		 $this->online_site = $online_site;
    }
    function db_connect()
	{              
		$result = mysql_connect($this->hostname,$this->username,$this->password);
                                                                                     
		if (!$result) {             
				//echo 'Connection to database server at: '.$this->hostname.' failed.';
			    return false;
        }
		return $result;
	}
                        
    function db_pconnect()
	{
         $result = mysql_pconnect($this->hostname,$this->username,$this->password);
                        /*
                        Open a persistant connection to the server. Same as mysql_connect with CGI
                        Because PHP has to be spawned each time anyway.
                        */
		if (!$result)
        {
			//echo 'Connection to database server at: '.$this->hostname.' failed.';
            return false;
        }
                                                
		return $result;
	}

    function select_db($database)
    {
        $this->database = $database;
      	if (!mysql_select_db($this->database))
        {
			//echo 'Selection of database: '.$this->database.' failed.';
			return false;
        }
    }
                        
  /* function query($query)
   {
	    $result = mysql_query($query) or die("Query failed: $query<br><br>" . mysql_error());
		return $result;
   }*/
       
	function query($query)
   {
	   // $result = mysql_query($query);// or die("" . mysql_error());
		$result = mysql_query($query) or die("Query failed: $query<br><br>" . mysql_error());
		//$result = mysql_query($query) ;
		return $result;
   }
                    
	function fetch_array($result)
	{
	 return mysql_fetch_array($result);
	}
	
	function return_query_num()
	{
	 return $this->query_num;
	}
	
	function num_rows($result)
	{
	 return mysql_num_rows($result);
	}
	
	function order_id($field,$table){
		$query_order_id = "SELECT ".$field." from ".$table." order by ".$field." DESC limit 1";
		$result_order_id = $this->query($query_order_id); 
		$row_order_id=$this->fetch_array($result_order_id);
		$order_id = $row_order_id[$field] + 1;
		return $order_id;
	}
	
	
	function get_order_id($field,$table){
		$query_order_id = "SELECT ".$field." from ".$table." order by ".$field." DESC limit 1";
		$result_order_id = $this->query($query_order_id); 
		$row_order_id=$this->fetch_array($result_order_id);
		$order_id =  is_numeric($row_order_id[$field]) + 1;
		return $order_id;
	}
	
	function update_order_id($table,$order_id){
		$qry = "update ".$table." set lorder_id = lorder_id + 1
where lorder_id >= ".$order_id;
		$result = $this->query($qry); 
		return ($result == DB_OK) ? TRUE : FALSE;
	}
	
	function get_upd_by($upd_by){
		$query_usr = "SELECT lname FROM lumonata_user where lusername = '$upd_by'";
		$result_usr = $this->query($query_usr); 
		$row_usr=$this->fetch_array($result_usr);
		$usr = $row_usr['lname'];
		return $usr;
	}
	
	function success_alert_file($file="home.php",$page,$prc,$view){			
		?>
		<script type="text/javascript">
			location = "../../../<?=$file;?>?app=<?=$page;?>&prc=<?=$prc;?>&p=<?=$view;?>";
		</script>
		<? 
	}  
	
	function success_alert($page,$prc,$view){			
		?>
		<script type="text/javascript">
			location = "../../../home.php?mod=<?=$page;?>&prc=<?=$prc;?>&p=<?=$view;?>";
		</script>
		<? 
	} 
			
	function fail_alert($alert){
		?>
			<script type="text/javascript">
				alert("<?=$alert;?>");
				history.back();
		</script>
		<?
	}
	
	function get_num_rows($table,$field,$id){
		$query_num  = "SELECT * FROM ".$table." WHERE ".$field."='".$id."'";
		$result_num = $this->query($query_num);
		$num = $this->num_rows($result_num);
		return $num;
	}
	function get_num_rows2($table,$field,$id,$field2,$id2){
		$query_num  = "SELECT * FROM ".$table." WHERE ".$field."='".$id."' AND ".$field2."='".$id2."'";
		$result_num = $this->query($query_num);
		$num = $this->num_rows($result_num);
		return $num;
	}
	function get_value_field($table,$field_get,$field,$id){
		$query_num  = "SELECT * FROM ".$table." WHERE ".$field."='".$id."'";
		$result_num = $this->query($query_num);
		$row_num=$this->fetch_array($result_num);
		$value =  $row_num[$field_get];
		return $value;
	}
	function get_view($usrtype,$sef_url){
		$query_pri = "SELECT p.lusertype_id as lusertype_id,
						p.lmodule_id as lmodule_id,
						p.lview as lview,
						p.linsert as linsert,
						p.ledit as ledit,
						p.ldelete as ldelete,
						m.lsef_url as lsef_url
				 FROM lumonata_privilege p,lumonata_module m 
				 WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
				 AND p.lmodule_id = m.lmodule_id ";
		$result_pri = $this->query($query_pri); 
		$row_pri=$this->fetch_array($result_pri);
		$view = $row_pri['lview'];
		if ($view == 1){ 
		return true; 
		}else{
		 return false;
		 }
	}
	
	function get_insert($usrtype,$sef_url){
		$query_pri = "SELECT p.lusertype_id as lusertype_id,
						p.lmodule_id as lmodule_id,
						p.lview as lview,
						p.linsert as linsert,
						p.ledit as ledit,
						p.ldelete as ldelete,
						m.lsef_url as lsef_url
				 FROM lumonata_privilege p,lumonata_module m 
				 WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
				 AND p.lmodule_id = m.lmodule_id ";
		$result_pri = $this->query($query_pri); 
		$row_pri=$this->fetch_array($result_pri);
		$insert = $row_pri['linsert'];
		if ($insert == 1){ return true; }else{ return false; }
		//return $insert;
	}
	
	function get_edit($usrtype,$sef_url){
		$query_pri2 = "SELECT p.lusertype_id as lusertype_id,
						p.lmodule_id as lmodule_id,
						p.lview as lview,
						p.linsert as linsert,
						p.ledit as ledit,
						p.ldelete as ldelete,
						m.lsef_url as lsef_url
				 FROM lumonata_privilege p,lumonata_module m 
				 WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
				 AND p.lmodule_id = m.lmodule_id ";
		$result_pri2 = $this->query($query_pri2); 
		$row_pri2 = $this->fetch_array($result_pri2);
		$edit = $row_pri2['ledit'];
		
		//return $edit;
		if ($edit == 1){ return true; }else{ return false;} 
		
	}
	function get_delete($usrtype,$sef_url){
		$query_pri = "SELECT p.lusertype_id as lusertype_id,
						p.lmodule_id as lmodule_id,
						p.lview as lview,
						p.linsert as linsert,
						p.ledit as ledit,
						p.ldelete as ldelete,
						m.lsef_url as lsef_url
				 FROM lumonata_privilege p,lumonata_module m 
				 WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
				 AND p.lmodule_id = m.lmodule_id ";
		$result_pri = $this->query($query_pri); 
		$row_pri=$this->fetch_array($result_pri);
		$delete = $row_pri['ldelete'];
		//return $delete;
		if ($delete == 1){ return true; }else{ return false; }
	}
	function str2time($str){
		$str = "/".$str;
		$jml=substr_count($str,"/");//menghitung jumlah kata yang dipisahkan dengan '/'
		$pre=explode("/",$str); //
		$result = $pre[2]."/".$pre[1]."/".$pre[3];
		$result = strtotime($result);
		return $result;
	}
	function site_url(){
		return $this->online_site;
	}
	
	function escape($string) {
		return addslashes( $string );
	}	
	function escape_by_ref(&$s) {
		$s = $this->escape($s);
	}
	function prepare_query($args=NULL) {
		if ( NULL === $args )
			return;
		$args = func_get_args();
		$query = array_shift($args);
		$query = str_replace("'%s'", '%s', $query); // in case someone mistakenly already singlequoted it
		$query = str_replace('"%s"', '%s', $query); // doublequote unquoting
		$query = str_replace('%s', "'%s'", $query); // quote the strings
		array_walk($args, array(&$this, 'escape_by_ref'));
		return @vsprintf($query, $args);
	}
	
	/* Contoh Penggunaan
		$query=parent::prepare_query("SELECT * FROM table_name where string_field1=%s and string_fields2=%s",'nyoman','tabanan');
		$query=parent::prepare_query("INSERT INTO table_name(id,nama,alamat) values(%d,%s,%s)",1,'bagus','jl permaisuri');
	*/
};

$db=new db();
$db->set_cred(HOSTNAME,DBUSER,DBPASSWORD,SITE_URL);//set credentials ready to connect,
$db->db_connect();// connect to the database using a non persistant connection
// $db->db_pconnect();// connect to the database using a persistant connection
$db->select_db(DBNAME); // Select database

?>
