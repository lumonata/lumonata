<? 
class validation extends db{

	// function to validate an integer
	function validateInteger($str) {
		return preg_match("/^-?([0-9])+$/", $str);
	}

	// function to validate a float
	function validateFloat($str) {
		return preg_match("/^-?([0-9])+([\.|,]([0-9])*)?$/", $str);
	}
	
	// function to validate a alphabetic strings
	function validateAlpha($str) {
    	return preg_match("/^[a-z]+$/i", $str);
	}
	
	// function to validate a alphanumeric strings
	function validateAlphaNum($str) {
    	return preg_match("/^[a-z0-9]*$/i", $str);
	}
	
	// function to validate an e-mail address
	function validateEmailAddress($str) {
		return eregi("^([a-z0-9_-])+([\.a-z0-9_-])*@([a-z0-9-])+(\.[a-z0-9-]+)*\.([a-z]{2,6})$", $str);
	}
	
	// function to validate a URL
	function validateUrl($str) {
		return preg_match("/^(http|https|ftp):\/\/([a-z0-9]([a-z0-9_-]*[a-z0-9])?\.)+[a-z]{2,6}\/?([a-z0-9\?\._-~&#=+%]*)?/", $str);
	}
	
	
}
?>